-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-07-2018 a las 23:33:48
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `icr_bm`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `arbitro`
--

CREATE TABLE `arbitro` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `especialidad` varchar(150) NOT NULL,
  `correo` varchar(150) NOT NULL,
  `universidad` varchar(150) NOT NULL,
  `perfil` text NOT NULL,
  `dni` varchar(40) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `telefono` varchar(30) NOT NULL,
  `nacionalidad` varchar(50) NOT NULL,
  `pagina_web` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `arbitro`
--

INSERT INTO `arbitro` (`id`, `nombre`, `especialidad`, `correo`, `universidad`, `perfil`, `dni`, `fecha_nacimiento`, `telefono`, `nacionalidad`, `pagina_web`) VALUES
(2, 'jean', 'pirata', 'luciacastelo2001@hotmail.com', 'ucsp', 'Soy una persona tranqui', '71559925', '1993-03-16', '959356446', 'peru', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `arbitro_tema`
--

CREATE TABLE `arbitro_tema` (
  `id` int(11) NOT NULL,
  `arbitro_id` int(11) NOT NULL,
  `tema_id` int(11) NOT NULL,
  `decision` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `arbitro_tema`
--

INSERT INTO `arbitro_tema` (`id`, `arbitro_id`, `tema_id`, `decision`) VALUES
(1, 2, 1, 'tema_de_interes'),
(2, 2, 2, 'no_interes'),
(3, 2, 3, 'tema_de_interes'),
(4, 2, 4, 'indiferente'),
(5, 2, 5, 'indiferente'),
(6, 2, 6, 'no_interes'),
(7, 2, 7, 'no_interes'),
(8, 2, 8, 'no_interes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `arbitro_topico`
--

CREATE TABLE `arbitro_topico` (
  `id` int(11) NOT NULL,
  `arbitro_id` int(11) NOT NULL,
  `topico_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `autor`
--

CREATE TABLE `autor` (
  `id` int(11) NOT NULL,
  `nombre` varchar(70) NOT NULL,
  `correo` varchar(150) NOT NULL,
  `universidad` varchar(150) NOT NULL,
  `nacionalidad` varchar(75) NOT NULL,
  `categoria` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `autor`
--

INSERT INTO `autor` (`id`, `nombre`, `correo`, `universidad`, `nacionalidad`, `categoria`) VALUES
(1, 'jean phillip', 'castelo huaquipaco', 'Santa anita', 'peru', 'paper'),
(2, 'jonas gutierrez', 'ksadfjklfdalkjasdfjkl', 'lkjasdjklsdafjklasd', 'sdafkljasdf', 'paper');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `autor_paper`
--

CREATE TABLE `autor_paper` (
  `id` int(11) NOT NULL,
  `autor_id` int(11) NOT NULL,
  `paper_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(4, 'evaluadores', 'evaluadores');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(6, '::1', 'test@test.com', 1530835362);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paper`
--

CREATE TABLE `paper` (
  `id` int(11) NOT NULL,
  `titulo` varchar(150) NOT NULL,
  `abstract` text NOT NULL,
  `trabajo` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `paper`
--

INSERT INTO `paper` (`id`, `titulo`, `abstract`, `trabajo`) VALUES
(1, 'asdfasdf', 'asdfasd', 'Declaración_Jurada.pdf'),
(2, 'asdfasdf', 'asdfasdf', 'Declaración_Jurada.pdf'),
(3, 'asdfasdf', 'asdfasdf', 'Declaración_Jurada.pdf'),
(4, 'asdfasdf', 'asdfasdf', 'Declaración_Jurada.pdf'),
(5, 'asdfasdf', 'asdfasdf', 'Declaración_Jurada.pdf'),
(6, 'asdfasdfasdfasdf', 'asdfasdfasdfasdfasdf', 'Equipo_de_Investigacion.pdf'),
(7, 'asdfasdfasdfa', 'sdfasdfasdfasdf', '9783319706085-c2.pdf'),
(8, 'asdfasdfasdfa', 'sdfasdfasdfasdf', '9783319706085-c2.pdf'),
(9, 'asdfasdfasdfa', 'sdfasdfasdfasdf', '9783319706085-c2.pdf'),
(10, 'asdfasdfasdfa', 'sdfasdfasdfasdf', '9783319706085-c2.pdf'),
(11, 'asdfasdfasdfasdf', 'sadfasdfasdfasdf', '96-189-1-SM_(2).pdf'),
(12, 'asdfasdfasdfasdf', 'sadfasdfasdfasdf', '96-189-1-SM_(2).pdf'),
(13, 'asdfasdf', 'asasdfasdfasd', '000011_-_Caroline_Olga_Melgar_Carrasco_-_Santiago_el_Pajarero_-_Estudiante_-_Sábado_19_de_Mayo.pdf'),
(14, 'asdfasdfasdf', 'asdfasdfasdf', 'Declaración_Jurada.pdf'),
(15, 'asdfasdfasdf', 'asdfasdfasdf', 'Declaración_Jurada1.pdf'),
(16, 'asdfasdfasd', 'asdfasdfasdfasdfasdf', 'Declaración_Jurada2.pdf'),
(17, 'asdfasdfasd', 'asdfasdfasdfasdfasdf', 'Declaración_Jurada3.pdf'),
(18, 'asdfasdfasd', 'asdfasdfasdfasdfasdf', 'Declaración_Jurada4.pdf'),
(19, 'asdfasdfasd', 'asdfasdfasdfasdfasdf', 'Declaración_Jurada5.pdf'),
(20, 'asdfasdfasd', 'asdfasdfasdfasdfasdf', 'Declaración_Jurada6.pdf'),
(21, 'asdfasdfasd', 'asdfasdfasdfasdfasdf', 'Declaración_Jurada7.pdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paper_topico`
--

CREATE TABLE `paper_topico` (
  `id` int(11) NOT NULL,
  `topico_id` int(11) NOT NULL,
  `paper_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `paper_topico`
--

INSERT INTO `paper_topico` (`id`, `topico_id`, `paper_id`) VALUES
(1, 4, 5),
(2, 17, 6),
(3, 17, 7),
(4, 17, 8),
(5, 17, 9),
(6, 17, 10),
(7, 27, 11),
(8, 27, 12),
(9, 18, 13),
(10, 27, 14),
(11, 27, 15),
(12, 20, 16),
(13, 20, 17),
(14, 8, 20),
(15, 11, 20),
(16, 20, 20),
(17, 8, 21),
(18, 11, 21),
(19, 20, 21);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `revision`
--

CREATE TABLE `revision` (
  `id` int(11) NOT NULL,
  `trabajo_id` int(11) NOT NULL,
  `arbitro_id` int(11) NOT NULL,
  `evaluation` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tema`
--

CREATE TABLE `tema` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tema`
--

INSERT INTO `tema` (`id`, `nombre`) VALUES
(1, 'Educación, formación y capacitación'),
(2, 'Procesos disruptivos soportados en I+D+i, tecnología, comunicación y personas'),
(3, 'Liderazgo y emprendimiento en la gestión de negocios'),
(4, 'Econegocios y desarrollo'),
(5, 'Negocios socialmente responsables'),
(6, 'Negocios exponenciales y startups'),
(7, 'Administración eficiente en la gestión pública'),
(8, 'E-business y empresas de servicios');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiempo`
--

CREATE TABLE `tiempo` (
  `id` int(11) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `topico`
--

CREATE TABLE `topico` (
  `id` int(11) NOT NULL,
  `nombre` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `topico`
--

INSERT INTO `topico` (`id`, `nombre`) VALUES
(1, 'Administración de Proyectos'),
(2, 'Equipos de Trabajo'),
(3, 'Modelos de Negocio'),
(4, 'Administración de TICs'),
(5, 'Finanzas'),
(6, 'Negociación'),
(7, 'Administración Estratégica'),
(8, 'Formación y Desarrollo'),
(9, 'Negocios Inmobiliarios'),
(10, 'Administración General'),
(11, 'Gestión de Alianzas Estratégicas'),
(12, 'Negocios Internacionales'),
(13, 'Banca'),
(14, 'Gestión de la Calidad'),
(15, 'NIIFs y NICs'),
(16, 'Etica en los Negocios'),
(17, 'Gestión del Sector Público'),
(18, 'Organizaciones sin fines de lucro'),
(19, 'Carreras'),
(20, 'Gestión del Talento Humano'),
(21, 'Pensamiento e Innovación'),
(22, 'Comunicaciones en los Negocios'),
(23, 'Gestión del Tiempo'),
(24, 'Pequeños Negocios'),
(25, 'Comportamiento del Consumidor y Mercado'),
(26, 'Instrumentos para Financiamiento'),
(27, 'Planificación Estratégica'),
(28, 'Compras y Adquisiciones'),
(29, 'Inversiones y Riesgos'),
(30, 'Recursos Humanos'),
(31, 'Cadena de Suministro'),
(32, 'Investigación y Tecnología'),
(33, 'Relaciones Corporativas'),
(34, 'E-government'),
(35, 'Legislación Empresarial'),
(36, 'Relaciones Publicas'),
(37, 'E-business'),
(38, 'Liderazgo'),
(39, 'Servicios Industriales'),
(40, 'E-Commerce'),
(41, 'Manejo de Riesgos y Seguros'),
(42, 'Sistemas de Información y Tecnología'),
(43, 'Educación'),
(44, 'Manufactura'),
(45, 'Supervisión'),
(46, 'Emprendimientos'),
(47, 'Marketing'),
(48, 'Ventas y Distribución');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1530905866, 1, 'Admin', 'istrator', 'ADMIN', '0'),
(2, '::1', 'jean.castelo@ucsp.edu.pe', '$2y$08$kUd6mommcwZpSXCiiAPDqOmn9UNhFFnfItBWKf0OoxGNjLsyzV8da', NULL, 'jean.castelo@ucsp.edu.pe', NULL, NULL, NULL, NULL, 1530056734, 1530757245, 1, 'Jean', 'Castelo', 'ucsp', '959356446'),
(3, '::1', 'test@test.com', '$2y$08$7do66LchbGNxudNZcGjHa.c0iuumo5IgPL.26LN/WqYvbEHvWKuiu', NULL, 'test@test.com', NULL, NULL, NULL, NULL, 1530756532, 1530756560, 1, 'Mauricio', 'Ríos García', 'Universidad Nacional de San Agustín', '9999999999'),
(4, '::1', 'luciacastelo2001@hotmail.com', '$2y$08$IoG0/qAIwh4YDqax1AQ2yue2szVEeMVviX5do71wij0lxllNhxnGG', NULL, 'luciacastelo2001@hotmail.com', NULL, NULL, NULL, NULL, 1530815203, 1530904190, 1, 'Jean', 'Castelo', 'Universidad Federal de Santa Catarina', '9999999999'),
(5, '::1', 'sdljkdsfajkla@tomate.com', '$2y$08$GaR5L5G9T46gtXMMC/viz.6konOHOCBQ2wEN7ozOeO9vuCTNxfLWa', NULL, 'sdljkdsfajkla@tomate.com', NULL, NULL, NULL, NULL, 1530910399, NULL, 1, 'proban', 'assdfa', 'cato', 'aasdfasdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(4, 2, 1),
(5, 2, 4),
(7, 3, 4),
(9, 4, 2),
(10, 4, 4),
(11, 5, 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `arbitro`
--
ALTER TABLE `arbitro`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `arbitro_tema`
--
ALTER TABLE `arbitro_tema`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_arbitro_tema_id` (`arbitro_id`),
  ADD KEY `fk_tema_id` (`tema_id`);

--
-- Indices de la tabla `arbitro_topico`
--
ALTER TABLE `arbitro_topico`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_arbitro_id` (`arbitro_id`),
  ADD KEY `fk_topico_id` (`topico_id`);

--
-- Indices de la tabla `autor`
--
ALTER TABLE `autor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `autor_paper`
--
ALTER TABLE `autor_paper`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_autor_id` (`autor_id`),
  ADD KEY `fk_paper_id` (`paper_id`);

--
-- Indices de la tabla `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `paper`
--
ALTER TABLE `paper`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `paper_topico`
--
ALTER TABLE `paper_topico`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_topico_paper_id` (`topico_id`),
  ADD KEY `fk_paper_topico_id` (`paper_id`);

--
-- Indices de la tabla `revision`
--
ALTER TABLE `revision`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_trabajo_arbitro_id` (`trabajo_id`),
  ADD KEY `fk_arbitro_trabajo_id` (`arbitro_id`);

--
-- Indices de la tabla `tema`
--
ALTER TABLE `tema`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tiempo`
--
ALTER TABLE `tiempo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `topico`
--
ALTER TABLE `topico`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `arbitro`
--
ALTER TABLE `arbitro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `arbitro_tema`
--
ALTER TABLE `arbitro_tema`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `arbitro_topico`
--
ALTER TABLE `arbitro_topico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `autor`
--
ALTER TABLE `autor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `autor_paper`
--
ALTER TABLE `autor_paper`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `paper`
--
ALTER TABLE `paper`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `paper_topico`
--
ALTER TABLE `paper_topico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `revision`
--
ALTER TABLE `revision`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tema`
--
ALTER TABLE `tema`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `tiempo`
--
ALTER TABLE `tiempo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `topico`
--
ALTER TABLE `topico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `arbitro_tema`
--
ALTER TABLE `arbitro_tema`
  ADD CONSTRAINT `fk_arbitro_tema_id` FOREIGN KEY (`arbitro_id`) REFERENCES `arbitro` (`id`),
  ADD CONSTRAINT `fk_tema_id` FOREIGN KEY (`tema_id`) REFERENCES `tema` (`id`);

--
-- Filtros para la tabla `arbitro_topico`
--
ALTER TABLE `arbitro_topico`
  ADD CONSTRAINT `fk_arbitro_id` FOREIGN KEY (`arbitro_id`) REFERENCES `arbitro` (`id`),
  ADD CONSTRAINT `fk_topico_id` FOREIGN KEY (`topico_id`) REFERENCES `topico` (`id`);

--
-- Filtros para la tabla `autor_paper`
--
ALTER TABLE `autor_paper`
  ADD CONSTRAINT `fk_autor_id` FOREIGN KEY (`autor_id`) REFERENCES `autor` (`id`),
  ADD CONSTRAINT `fk_paper_id` FOREIGN KEY (`paper_id`) REFERENCES `paper` (`id`);

--
-- Filtros para la tabla `paper_topico`
--
ALTER TABLE `paper_topico`
  ADD CONSTRAINT `fk_paper_topico_id` FOREIGN KEY (`paper_id`) REFERENCES `paper` (`id`),
  ADD CONSTRAINT `fk_topico_paper_id` FOREIGN KEY (`topico_id`) REFERENCES `topico` (`id`);

--
-- Filtros para la tabla `revision`
--
ALTER TABLE `revision`
  ADD CONSTRAINT `fk_arbitro_trabajo_id` FOREIGN KEY (`arbitro_id`) REFERENCES `arbitro` (`id`),
  ADD CONSTRAINT `fk_trabajo_arbitro_id` FOREIGN KEY (`trabajo_id`) REFERENCES `paper` (`id`);

--
-- Filtros para la tabla `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
