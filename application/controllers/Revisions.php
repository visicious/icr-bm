<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Revisions Controller
 *
 * @package  CodeIgniter
 * @category Controller
 */
class Revisions extends CI_Controller {

    /**
     * @param Revision
     * @param Paper
     * @param Arbitro
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('revision');
        $this->load->model('paper');
        $this->load->model('arbitro');
    }

    /**
     * Shows the form for creating a new revision.
     *
     * @return void
     */
    public function create()
    {
        $data = array();
        $this->_set_form_validation();

        if ($this->form_validation->run())
        {

            $paper = $this->wildfire->find('paper', $this->input->post('trabajo_id'));
            $this->revision->set_trabajo_id($paper);

            $arbitro = $this->wildfire->find('arbitro', $this->input->post('arbitro_id'));
            $this->revision->set_arbitro_id($arbitro);

            $this->revision->set_evaluation($this->input->post('evaluation'));

            $this->revision->save();

            $this->session->set_flashdata('notification', 'The revision has been created successfully!');
            $this->session->set_flashdata('alert', 'success');

            redirect('revisions');
        }

        $data['papers'] = $this->wildfire->get_all('paper')->as_dropdown('id');
        $data['arbitros'] = $this->wildfire->get_all('arbitro')->as_dropdown('id');

        $this->load->view('revisions/create', $data);
    }

    /**
     * Deletes the specified revision from storage.
     * 
     * @param  int $id
     * @return void
     */
    public function delete($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $this->wildfire->delete('revision', $id);

        $this->session->set_flashdata('notification', 'The revision has been deleted successfully!');
        $this->session->set_flashdata('alert', 'success');

        redirect('revisions');
    }

    /**
     * Shows the form for editing the specified revision.
     * 
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $this->_set_form_validation();

        if ($this->form_validation->run())
        {
            $revision = $this->wildfire->find('revision', $id);


            $paper = $this->wildfire->find('paper', $this->input->post('trabajo_id'));
            $revision->set_trabajo_id($paper);

            $arbitro = $this->wildfire->find('arbitro', $this->input->post('arbitro_id'));
            $revision->set_arbitro_id($arbitro);

            $revision->set_evaluation($this->input->post('evaluation'));

            $revision->save();

            $this->session->set_flashdata('notification', 'The revision has been updated successfully!');
            $this->session->set_flashdata('alert', 'success');

            redirect('revisions');
        }

        $data['revision'] = $this->wildfire->find('revision', $id);
        $data['papers'] = $this->wildfire->get_all('paper')->as_dropdown('id');
        $data['arbitros'] = $this->wildfire->get_all('arbitro')->as_dropdown('id');

        $this->load->view('revisions/edit', $data);
    }

    /**
     * Displays a listing of revisions.
     *
     * @return void
     */
    public function index()
    {
        $this->load->library('pagination');

        include APPPATH . 'config/pagination.php';

        $delimiters = array();
        $delimiters['keyword'] = $this->input->get('keyword');

        $config['base_url'] = base_url('revisions');
        $config['suffix'] = '&keyword=' . $delimiters['keyword'];
        $config['total_rows'] = $this->wildfire->get_all('revision', $delimiters)->total_rows();

        $delimiters['page'] = $this->input->get($config['query_string_segment']);
        $delimiters['per_page'] = $config['per_page'];

        $this->pagination->initialize($config);

        $data['revisions'] = $this->wildfire->get_all('revision', $delimiters)->result();
        $data['links'] = $this->pagination->create_links();

        $this->load->view('revisions/index', $data);
    }

    /**
     * Displays the specified revision.
     * 
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $data['revision'] = $this->wildfire->find('revision', $id);

        $this->load->view('revisions/show', $data);
    }

    /**
     * Validates the input retrieved from the view.
     * 
     * @return void
     */
    private function _set_form_validation()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('trabajo_id', 'Paper', 'required|greater_than[0]');
        $this->form_validation->set_rules('arbitro_id', 'Arbitro', 'required|greater_than[0]');
        $this->form_validation->set_rules('evaluation', 'Evaluation', 'required');
    }

}