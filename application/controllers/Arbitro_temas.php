<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Arbitro Temas Controller
 *
 * @package  CodeIgniter
 * @category Controller
 */
class Arbitro_temas extends CI_Controller {

    /**
     * @param Arbitro_tema
     * @param Arbitro
     * @param Tema
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('arbitro_tema');
        $this->load->model('arbitro');
        $this->load->model('tema');
    }

    /**
     * Shows the form for creating a new arbitro tema.
     *
     * @return void
     */
    public function create()
    {
        $data = array();
        $this->_set_form_validation();

        if ($this->form_validation->run())
        {

            $arbitro = $this->wildfire->find('arbitro', $this->input->post('arbitro_id'));
            $this->arbitro_tema->set_arbitro_id($arbitro);

            $tema = $this->wildfire->find('tema', $this->input->post('tema_id'));
            $this->arbitro_tema->set_tema_id($tema);
            $this->arbitro_tema->set_decision($this->upload->data('decision'));

            $this->arbitro_tema->save();

            $this->session->set_flashdata('notification', 'The arbitro tema has been created successfully!');
            $this->session->set_flashdata('alert', 'success');

            redirect('arbitro_temas');
        }

        $data['arbitros'] = $this->wildfire->get_all('arbitro')->as_dropdown('id');
        $data['temas'] = $this->wildfire->get_all('tema')->as_dropdown('id');

        $this->load->view('arbitro_temas/create', $data);
    }

    /**
     * Deletes the specified arbitro tema from storage.
     * 
     * @param  int $id
     * @return void
     */
    public function delete($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $this->wildfire->delete('arbitro_tema', $id);

        $this->session->set_flashdata('notification', 'The arbitro tema has been deleted successfully!');
        $this->session->set_flashdata('alert', 'success');

        redirect('arbitro_temas');
    }

    /**
     * Shows the form for editing the specified arbitro tema.
     * 
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $this->_set_form_validation();

        if ($this->form_validation->run())
        {
            $arbitro_tema = $this->wildfire->find('arbitro_tema', $id);


            $arbitro = $this->wildfire->find('arbitro', $this->input->post('arbitro_id'));
            $arbitro_tema->set_arbitro_id($arbitro);

            $tema = $this->wildfire->find('tema', $this->input->post('tema_id'));
            $arbitro_tema->set_tema_id($tema);
            $arbitro_tema->set_decision($this->upload->data('decision'));

            $arbitro_tema->save();

            $this->session->set_flashdata('notification', 'The arbitro tema has been updated successfully!');
            $this->session->set_flashdata('alert', 'success');

            redirect('arbitro_temas');
        }

        $data['arbitro_tema'] = $this->wildfire->find('arbitro_tema', $id);
        $data['arbitros'] = $this->wildfire->get_all('arbitro')->as_dropdown('id');
        $data['temas'] = $this->wildfire->get_all('tema')->as_dropdown('id');

        $this->load->view('arbitro_temas/edit', $data);
    }

    /**
     * Displays a listing of arbitro temas.
     *
     * @return void
     */
    public function index()
    {
        $this->load->library('pagination');

        include APPPATH . 'config/pagination.php';

        $delimiters = array();
        $delimiters['keyword'] = $this->input->get('keyword');

        $config['base_url'] = base_url('arbitro_temas');
        $config['suffix'] = '&keyword=' . $delimiters['keyword'];
        $config['total_rows'] = $this->wildfire->get_all('arbitro_tema', $delimiters)->total_rows();

        $delimiters['page'] = $this->input->get($config['query_string_segment']);
        $delimiters['per_page'] = $config['per_page'];

        $this->pagination->initialize($config);

        $data['arbitro_temas'] = $this->wildfire->get_all('arbitro_tema', $delimiters)->result();
        $data['links'] = $this->pagination->create_links();

        $this->load->view('arbitro_temas/index', $data);
    }

    /**
     * Displays the specified arbitro tema.
     * 
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $data['arbitro_tema'] = $this->wildfire->find('arbitro_tema', $id);

        $this->load->view('arbitro_temas/show', $data);
    }

    /**
     * Validates the input retrieved from the view.
     * 
     * @return void
     */
    private function _set_form_validation()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('arbitro_id', 'Arbitro', 'required|greater_than[0]');
        $this->form_validation->set_rules('tema_id', 'Tema', 'required|greater_than[0]');
        $this->form_validation->set_rules('decision', 'Decision', 'required');
    }

}