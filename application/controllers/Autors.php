<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Autors Controller
 *
 * @package  CodeIgniter
 * @category Controller
 */
class Autors extends CI_Controller {

    /**
     * @param Autor
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('autor');
        $this->load->model('autor_paper');
        $this->load->model('paper');

        $this->load->library('ion_auth');
    }

    /**
     * Shows the form for creating a new autor.
     *
     * @return void
     */
    public function create()
    {
        $data = array();
        $data["is_member"] = 0;
        $data["is_additional_autor"] = 0;
        $is_author= false;
        $user = $this->ion_auth->user()->row();
        
        if (!empty($user)) {
            $is_author = ($user->user_type == 'autor') ? true : false;
            $data["has_universidad"] = ($user->company) ? true : false;
            $data["has_dni"] = ($user->dni) ? true : false;
            $data["has_phone"] = ($user->phone) ? true : false;
        }
        $after_registration_url = 'autors';
        $this->_set_form_validation();

        if ($this->ion_auth->in_group("members") && $is_author) {
            $autor = $this->wildfire->find('autor', array('correo' => $user->email));

            $after_registration_url = 'papers/create';
            $data["is_member"] = 1;

            if (!empty($autor)) { //Si el usuario ha completado su registro
                redirect($after_registration_url);
            }
        } 
        else if (!$this->ion_auth->is_admin())
        {
            $this->session->set_flashdata('message', 'Acceso restringido');
            redirect('auth/login');
        }


        if ($this->form_validation->run())
        {
            $this->autor->set_nombre($this->input->post('nombre'));
            if ($user->company) {
                $this->autor->set_universidad($user->company);
            } else {
                $this->autor->set_universidad($this->input->post('universidad'));
            }
            $this->autor->set_nacionalidad($this->input->post('nacionalidad'));
            if ($user->dni) {
                $this->autor->set_dni($user->dni);
            } else {
                $this->autor->set_dni($this->input->post('dni'));
            }
            $this->autor->set_fecha_nacimiento($this->input->post('fecha_nacimiento'));
            if ($user->phone) {
                $this->autor->set_telefono($user->phone);
            } else {
                $this->autor->set_telefono($this->input->post('telefono'));
            }
            $this->autor->set_pagina_web($this->input->post('pagina_web'));
            $this->autor->set_categoria('paper');

            if ($data["is_member"]) {
                $user = $this->ion_auth->user()->row();
                $this->autor->set_correo($user->email);
            }
            else {
                $this->autor->set_correo($this->input->post('correo'));
            }

            $this->autor->save();

            $this->session->set_flashdata('notification', '¡El autor ha sido creado de manera exitosa!');
            $this->session->set_flashdata('alert', 'success');

            redirect($after_registration_url);
        }

        $this->load->view('autors/create', $data);
    }

    /**
     * Deletes the specified autor from storage.
     * 
     * @param  int $id
     * @return void
     */
    public function delete($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        if (!$this->ion_auth->is_admin())
        {
            $this->session->set_flashdata('message', 'Acceso restringido');
            redirect('auth/login');
        }

        $this->wildfire->delete('autor', $id);

        $this->session->set_flashdata('notification', 'Autor borrado exitosamente!');
        $this->session->set_flashdata('alert', 'success');
   

        redirect('autors');
    }

    public function delete_authors($paper_id, $autor_id)
    {
        if ( !isset($paper_id) || !isset($autor_id))
        {
            show_404();
        }
        $after_registration_url = 'autors';

        if ($this->ion_auth->in_group("members"))
        {
            $after_registration_url = 'dashboard/index';
        }
        else if (!$this->ion_auth->is_admin())
        {
            $this->session->set_flashdata('message', 'Acceso restringido');
            redirect('auth/login');
        }

        

        $autor_id_allowed = false;
        $user = $this->ion_auth->user()->row();
        $user_autor = $this->wildfire->find('autor', array('correo' => $user->email));
        $papers_with_autor = $this->wildfire->get_all('autor_paper', array('conditionals' => array('autor_id' => $autor_id, 'paper_id' => $paper_id)))->result();
        foreach ($papers_with_autor as $paper) 
        {
            if ($paper->get_autor_id()->get_id() == $autor_id)
            {
                $autor_id_allowed = true;
            }
        }

        if ($user_autor->get_id() != $autor_id && $autor_id_allowed) {
            $this->wildfire->delete('autor_paper', array('autor_id' => $autor_id, 'paper_id' => $paper_id));

            $this->session->set_flashdata('notification', 'Autor borrado exitosamente!');
            $this->session->set_flashdata('alert', 'success');
        }
        else if($user_autor->get_id() == $autor_id)
        {
            $this->session->set_flashdata('notification', 'No se puede borrar el autor asociado al usuario.');
            $this->session->set_flashdata('alert', 'error');
        }
        else    
        {
            $this->session->set_flashdata('notification', 'No se puede borrar este autor. No pertenece al paper.');
            $this->session->set_flashdata('alert', 'error');
        }

        redirect($after_registration_url);
    }

    /**
     * Shows the form for editing the specified autor.
     * 
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }
        $after_registration_url = 'autors';

        if ($this->ion_auth->in_group("members"))
        {
            $after_registration_url = 'dashboard/index';
        } 
        else if (!$this->ion_auth->is_admin())
        {
            $this->session->set_flashdata('message', 'Acceso restringido');
            redirect('auth/login');
        }

        

        $user = $this->ion_auth->user()->row();
        $user_autor = $this->wildfire->find('autor', array('id' => $id));
        $this->_set_form_validation();

        if ($this->form_validation->run())
        {
            $autor = $this->wildfire->find('autor', $id);

            $autor->set_nombre($this->input->post('nombre'));
            if ($user_autor->get_id() != $id) {
                $autor->set_correo($this->input->post('correo'));
            }
            $autor->set_universidad($this->input->post('universidad'));
            $autor->set_nacionalidad($this->input->post('nacionalidad'));
            $autor->set_categoria($this->input->post('categoria'));
            $autor->set_dni($this->input->post('dni'));
            $autor->set_fecha_nacimiento($this->input->post('fecha_nacimiento'));
            $autor->set_telefono($this->input->post('telefono'));
            $autor->set_pagina_web($this->input->post('pagina_web'));

            $autor->save();

            $this->session->set_flashdata('notification', 'The autor has been updated successfully!');
            $this->session->set_flashdata('alert', 'success');

            redirect($after_registration_url);
        }

        $data['autor'] = $this->wildfire->find('autor', $id);

        $this->load->view('autors/edit', $data);
    }

    /**
     * Displays a listing of autors.
     *
     * @return void
     */
    public function index()
    {
        if (!$this->ion_auth->is_admin())
        {
            $this->session->set_flashdata('message', 'Acceso restringido');
            redirect('auth/login');
        }

        $this->load->library('pagination');

        include APPPATH . 'config/pagination.php';

        $delimiters = array();
        $delimiters['keyword'] = $this->input->get('keyword');

        $config['base_url'] = base_url('autors');
        $config['suffix'] = '&keyword=' . $delimiters['keyword'];
        $config['total_rows'] = $this->wildfire->get_all('autor', $delimiters)->total_rows();

        $delimiters['page'] = $this->input->get($config['query_string_segment']);
        $delimiters['per_page'] = $config['per_page'];

        $this->pagination->initialize($config);

        $data['autors'] = $this->wildfire->get_all('autor', $delimiters)->result();
        $data['links'] = $this->pagination->create_links();

        $this->load->view('autors/index', $data);
    }

    /**
     * Displays the specified autor.
     * 
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        if (!$this->ion_auth->is_admin())
        {
            $this->session->set_flashdata('message', 'Acceso restringido');
            redirect('auth/login');
        }

        $data['autor'] = $this->wildfire->find('autor', $id);

        $this->load->view('autors/show', $data);
    }

    public function add_author($paper_id)
    {
        if ( ! isset($paper_id))
        {
            show_404();
        }

        $data = array();
        $data["is_member"] = 0;
        $data["is_additional_autor"] = 1;
        $data["paper_id"] = $paper_id;
        $after_registration_url = 'autors';
        $this->_set_form_validation();

        if ($this->ion_auth->in_group("members") && $this->ion_auth->user()->row()->user_type == 'autor') {

            $after_registration_url = 'dashboard/index';

        } 
        else if (!$this->ion_auth->is_admin())
        {
            $this->session->set_flashdata('message', 'Acceso restringido');
            redirect('auth/login');
        }


        if ($this->form_validation->run())
        {
            $this->autor->set_nombre($this->input->post('nombre'));
            $this->autor->set_universidad($this->input->post('universidad'));
            $this->autor->set_nacionalidad($this->input->post('nacionalidad'));
            $this->autor->set_dni($this->input->post('dni'));
            $this->autor->set_fecha_nacimiento($this->input->post('fecha_nacimiento'));
            $this->autor->set_telefono($this->input->post('telefono'));
            $this->autor->set_pagina_web($this->input->post('pagina_web'));
            $this->autor->set_categoria('paper');
            $this->autor->set_correo($this->input->post('correo'));

            $this->autor->save();

            $this->autor_paper->set_autor_id($this->autor);
            $paper = $this->wildfire->find('paper', $paper_id);
            $this->autor_paper->set_paper_id($paper);

            $this->autor_paper->save();

            $this->session->set_flashdata('notification', 'Autor registrado correctamente!');
            $this->session->set_flashdata('alert', 'success');

            redirect($after_registration_url);
        }

        $this->load->view('autors/create', $data);
    }

    /**
     * Validates the input retrieved from the view.
     * 
     * @return void
     */
    private function _set_form_validation()
    {
        $this->load->library('form_validation');
        $user = $this->ion_auth->user()->row();

        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
        if (!$this->ion_auth->in_group("members")) {
            $this->form_validation->set_rules('correo', 'Correo', 'required');
        }
        if (!$user->company) {
            $this->form_validation->set_rules('universidad', 'Universidad', 'required');
        }
        $this->form_validation->set_rules('nacionalidad', 'Nacionalidad', 'required');
        if (!$user->dni) {
            $this->form_validation->set_rules('dni', 'DNI/Pasaporte', 'required');
        }
        $this->form_validation->set_rules('fecha_nacimiento', 'Fecha de Nacimiento', 'required');
        if (!$user->phone) {
            $this->form_validation->set_rules('telefono', 'Telefono', 'required');
        }
    }

    public function get_autors_pdf()
    {
        if (!$this->ion_auth->is_admin())
        {
            $this->session->set_flashdata('message', 'Acceso restringido');
            redirect('auth/login');
        }

        $this->load->library("Pdf");       
  
        $data = array();       
        $data['users'] = $this->ion_auth->users()->result();
        $contador = 0;

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);    
      
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('iCR B&M 2018');
        $pdf->SetTitle('Registro de Inscritos');
        
        $pdf->SetFont('helvetica', 'B', 10);
        $x = 15;
        $y = 35;
        $w = 30;
        $h = 30;
    
        

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->AddPage();
        $html = <<<EOD
    <table class="table table table-striped table-hover">
            <thead>
                <tr>
                    <td>Nombre</td>
                    <td>Correo</td>
                    <td>DNI/Pasaporte/Documento de Identidad</td>
                    <td>Inscrito como</td>
                    <td>Pago</td>
                    <td>Voucher</td>
                </tr>
            </thead>
           
EOD;
        foreach ($data['users'] as $autor) {
            if(!empty($autor->user_type)){
                $bank_data = explode(",",$autor->bank_detail);
                $bank_string = "Numero de Operacion: ".$bank_data[0];
                $bank_string .= "Banco: ".$bank_data[1];
                $bank_string .= "Fecha: ".$bank_data[2];
                $bank_string .= "Monto: ".$bank_data[3];
                $html .= '<tr>'.
                            '<td>'.  $autor->first_name.' '.$autor->last_name. '</td>'.
                            '<td>' . $autor->email .'</td>'.
                            '<td>' . $autor->dni .'</td>'.
                            '<td>' . $autor->user_type .'</td>'.
                            '<td>' . $bank_string .'</td>'.
                            '<td> <img width="250" height="250" src="'.  base_url('uploads/voucher_photos/'.$autor->voucher_slug) .'"></td>'.
                        '</tr>';
            }
        }

        $html .= <<<EOD
    
    </table>
EOD;
        
        $pdf->writeHTML($html, true, false, false, false, '');
      
        // ---------------------------------------------------------    
      
        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $pdf->Output('registro_inscritos.pdf', 'I');


        $this->pdf->load_view('autors/autors_pdf', $data);
    }
}