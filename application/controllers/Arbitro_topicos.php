<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Arbitro Topicos Controller
 *
 * @package  CodeIgniter
 * @category Controller
 */
class Arbitro_topicos extends CI_Controller {

    /**
     * @param Arbitro_topico
     * @param Arbitro
     * @param Topico
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('arbitro_topico');
        $this->load->model('arbitro');
        $this->load->model('topico');
    }

    /**
     * Shows the form for creating a new arbitro topico.
     *
     * @return void
     */
    public function create()
    {
        $data = array();
        $this->_set_form_validation();

        if ($this->form_validation->run())
        {

            $arbitro = $this->wildfire->find('arbitro', $this->input->post('arbitro_id'));
            $this->arbitro_topico->set_arbitro_id($arbitro);

            $topico = $this->wildfire->find('topico', $this->input->post('topico_id'));
            $this->arbitro_topico->set_topico_id($topico);
            $this->arbitro_topico->set_decision($this->upload->data('decision'));

            $this->arbitro_topico->save();

            $this->session->set_flashdata('notification', 'The arbitro topico has been created successfully!');
            $this->session->set_flashdata('alert', 'success');

            redirect('arbitro_topicos');
        }

        $data['arbitros'] = $this->wildfire->get_all('arbitro')->as_dropdown('id');
        $data['topicos'] = $this->wildfire->get_all('topico')->as_dropdown('id');

        $this->load->view('arbitro_topicos/create', $data);
    }

    /**
     * Deletes the specified arbitro topico from storage.
     * 
     * @param  int $id
     * @return void
     */
    public function delete($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $this->wildfire->delete('arbitro_topico', $id);

        $this->session->set_flashdata('notification', 'The arbitro topico has been deleted successfully!');
        $this->session->set_flashdata('alert', 'success');

        redirect('arbitro_topicos');
    }

    /**
     * Shows the form for editing the specified arbitro topico.
     * 
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $this->_set_form_validation();

        if ($this->form_validation->run())
        {
            $arbitro_topico = $this->wildfire->find('arbitro_topico', $id);


            $arbitro = $this->wildfire->find('arbitro', $this->input->post('arbitro_id'));
            $arbitro_topico->set_arbitro_id($arbitro);

            $topico = $this->wildfire->find('topico', $this->input->post('topico_id'));
            $arbitro_topico->set_topico_id($topico);
            $arbitro_topico->set_decision($this->upload->data('decision'));


            $arbitro_topico->save();

            $this->session->set_flashdata('notification', 'The arbitro topico has been updated successfully!');
            $this->session->set_flashdata('alert', 'success');

            redirect('arbitro_topicos');
        }

        $data['arbitro_topico'] = $this->wildfire->find('arbitro_topico', $id);
        $data['arbitros'] = $this->wildfire->get_all('arbitro')->as_dropdown('id');
        $data['topicos'] = $this->wildfire->get_all('topico')->as_dropdown('id');

        $this->load->view('arbitro_topicos/edit', $data);
    }

    /**
     * Displays a listing of arbitro topicos.
     *
     * @return void
     */
    public function index()
    {
        $this->load->library('pagination');

        include APPPATH . 'config/pagination.php';

        $delimiters = array();
        $delimiters['keyword'] = $this->input->get('keyword');

        $config['base_url'] = base_url('arbitro_topicos');
        $config['suffix'] = '&keyword=' . $delimiters['keyword'];
        $config['total_rows'] = $this->wildfire->get_all('arbitro_topico', $delimiters)->total_rows();

        $delimiters['page'] = $this->input->get($config['query_string_segment']);
        $delimiters['per_page'] = $config['per_page'];

        $this->pagination->initialize($config);

        $data['arbitro_topicos'] = $this->wildfire->get_all('arbitro_topico', $delimiters)->result();
        $data['links'] = $this->pagination->create_links();

        $this->load->view('arbitro_topicos/index', $data);
    }

    /**
     * Displays the specified arbitro topico.
     * 
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $data['arbitro_topico'] = $this->wildfire->find('arbitro_topico', $id);

        $this->load->view('arbitro_topicos/show', $data);
    }

    /**
     * Validates the input retrieved from the view.
     * 
     * @return void
     */
    private function _set_form_validation()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('arbitro_id', 'Arbitro', 'required|greater_than[0]');
        $this->form_validation->set_rules('topico_id', 'Topico', 'required|greater_than[0]');
        $this->form_validation->set_rules('decision', 'Decision', 'required');
    }

}