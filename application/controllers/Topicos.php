<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Topicos Controller
 *
 * @package  CodeIgniter
 * @category Controller
 */
class Topicos extends CI_Controller {

    /**
     * @param Topico
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('topico');
        $this->load->library('ion_auth');
    }

    /**
     * Shows the form for creating a new topico.
     *
     * @return void
     */
    public function create()
    {
        if (!$this->ion_auth->is_admin())
        {
            $this->session->set_flashdata('message', 'You must be an admin to view this page');
            redirect('auth/login');
        }


        $data = array();
        $this->_set_form_validation();

        if ($this->form_validation->run())
        {
            $this->topico->set_nombre($this->input->post('nombre'));

            $this->topico->save();

            $this->session->set_flashdata('notification', 'The topico has been created successfully!');
            $this->session->set_flashdata('alert', 'success');

            redirect('topicos');
        }

        $this->load->view('topicos/create', $data);
    }

    /**
     * Deletes the specified topico from storage.
     * 
     * @param  int $id
     * @return void
     */
    public function delete($id)
    {
        if (!$this->ion_auth->is_admin())
        {
            $this->session->set_flashdata('message', 'You must be an admin to view this page');
            redirect('auth/login');
        }


        if ( ! isset($id))
        {
            show_404();
        }

        $this->wildfire->delete('topico', $id);

        $this->session->set_flashdata('notification', 'The topico has been deleted successfully!');
        $this->session->set_flashdata('alert', 'success');

        redirect('topicos');
    }

    /**
     * Shows the form for editing the specified topico.
     * 
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        if (!$this->ion_auth->is_admin())
        {
            $this->session->set_flashdata('message', 'You must be an admin to view this page');
            redirect('auth/login');
        }

        if ( ! isset($id))
        {
            show_404();
        }

        $this->_set_form_validation();

        if ($this->form_validation->run())
        {
            $topico = $this->wildfire->find('topico', $id);

            $topico->set_nombre($this->input->post('nombre'));

            $topico->save();

            $this->session->set_flashdata('notification', 'The topico has been updated successfully!');
            $this->session->set_flashdata('alert', 'success');

            redirect('topicos');
        }

        $data['topico'] = $this->wildfire->find('topico', $id);

        $this->load->view('topicos/edit', $data);
    }

    /**
     * Displays a listing of topicos.
     *
     * @return void
     */
    public function index()
    {
        if (!$this->ion_auth->is_admin())
        {
            $this->session->set_flashdata('message', 'Acceso restringido.');
            redirect('auth/login');
        }

        $this->load->library('pagination');

        include APPPATH . 'config/pagination.php';

        $delimiters = array();
        $delimiters['keyword'] = $this->input->get('keyword');

        $config['base_url'] = base_url('topicos');
        $config['suffix'] = '&keyword=' . $delimiters['keyword'];
        $config['total_rows'] = $this->wildfire->get_all('topico', $delimiters)->total_rows();

        $delimiters['page'] = $this->input->get($config['query_string_segment']);
        $delimiters['per_page'] = $config['per_page'];

        $this->pagination->initialize($config);

        $data['topicos'] = $this->wildfire->get_all('topico', $delimiters)->result();
        $data['links'] = $this->pagination->create_links();

        $this->load->view('topicos/index', $data);
    }

    /**
     * Displays the specified topico.
     * 
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        if (!$this->ion_auth->is_admin())
        {
            $this->session->set_flashdata('message', 'You must be an admin to view this page');
            redirect('auth/login');
        }
        
        if ( ! isset($id))
        {
            show_404();
        }

        $data['topico'] = $this->wildfire->find('topico', $id);

        $this->load->view('topicos/show', $data);
    }

    /**
     * Validates the input retrieved from the view.
     * 
     * @return void
     */
    private function _set_form_validation()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
    }

}