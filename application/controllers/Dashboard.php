<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Dashboard Controller
 *
 * @package  CodeIgniter
 * @category Controller
 */
class Dashboard extends CI_Controller {

    /**
     * @param topico
     */
    public function __construct()
    {
        parent::__construct();


        $this->load->model('topico');
        $this->load->model('arbitro_topico');
        $this->load->model('arbitro');
        $this->load->model('revision');
        $this->load->model('paper');
        $this->load->model('autor');
        $this->load->model('autor_paper');
        $this->load->model('paper_arbitro');
        $this->load->model('paper_topico');

        $this->load->library(array('ion_auth', 'form_validation'));
    }

    public function index()
    {
        $data = array();
        $data['show_topicos'] = 0;
        $data['autor_complete'] = 0;
        $data['user_type'] = 'member';
        $data['show_papers'] = 0;

        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        } 

        $user = $this->ion_auth->user()->row();
        if (!empty($user)) {
            $data['is_author'] = ($user->user_type == 'autor') ? true : false;
            $data['has_pay'] = $user->has_pay;
        }
        
        if ($this->ion_auth->is_admin()) {
            $data['user_type'] = 'admin';
            redirect('auth/index');
        } 
        else if ($this->ion_auth->in_group("chair")) {
            $data['user_type'] = 'chair';

            if ($this->ion_auth->in_group("evaluadores")) {
                $arbitro = $this->wildfire->find('arbitro', array('correo' => $user->email));

                if (!empty($arbitro)) { //Si el usuario ha completado su registro
                    $arbitro_topicos = $this->wildfire->find('arbitro_topico', array('arbitro_id' => $arbitro->get_id()));

                    if (empty($arbitro_topicos)) {
                        $data['show_topicos'] = 1;
                        $data['topicos'] = $this->wildfire->get_all('topico')->result();
                    }

                    $paper_arbitros = $this->wildfire->get_all('paper_arbitro', array('conditionals' => array('arbitro_id' => $arbitro->get_id())))->result();
                    $assigned_papers = array();
                    if (!empty($paper_arbitros)) { 
                        $data['show_papers'] = 1;
                    }

                    foreach ($paper_arbitros as $paper_arbitro_row) {
                        $assigned_paper = $this->wildfire->find('paper', $paper_arbitro_row->get_paper_id()->get_id());
                        $assigned_paper->score = $paper_arbitro_row->get_score();
                        $assigned_paper->arbitro_id = $paper_arbitro_row->get_arbitro_id()->get_id();
                        array_push($assigned_papers, $assigned_paper);
                    }
                    $data['assigned_papers'] = $assigned_papers;
                } else { //Si el usuario todavia no ha completado su registro
                    redirect('arbitros/create');
                }
            }

            $data['reviewers'] = $this->wildfire->get_all('arbitro', array('order_by' => 'id ASC'))->result();
            $data['papers_per_reviewer'] = $this->wildfire->get_all('paper_arbitro', array('order_by' => 'arbitro_id ASC'))->result();
            $topicos_per_paper = $this->wildfire->get_all('paper_topico', array('order_by' => 'paper_id ASC'))->result();
            $papers_with_topicos = array();
            $actual_id = -1;

            foreach ($topicos_per_paper as $topico_per_paper) {
                if ($actual_id == $topico_per_paper->get_paper_id()->get_id()) {
                    $papers_with_topicos[$topico_per_paper->get_paper_id()->get_id()]["topicos"] .= ",".$topico_per_paper->get_topico_id()->get_nombre();
                } else {
                    $papers_with_topicos[$topico_per_paper->get_paper_id()->get_id()] = array( 'titulo' => $topico_per_paper->get_paper_id()->get_titulo() , 'topicos' => $topico_per_paper->get_topico_id()->get_nombre());
                }
                
                $actual_id = $topico_per_paper->get_paper_id()->get_id();
            }

            $data['topicos_per_paper'] = $papers_with_topicos;
        } 
        else if ($this->ion_auth->in_group("evaluadores")) {
            $data['user_type'] = 'evaluador';
            $user = $this->ion_auth->user()->row();
            $arbitro = $this->wildfire->find('arbitro', array('correo' => $user->email));

            if (!empty($arbitro)) { //Si el usuario ha completado su registro
                $arbitro_topicos = $this->wildfire->find('arbitro_topico', array('arbitro_id' => $arbitro->get_id()));

                if (empty($arbitro_topicos)) {
                    $data['show_topicos'] = 1;
                    $data['topicos'] = $this->wildfire->get_all('topico')->result();
                }

                $paper_arbitros = $this->wildfire->get_all('paper_arbitro', array('conditionals' => array('arbitro_id' => $arbitro->get_id())))->result();
                $assigned_papers = array();
                if (!empty($paper_arbitros)) { 
                    $data['show_papers'] = 1;
                }

                foreach ($paper_arbitros as $paper_arbitro_row) {
                    $assigned_paper = $this->wildfire->find('paper', $paper_arbitro_row->get_paper_id()->get_id());
                    $assigned_paper->score = $paper_arbitro_row->get_score();
                    $assigned_paper->arbitro_id = $paper_arbitro_row->get_arbitro_id()->get_id();
                    array_push($assigned_papers, $assigned_paper);
                }
                $data['assigned_papers'] = $assigned_papers;
            } else { //Si el usuario todavia no ha completado su registro
                redirect('arbitros/create');
            }    
        } 
        else if ($this->ion_auth->in_group("members")) {
            $user = $this->ion_auth->user()->row();
            $data['autor_or_participant'] = $user->user_type;
            $autor = $this->wildfire->find('autor', array('correo' => $user->email));

            if (empty($autor) && $user->user_type == "autor") {
                redirect('autors/create');
            } else if ($user->user_type == "autor") {            
                $has_paper = $this->wildfire->get_all('autor_paper', array('conditionals' => array('autor_id' => $autor->get_id())))->result();
                if (!empty($has_paper)) {
                    $data['papers'] = array();
                    foreach ($has_paper as $paper) {
                        $papers_per_autor = $this->wildfire->get_all('paper', array('conditionals' => array('id' => $paper->get_paper_id()->get_id())))->result();
                        foreach ($papers_per_autor as $each_paper) {
                            $other_autors = $this->wildfire->get_all('autor_paper', array( 'conditionals' => array('paper_id' => $each_paper->get_id())))->result();
                            $other_autors_array = array();
                            foreach($other_autors as $autors){
                                array_push($other_autors_array, $this->wildfire->find('autor', array('id' => $autors->get_autor_id()->get_id())));
                            }

                            $data['other_autors'][$each_paper->get_id()] = $other_autors_array;
                            array_push($data['papers'], $each_paper);
                        }
                    }
                }
            } /*else if (!$user->has_pay) {
                redirect('auth/upload_pay');
            } else {
                redirect('dashboard/upload_pay');
            }*/
        } 
        else {
            redirect('auth/login');
        }

        $this->load->view('dashboard/index', $data);
    }

    public function pick_tracks(){
        $data = array();
        $data['show_topicos'] = 0;
        $this->_set_form_pick_tracks_validation();

        $user = $this->ion_auth->user()->row();
        $arbitro = $this->wildfire->find('arbitro', array('correo' => $user->email));

        if ($this->form_validation->run())
        {
            $array_arbitro_topico = array();

            $decision = $this->input->post('decision');

            foreach($decision as $key=>$elem) {

                $array_elem = array(
                    'arbitro_id' => $arbitro->get_id(),
                    'topico_id' => $key,
                    'decision' => $elem,
                );

                array_push($array_arbitro_topico, $array_elem);
            }
            $this->arbitro_topico->save_multiple($array_arbitro_topico);

            $this->session->set_flashdata('notification', 'Registradas sus preferencias');
            $this->session->set_flashdata('alert', 'success');

            redirect('dashboard/index');
        }

        if ($this->ion_auth->in_group("evaluadores")) {

            if (!empty($arbitro)) { //Si el usuario ha completado su registro
                $arbitro_topicos = $this->wildfire->find('arbitro_topico', array('arbitro_id' => $arbitro->get_id()));

                if (empty($arbitro_topicos)) {
                    $data['show_topicos'] = 1;
                    $data['topicos'] = $this->wildfire->get_all('topico')->result();
                }
            } else { //Si el usuario todavia no ha completado su registro
                redirect('arbitros/create');
            }
        }

        $this->load->view('dashboard/index', $data);
    }

    private function _set_form_pick_tracks_validation()
    {
        $this->load->library('form_validation');
        $topicos = $this->wildfire->get_all('topico')->result();
        foreach ($topicos as $key => $topico) {
            $this->form_validation->set_rules('decision['.$topico->get_id().']', 'Decision', 'required');
        }
        
    }
}