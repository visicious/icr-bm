<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Papers Controller
 *
 * @package  CodeIgniter
 * @category Controller
 */
class Papers extends CI_Controller {

    /**
     * @param Paper
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('paper_topico');
        $this->load->model('topico');
        $this->load->model('paper');
        $this->load->model('autor');
        $this->load->model('autor_paper');

        $config = array(
                'upload_path' => "./uploads/",
                'allowed_types' => "doc|docx",
                'overwrite' => FALSE,
                'max_size' => "10048000",
                'max_height' => "768",
                'max_width' => "1024"
            );
        $this->load->library('upload', $config);
        $this->load->library('ion_auth');
        $this->load->helper('string');
    }

    /**
     * Shows the form for creating a new paper.
     *
     * @return void
     */
    public function create()
    {
        $data = array();
        $data["is_member"] = 0;
        $after_registration_url = 'papers';
        $this->_set_form_validation();

        if ($this->ion_auth->in_group("members")) 
        {
            $after_registration_url = 'dashboard/index';
            $data["is_member"] = 1;
        } 
        else if (!$this->ion_auth->is_admin())
        {
            $this->session->set_flashdata('message', 'Acceso restringido');
            redirect('auth/login');

        }

        // TODO: Change configuration to set name to be a aleatory name and save it as a field in the database
        //$this->upload->initialize($config);

        if ($this->form_validation->run())
        {
            $topicos_slug = "";
            foreach($this->input->post('topicos') as $key=>$elem) {
                $topicos_slug .= $key;
            }

            $config = array(
                'upload_path' => "./uploads/",
                'allowed_types' => "doc|docx",
                'overwrite' => FALSE,
                'max_size' => "10048000",
                'max_height' => "768",
                'max_width' => "1024",
                'file_name' => $topicos_slug."-".random_string('alnum', 16)
            );

            $this->upload->initialize($config);
            if($this->upload->do_upload('trabajo'))
            {
                $data = array('upload_data' => $this->upload->data());
            } else {
                $error = array('error' => $this->upload->display_errors());
                $data['topicos'] = $this->wildfire->get_all('topico')->as_dropdown('nombre');
                $this->session->set_flashdata('notification', 'Error al subir el paper. Revise el formato y el tamaño del archivo y vuelva a intentarlo');
                $this->session->set_flashdata('alert', 'error');
                $this->load->view('papers/create', $data);
                return;
            }

            $topicos = $this->input->post('topicos');

            $this->paper->set_titulo($this->input->post('titulo'));
            $this->paper->set_abstract($this->input->post('abstract'));
            $this->paper->set_trabajo($this->upload->data('file_name'));

            $this->paper->save();

            $paper_elem = $this->wildfire->find('paper', $this->paper->get_id());
            $array_paper_topico = array();

            foreach($topicos as $key=>$elem) {

                $array_elem = array(
                    'topico_id' => $key,
                    'paper_id' => $this->paper->get_id(),
                );

                array_push($array_paper_topico, $array_elem);
            }
            $this->paper_topico->save_multiple($array_paper_topico);

            $has_doc_tesis = $this->input->post('has_doc_tesis') == 'yes'?1:0;

            $user = $this->ion_auth->user()->row();
            $autor = $this->wildfire->find('autor', array('correo' => $user->email));
            $autor->set_has_doc_tesis($has_doc_tesis);
            $autor->save();

            $this->autor_paper->set_autor_id($autor);

            $paper = $this->wildfire->find('paper', $this->paper->get_id());
            $this->autor_paper->set_paper_id($paper);

            $this->autor_paper->save();


            $this->session->set_flashdata('notification', 'Paper creado de manera satisfactoria!');
            $this->session->set_flashdata('alert', 'success');

            redirect($after_registration_url);
        } else {
            $data['topicos'] = $this->wildfire->get_all('topico')->as_dropdown('nombre');
        }

        $this->load->view('papers/create', $data);
    }

    /**
     * Deletes the specified paper from storage.
     * 
     * @param  int $id
     * @return void
     */
    public function delete($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $this->wildfire->delete('paper', $id);

        $this->session->set_flashdata('notification', 'The paper has been deleted successfully!');
        $this->session->set_flashdata('alert', 'success');

        redirect('papers');
    }

    /**
     * Shows the form for editing the specified paper.
     * 
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $this->_set_form_validation();

        if ($this->form_validation->run())
        {
            $paper = $this->wildfire->find('paper', $id);

            $paper->set_titulo($this->input->post('titulo'));
            $paper->set_abstract($this->input->post('abstract'));
            $paper->set_trabajo($this->input->post('trabajo'));

            $paper->save();

            $this->session->set_flashdata('notification', 'The paper has been updated successfully!');
            $this->session->set_flashdata('alert', 'success');

            redirect('papers');
        }

        $data['paper'] = $this->wildfire->find('paper', $id);

        $this->load->view('papers/edit', $data);
    }

    /**
     * Displays a listing of papers.
     *
     * @return void
     */
    public function index()
    {
        $this->load->library('pagination');

        include APPPATH . 'config/pagination.php';

        $delimiters = array();
        $delimiters['keyword'] = $this->input->get('keyword');

        $config['base_url'] = base_url('papers');
        $config['suffix'] = '&keyword=' . $delimiters['keyword'];
        $config['total_rows'] = $this->wildfire->get_all('paper', $delimiters)->total_rows();

        $delimiters['page'] = $this->input->get($config['query_string_segment']);
        $delimiters['per_page'] = $config['per_page'];

        $this->pagination->initialize($config);

        $data['papers'] = $this->wildfire->get_all('paper', $delimiters)->result();
        $data['links'] = $this->pagination->create_links();

        $this->load->view('papers/index', $data);
    }

    /**
     * Displays the specified paper.
     * 
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $data['paper'] = $this->wildfire->find('paper', $id);

        $this->load->view('papers/show', $data);
    }

    /**
     * Validates the input retrieved from the view.
     * 
     * @return void
     */
    private function _set_form_validation()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('titulo', 'Titulo', 'trim|required');
        $this->form_validation->set_rules('abstract', 'Abstract', 'trim|required');
        if (empty($_FILES['trabajo']['name']))
        {
            $this->form_validation->set_rules('trabajo', 'Trabajo ', 'required');
        }
        //$this->form_validation->set_rules('trabajo', 'Trabajo', 'required');
    }

}