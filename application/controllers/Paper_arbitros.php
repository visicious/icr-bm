<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Paper Arbitros Controller
 *
 * @package  CodeIgniter
 * @category Controller
 */
class Paper_arbitros extends CI_Controller {

    /**
     * @param Paper_arbitro
     * @param Arbitro
     * @param Paper
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('autor_paper');
        $this->load->model('paper_arbitro');
        $this->load->model('autor');
        $this->load->model('arbitro');
        $this->load->model('paper');

        $this->load->library(array('ion_auth', 'form_validation'));
    }

    /**
     * Shows the form for creating a new paper arbitro.
     *
     * @return void
     */
    public function create()
    {
        $data = array();
        $after_registration_url = 'paper_arbitros';
        $this->_set_form_validation();

        if ($this->ion_auth->in_group("chair")) 
        {
            $after_registration_url = 'dashboard/index';
        } 
        else if (!$this->ion_auth->is_admin())
        {
            $this->session->set_flashdata('message', 'Acceso restringido');
            redirect('auth/login');

        }

        if ($this->form_validation->run())
        {

            $arbitro = $this->wildfire->find('arbitro', $this->input->post('arbitro_id'));
            $this->paper_arbitro->set_arbitro_id($arbitro);

            $paper = $this->wildfire->find('paper', $this->input->post('paper_id'));
            $this->paper_arbitro->set_paper_id($paper);

            $this->paper_arbitro->set_score($this->input->post('score'));

            $this->paper_arbitro->save();

            $this->session->set_flashdata('notification', 'The paper arbitro has been created successfully!');
            $this->session->set_flashdata('alert', 'success');

            redirect($after_registration_url);
        }

        $data['arbitros'] = $this->wildfire->get_all('arbitro')->as_dropdown('nombre');
        $data['papers'] = array();
        $valid_papers = $this->wildfire->get_all('autor_paper')->result();
        foreach ($valid_papers as $paper) {
            $data['papers'][$paper->get_paper_id()->get_titulo()] = $paper->get_paper_id()->get_titulo();
        }

        $this->load->view('paper_arbitros/create', $data);
    }

    /**
     * Deletes the specified paper arbitro from storage.
     * 
     * @param  int $id
     * @return void
     */
    public function delete($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $this->wildfire->delete('paper_arbitro', $id);

        $this->session->set_flashdata('notification', 'The paper arbitro has been deleted successfully!');
        $this->session->set_flashdata('alert', 'success');

        redirect('paper_arbitros');
    }

    /**
     * Shows the form for editing the specified paper arbitro.
     * 
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $this->_set_form_validation();

        if ($this->form_validation->run())
        {
            $paper_arbitro = $this->wildfire->find('paper_arbitro', $id);

            $paper_arbitro->set_id($this->input->post('id'));

            $arbitro = $this->wildfire->find('arbitro', $this->input->post('arbitro_id'));
            $paper_arbitro->set_arbitro_id($arbitro);

            $paper = $this->wildfire->find('paper', $this->input->post('paper_id'));
            $paper_arbitro->set_paper_id($paper);

            $paper_arbitro->set_score($this->input->post('score'));

            $paper_arbitro->save();

            $this->session->set_flashdata('notification', 'The paper arbitro has been updated successfully!');
            $this->session->set_flashdata('alert', 'success');

            redirect('paper_arbitros');
        }


        $data['paper_arbitro'] = $this->wildfire->find('paper_arbitro', $id);
        $data['arbitros'] = $this->wildfire->get_all('arbitro')->as_dropdown('id');
        $data['papers'] = $this->wildfire->get_all('paper')->as_dropdown('id');

        $this->load->view('paper_arbitros/edit', $data);
    }

    /**
     * Displays a listing of paper arbitros.
     *
     * @return void
     */
    public function index()
    {
        $this->load->library('pagination');

        include APPPATH . 'config/pagination.php';

        $delimiters = array();
        $delimiters['keyword'] = $this->input->get('keyword');

        $config['base_url'] = base_url('paper_arbitros');
        $config['suffix'] = '&keyword=' . $delimiters['keyword'];
        $config['total_rows'] = $this->wildfire->get_all('paper_arbitro', $delimiters)->total_rows();

        $delimiters['page'] = $this->input->get($config['query_string_segment']);
        $delimiters['per_page'] = $config['per_page'];

        $this->pagination->initialize($config);

        $data['paper_arbitros'] = $this->wildfire->get_all('paper_arbitro', $delimiters)->result();
        $data['links'] = $this->pagination->create_links();

        $this->load->view('paper_arbitros/index', $data);
    }

    /**
     * Displays the specified paper arbitro.
     * 
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $data['paper_arbitro'] = $this->wildfire->find('paper_arbitro', $id);

        $this->load->view('paper_arbitros/show', $data);
    }

    /**
     * Validates the input retrieved from the view.
     * 
     * @return void
     */
    private function _set_form_validation()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('arbitro_id', 'Arbitro', 'required');
        $this->form_validation->set_rules('paper_id', 'Paper', 'required');
        $this->form_validation->set_rules('score', 'Score', 'numeric');
    }

    public function set_score($arbitro_id, $paper_id, $score) {
        $paper_arbitros = $this->wildfire->get_all('paper_arbitro', array('conditionals' => array('arbitro_id' => $arbitro_id, 'paper_id' => $paper_id)))->result();
        $text = "Calificación guarda correctamente";

        foreach ($paper_arbitros as $paper_arbitro) {
            $paper_arbitro->set_score($score);
            $paper_arbitro->save();
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode(array(
                    'text' => $text,
                    'type' => 'success'
            )));;
    }

}