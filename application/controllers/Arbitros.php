<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Arbitros Controller
 *
 * @package  CodeIgniter
 * @category Controller
 */
class Arbitros extends CI_Controller {

    /**
     * @param Arbitro
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('arbitro');
        $this->load->library('ion_auth');
    }

    /**
     * Shows the form for creating a new arbitro.
     *
     * @return void
     */
    public function create()
    {
        $data = array();
        $after_registration_url = 'arbitros';
        $data["is_evaluador"] = 0;

        if ($this->ion_auth->in_group("evaluadores")) {
            $user = $this->ion_auth->user()->row();
            $arbitro = $this->wildfire->find('arbitro', array('correo' => $user->email));

            $after_registration_url = 'auth/change_password';
            $data["is_evaluador"] = 1;

            if (!empty($arbitro)) { //Si el usuario ha completado su registro
                redirect('dashboard/index');
            }
        } 
        else if (!$this->ion_auth->is_admin())
        {
            $this->session->set_flashdata('message', 'Acceso restringido');
            redirect('auth/login');
        }
        
        $this->_set_form_validation();

        if ($this->form_validation->run())
        {
            $this->arbitro->set_nombre($this->input->post('nombre'));
            $this->arbitro->set_especialidad($this->input->post('especialidad'));
            $this->arbitro->set_universidad($this->input->post('universidad'));
            $this->arbitro->set_perfil($this->input->post('perfil'));
            $this->arbitro->set_dni($this->input->post('dni'));
            $this->arbitro->set_fecha_nacimiento($this->input->post('fecha_nacimiento'));
            $this->arbitro->set_telefono($this->input->post('telefono'));
            $this->arbitro->set_nacionalidad($this->input->post('nacionalidad'));
            $this->arbitro->set_pagina_web($this->input->post('pagina_web'));

            if ($data["is_evaluador"]) {
                $user = $this->ion_auth->user()->row();
                $this->arbitro->set_correo($user->email);
            }
            else {
                $this->arbitro->set_correo($this->input->post('correo'));
            }

            $this->arbitro->save();

            $this->session->set_flashdata('notification', 'The arbitro has been created successfully!');
            $this->session->set_flashdata('alert', 'success');

            redirect($after_registration_url);
        }

        $this->load->view('arbitros/create', $data);
    }

    /**
     * Deletes the specified arbitro from storage.
     * 
     * @param  int $id
     * @return void
     */
    public function delete($id)
    {
        if (!$this->ion_auth->is_admin())
        {
            $this->session->set_flashdata('message', 'You must be an admin to view this page');
            redirect('auth/login');
        }

        if ( ! isset($id))
        {
            show_404();
        }

        $this->wildfire->delete('arbitro', $id);

        $this->session->set_flashdata('notification', 'The arbitro has been deleted successfully!');
        $this->session->set_flashdata('alert', 'success');

        redirect('arbitros');
    }

    /**
     * Shows the form for editing the specified arbitro.
     * 
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        if (!$this->ion_auth->is_admin())
        {
            $this->session->set_flashdata('message', 'You must be an admin to view this page');
            redirect('auth/login');
        }

        if ( ! isset($id))
        {
            show_404();
        }

        $this->_set_form_validation();

        if ($this->form_validation->run())
        {
            $arbitro = $this->wildfire->find('arbitro', $id);

            $arbitro->set_nombre($this->input->post('nombre'));
            $arbitro->set_especialidad($this->input->post('especialidad'));
            $arbitro->set_correo($this->input->post('correo'));
            $arbitro->set_universidad($this->input->post('universidad'));
            $arbitro->set_perfil($this->input->post('perfil'));
            $arbitro->set_dni($this->input->post('dni'));
            $arbitro->set_fecha_nacimiento($this->input->post('fecha_nacimiento'));
            $arbitro->set_telefono($this->input->post('telefono'));
            $arbitro->set_nacionalidad($this->input->post('nacionalidad'));
            $arbitro->set_pagina_web($this->input->post('pagina_web'));

            $arbitro->save();

            $this->session->set_flashdata('notification', 'The arbitro has been updated successfully!');
            $this->session->set_flashdata('alert', 'success');

            redirect('arbitros');
        }

        $data['arbitro'] = $this->wildfire->find('arbitro', $id);

        $this->load->view('arbitros/edit', $data);
    }

    /**
     * Displays a listing of arbitros.
     *
     * @return void
     */
    public function index()
    {
        if (!$this->ion_auth->is_admin())
        {
            $this->session->set_flashdata('message', 'You must be an admin to view this page');
            redirect('auth/login');
        }

        $this->load->library('pagination');

        include APPPATH . 'config/pagination.php';

        $delimiters = array();
        $delimiters['keyword'] = $this->input->get('keyword');

        $config['base_url'] = base_url('arbitros');
        $config['suffix'] = '&keyword=' . $delimiters['keyword'];
        $config['total_rows'] = $this->wildfire->get_all('arbitro', $delimiters)->total_rows();

        $delimiters['page'] = $this->input->get($config['query_string_segment']);
        $delimiters['per_page'] = $config['per_page'];

        $this->pagination->initialize($config);

        $data['arbitros'] = $this->wildfire->get_all('arbitro', $delimiters)->result();
        $data['links'] = $this->pagination->create_links();

        $this->load->view('arbitros/index', $data);
    }

    /**
     * Displays the specified arbitro.
     * 
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        if (!$this->ion_auth->is_admin())
        {
            $this->session->set_flashdata('message', 'You must be an admin to view this page');
            redirect('auth/login');
        }
        
        if ( ! isset($id))
        {
            show_404();
        }

        $data['arbitro'] = $this->wildfire->find('arbitro', $id);

        $this->load->view('arbitros/show', $data);
    }

    /**
     * Validates the input retrieved from the view.
     * 
     * @return void
     */
    private function _set_form_validation()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
        $this->form_validation->set_rules('especialidad', 'Especialidad', 'required');
        if (!$this->ion_auth->in_group("evaluadores")) {
            $this->form_validation->set_rules('correo', 'Correo', 'required');
        }
        $this->form_validation->set_rules('universidad', 'Universidad', 'required');
        $this->form_validation->set_rules('nacionalidad', 'Nacionalidad', 'required');
        $this->form_validation->set_rules('dni', 'DNI/Pasaporte', 'required');
        $this->form_validation->set_rules('fecha_nacimiento', 'Fecha de Nacimiento', 'required');
        $this->form_validation->set_rules('telefono', 'Telefono', 'required');
        $this->form_validation->set_rules('perfil', 'Perfil', 'required');
    }

}