<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Temas Controller
 *
 * @package  CodeIgniter
 * @category Controller
 */
class Temas extends CI_Controller {

    /**
     * @param Tema
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('tema');
    }

    /**
     * Shows the form for creating a new tema.
     *
     * @return void
     */
    public function create()
    {
        $data = array();
        $this->_set_form_validation();

        if ($this->form_validation->run())
        {
            $this->tema->set_nombre($this->input->post('nombre'));

            $this->tema->save();

            $this->session->set_flashdata('notification', 'The tema has been created successfully!');
            $this->session->set_flashdata('alert', 'success');

            redirect('temas');
        }

        $this->load->view('temas/create', $data);
    }

    /**
     * Deletes the specified tema from storage.
     * 
     * @param  int $id
     * @return void
     */
    public function delete($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $this->wildfire->delete('tema', $id);

        $this->session->set_flashdata('notification', 'The tema has been deleted successfully!');
        $this->session->set_flashdata('alert', 'success');

        redirect('temas');
    }

    /**
     * Shows the form for editing the specified tema.
     * 
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $this->_set_form_validation();

        if ($this->form_validation->run())
        {
            $tema = $this->wildfire->find('tema', $id);

            $tema->set_nombre($this->input->post('nombre'));

            $tema->save();

            $this->session->set_flashdata('notification', 'The tema has been updated successfully!');
            $this->session->set_flashdata('alert', 'success');

            redirect('temas');
        }

        $data['tema'] = $this->wildfire->find('tema', $id);

        $this->load->view('temas/edit', $data);
    }

    /**
     * Displays a listing of temas.
     *
     * @return void
     */
    public function index()
    {
        $this->load->library('pagination');

        include APPPATH . 'config/pagination.php';

        $delimiters = array();
        $delimiters['keyword'] = $this->input->get('keyword');

        $config['base_url'] = base_url('temas');
        $config['suffix'] = '&keyword=' . $delimiters['keyword'];
        $config['total_rows'] = $this->wildfire->get_all('tema', $delimiters)->total_rows();

        $delimiters['page'] = $this->input->get($config['query_string_segment']);
        $delimiters['per_page'] = $config['per_page'];

        $this->pagination->initialize($config);

        $data['temas'] = $this->wildfire->get_all('tema', $delimiters)->result();
        $data['links'] = $this->pagination->create_links();

        $this->load->view('temas/index', $data);
    }

    /**
     * Displays the specified tema.
     * 
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $data['tema'] = $this->wildfire->find('tema', $id);

        $this->load->view('temas/show', $data);
    }

    /**
     * Validates the input retrieved from the view.
     * 
     * @return void
     */
    private function _set_form_validation()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
    }

}