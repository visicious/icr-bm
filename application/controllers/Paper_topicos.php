<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Paper Topicos Controller
 *
 * @package  CodeIgniter
 * @category Controller
 */
class Paper_topicos extends CI_Controller {

    /**
     * @param Paper_topico
     * @param Topico
     * @param Paper
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('paper_topico');
        $this->load->model('topico');
        $this->load->model('paper');
    }

    /**
     * Shows the form for creating a new paper topico.
     *
     * @return void
     */
    public function create()
    {
        $data = array();
        $this->_set_form_validation();

        if ($this->form_validation->run())
        {

            $topico = $this->wildfire->find('topico', $this->input->post('topico_id'));
            $this->paper_topico->set_topico_id($topico);

            $paper = $this->wildfire->find('paper', $this->input->post('paper_id'));
            $this->paper_topico->set_paper_id($paper);


            $this->paper_topico->save();

            $this->session->set_flashdata('notification', 'The paper topico has been created successfully!');
            $this->session->set_flashdata('alert', 'success');

            redirect('paper_topicos');
        }

        $data['topicos'] = $this->wildfire->get_all('topico')->as_dropdown('id');
        $data['papers'] = $this->wildfire->get_all('paper')->as_dropdown('id');

        $this->load->view('paper_topicos/create', $data);
    }

    /**
     * Deletes the specified paper topico from storage.
     * 
     * @param  int $id
     * @return void
     */
    public function delete($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $this->wildfire->delete('paper_topico', $id);

        $this->session->set_flashdata('notification', 'The paper topico has been deleted successfully!');
        $this->session->set_flashdata('alert', 'success');

        redirect('paper_topicos');
    }

    /**
     * Shows the form for editing the specified paper topico.
     * 
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $this->_set_form_validation();

        if ($this->form_validation->run())
        {
            $paper_topico = $this->wildfire->find('paper_topico', $id);


            $topico = $this->wildfire->find('topico', $this->input->post('topico_id'));
            $paper_topico->set_topico_id($topico);

            $paper = $this->wildfire->find('paper', $this->input->post('paper_id'));
            $paper_topico->set_paper_id($paper);


            $paper_topico->save();

            $this->session->set_flashdata('notification', 'The paper topico has been updated successfully!');
            $this->session->set_flashdata('alert', 'success');

            redirect('paper_topicos');
        }

        $data['paper_topico'] = $this->wildfire->find('paper_topico', $id);
        $data['topicos'] = $this->wildfire->get_all('topico')->as_dropdown('id');
        $data['papers'] = $this->wildfire->get_all('paper')->as_dropdown('id');

        $this->load->view('paper_topicos/edit', $data);
    }

    /**
     * Displays a listing of paper topicos.
     *
     * @return void
     */
    public function index()
    {
        $this->load->library('pagination');

        include APPPATH . 'config/pagination.php';

        $delimiters = array();
        $delimiters['keyword'] = $this->input->get('keyword');

        $config['base_url'] = base_url('paper_topicos');
        $config['suffix'] = '&keyword=' . $delimiters['keyword'];
        $config['total_rows'] = $this->wildfire->get_all('paper_topico', $delimiters)->total_rows();

        $delimiters['page'] = $this->input->get($config['query_string_segment']);
        $delimiters['per_page'] = $config['per_page'];

        $this->pagination->initialize($config);

        $data['paper_topicos'] = $this->wildfire->get_all('paper_topico', $delimiters)->result();
        $data['links'] = $this->pagination->create_links();

        $this->load->view('paper_topicos/index', $data);
    }

    /**
     * Displays the specified paper topico.
     * 
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $data['paper_topico'] = $this->wildfire->find('paper_topico', $id);

        $this->load->view('paper_topicos/show', $data);
    }

    /**
     * Validates the input retrieved from the view.
     * 
     * @return void
     */
    private function _set_form_validation()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('topico_id', 'Topico', 'required|greater_than[0]');
        $this->form_validation->set_rules('paper_id', 'Paper', 'required|greater_than[0]');
    }

}