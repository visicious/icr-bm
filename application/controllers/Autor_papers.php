<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Autor Papers Controller
 *
 * @package  CodeIgniter
 * @category Controller
 */
class Autor_papers extends CI_Controller {

    /**
     * @param Autor_paper
     * @param Autor
     * @param Paper
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('autor_paper');
        $this->load->model('autor');
        $this->load->model('paper');
    }

    /**
     * Shows the form for creating a new autor paper.
     *
     * @return void
     */
    public function create()
    {
        $data = array();
        $this->_set_form_validation();

        if ($this->form_validation->run())
        {

            $autor = $this->wildfire->find('autor', $this->input->post('autor_id'));
            $this->autor_paper->set_autor_id($autor);

            $paper = $this->wildfire->find('paper', $this->input->post('paper_id'));
            $this->autor_paper->set_paper_id($paper);


            $this->autor_paper->save();

            $this->session->set_flashdata('notification', 'The autor paper has been created successfully!');
            $this->session->set_flashdata('alert', 'success');

            redirect('autor_papers');
        }

        $data['autors'] = $this->wildfire->get_all('autor')->as_dropdown('id');
        $data['papers'] = $this->wildfire->get_all('paper')->as_dropdown('id');

        $this->load->view('autor_papers/create', $data);
    }

    /**
     * Deletes the specified autor paper from storage.
     * 
     * @param  int $id
     * @return void
     */
    public function delete($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $this->wildfire->delete('autor_paper', $id);

        $this->session->set_flashdata('notification', 'The autor paper has been deleted successfully!');
        $this->session->set_flashdata('alert', 'success');

        redirect('autor_papers');
    }

    /**
     * Shows the form for editing the specified autor paper.
     * 
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $this->_set_form_validation();

        if ($this->form_validation->run())
        {
            $autor_paper = $this->wildfire->find('autor_paper', $id);


            $autor = $this->wildfire->find('autor', $this->input->post('autor_id'));
            $autor_paper->set_autor_id($autor);

            $paper = $this->wildfire->find('paper', $this->input->post('paper_id'));
            $autor_paper->set_paper_id($paper);


            $autor_paper->save();

            $this->session->set_flashdata('notification', 'The autor paper has been updated successfully!');
            $this->session->set_flashdata('alert', 'success');

            redirect('autor_papers');
        }

        $data['autor_paper'] = $this->wildfire->find('autor_paper', $id);
        $data['autors'] = $this->wildfire->get_all('autor')->as_dropdown('id');
        $data['papers'] = $this->wildfire->get_all('paper')->as_dropdown('id');

        $this->load->view('autor_papers/edit', $data);
    }

    /**
     * Displays a listing of autor papers.
     *
     * @return void
     */
    public function index()
    {
        $this->load->library('pagination');

        include APPPATH . 'config/pagination.php';

        $delimiters = array();
        $delimiters['keyword'] = $this->input->get('keyword');

        $config['base_url'] = base_url('autor_papers');
        $config['suffix'] = '&keyword=' . $delimiters['keyword'];
        $config['total_rows'] = $this->wildfire->get_all('autor_paper', $delimiters)->total_rows();

        $delimiters['page'] = $this->input->get($config['query_string_segment']);
        $delimiters['per_page'] = $config['per_page'];

        $this->pagination->initialize($config);

        $data['autor_papers'] = $this->wildfire->get_all('autor_paper', $delimiters)->result();
        $data['links'] = $this->pagination->create_links();

        $this->load->view('autor_papers/index', $data);
    }

    /**
     * Displays the specified autor paper.
     * 
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        if ( ! isset($id))
        {
            show_404();
        }

        $data['autor_paper'] = $this->wildfire->find('autor_paper', $id);

        $this->load->view('autor_papers/show', $data);
    }

    /**
     * Validates the input retrieved from the view.
     * 
     * @return void
     */
    private function _set_form_validation()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('autor_id', 'Autor', 'required|greater_than[0]');
        $this->form_validation->set_rules('paper_id', 'Paper', 'required|greater_than[0]');
    }

}