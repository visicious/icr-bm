<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Paper Model
 *
 * @package  CodeIgniter
 * @category Model
 */
class Paper extends CI_Model {

    /**
     * @var integer
     */
    protected $_id;

    /**
     * @var string
     */
    protected $_titulo;

    /**
     * @var string
     */
    protected $_abstract;

    /**
     * @var string
     */
    protected $_trabajo;

    /**
     * Gets id
     *
     * @return integer
     */
    public function get_id()
    {
        return $this->_id;
    }

    /**
     * Gets id
     *
     * @param  integer
     * @return Paper
     */
    public function set_id($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Gets titulo
     *
     * @return string
     */
    public function get_titulo()
    {
        return $this->_titulo;
    }

    /**
     * Gets titulo
     *
     * @param  string
     * @return Paper
     */
    public function set_titulo($titulo)
    {
        $this->_titulo = $titulo;

        return $this;
    }

    /**
     * Gets abstract
     *
     * @return string
     */
    public function get_abstract()
    {
        return $this->_abstract;
    }

    /**
     * Gets abstract
     *
     * @param  string
     * @return Paper
     */
    public function set_abstract($abstract)
    {
        $this->_abstract = $abstract;

        return $this;
    }

    /**
     * Gets trabajo
     *
     * @return string
     */
    public function get_trabajo()
    {
        return $this->_trabajo;
    }

    /**
     * Gets trabajo
     *
     * @param  string
     * @return Paper
     */
    public function set_trabajo($trabajo)
    {
        $this->_trabajo = $trabajo;

        return $this;
    }

    /**
     * Saves the data to storage
     * 
     * @return boolean
     */
    public function save()
    {
        $data = array(
            'id' => $this->get_id(),
            'titulo' => $this->get_titulo(),
            'abstract' => $this->get_abstract(),
            'trabajo' => $this->get_trabajo(),
        );

        if ($this->_id > 0)
        {
            $this->db->where('id', $this->_id);

            if ($this->db->get('paper')->num_rows())
            {
                if ($this->db->update('paper', $data, array('id' => $this->_id)))
                {
                    return TRUE;
                }
            }
            else if ($this->db->insert('paper', $data))
            {
                return TRUE;
            }
        }
        else if ($this->db->insert('paper', $data))
        {
            $this->_id = $this->db->insert_id();
            
            return TRUE;
        }

        return FALSE;
    }

}