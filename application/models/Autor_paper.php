<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Autor Paper Model
 *
 * @package  CodeIgniter
 * @category Model
 */
class Autor_paper extends CI_Model {

    /**
     * @var integer
     */
    protected $_id;

    /**
     * @var integer
     */
    protected $_autor_id;

    /**
     * @var integer
     */
    protected $_paper_id;

    /**
     * Gets id
     *
     * @return integer
     */
    public function get_id()
    {
        return $this->_id;
    }

    /**
     * Gets id
     *
     * @param  integer
     * @return Autor_paper
     */
    public function set_id($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Gets autor id
     *
     * @return integer
     */
    public function get_autor_id()
    {
        return $this->_autor_id;
    }

    /**
     * Gets autor id
     *
     * @param  integer
     * @return Autor_paper
     */
    public function set_autor_id(\Autor $autor_id)
    {
        $this->_autor_id = $autor_id;

        return $this;
    }

    /**
     * Gets paper id
     *
     * @return integer
     */
    public function get_paper_id()
    {
        return $this->_paper_id;
    }

    /**
     * Gets paper id
     *
     * @param  integer
     * @return Autor_paper
     */
    public function set_paper_id(\Paper $paper_id)
    {
        $this->_paper_id = $paper_id;

        return $this;
    }

    /**
     * Saves the data to storage
     * 
     * @return boolean
     */
    public function save()
    {
        $data = array(
            'id' => $this->get_id(),
            'autor_id' => $this->get_autor_id()->get_id(),
            'paper_id' => $this->get_paper_id()->get_id(),
        );

        if ($this->_id > 0)
        {
            $this->db->where('id', $this->_id);

            if ($this->db->get('autor_paper')->num_rows())
            {
                if ($this->db->update('autor_paper', $data, array('id' => $this->_id)))
                {
                    return TRUE;
                }
            }
            else if ($this->db->insert('autor_paper', $data))
            {
                return TRUE;
            }
        }
        else if ($this->db->insert('autor_paper', $data))
        {
            $this->_id = $this->db->insert_id();
            
            return TRUE;
        }

        return FALSE;
    }

}