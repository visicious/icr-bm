<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Paper Topico Model
 *
 * @package  CodeIgniter
 * @category Model
 */
class Paper_topico extends CI_Model {

    /**
     * @var integer
     */
    protected $_id;

    /**
     * @var integer
     */
    protected $_topico_id;

    /**
     * @var integer
     */
    protected $_paper_id;

    /**
     * Gets id
     *
     * @return integer
     */
    public function get_id()
    {
        return $this->_id;
    }

    /**
     * Gets id
     *
     * @param  integer
     * @return Paper_topico
     */
    public function set_id($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Gets topico id
     *
     * @return integer
     */
    public function get_topico_id()
    {
        return $this->_topico_id;
    }

    /**
     * Gets topico id
     *
     * @param  integer
     * @return Paper_topico
     */
    public function set_topico_id(\Topico $topico_id)
    {
        $this->_topico_id = $topico_id;

        return $this;
    }

    /**
     * Gets paper id
     *
     * @return integer
     */
    public function get_paper_id()
    {
        return $this->_paper_id;
    }

    /**
     * Gets paper id
     *
     * @param  integer
     * @return Paper_topico
     */
    public function set_paper_id(\Paper $paper_id)
    {
        $this->_paper_id = $paper_id;

        return $this;
    }

    /**
     * Saves the data to storage
     * 
     * @return boolean
     */
    public function save()
    {
        $data = array(
            'id' => $this->get_id(),
            'topico_id' => $this->get_topico_id()->get_id(),
            'paper_id' => $this->get_paper_id()->get_id(),
        );

        if ($this->_id > 0)
        {
            $this->db->where('id', $this->_id);

            if ($this->db->get('paper_topico')->num_rows())
            {
                if ($this->db->update('paper_topico', $data, array('id' => $this->_id)))
                {
                    return TRUE;
                }
            }
            else if ($this->db->insert('paper_topico', $data))
            {
                return TRUE;
            }
        }
        else if ($this->db->insert('paper_topico', $data))
        {
            $this->_id = $this->db->insert_id();
            
            return TRUE;
        }

        return FALSE;
    }

    public function save_multiple($items = array())
    {
        $this->db->insert_batch('paper_topico', $items);
        return $this->db->affected_rows();
    }

}