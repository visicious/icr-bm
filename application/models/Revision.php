<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Revision Model
 *
 * @package  CodeIgniter
 * @category Model
 */
class Revision extends CI_Model {

    /**
     * @var integer
     */
    protected $_id;

    /**
     * @var integer
     */
    protected $_trabajo_id;

    /**
     * @var integer
     */
    protected $_arbitro_id;

    /**
     * @var float
     */
    protected $_evaluation;

    /**
     * Gets id
     *
     * @return integer
     */
    public function get_id()
    {
        return $this->_id;
    }

    /**
     * Gets id
     *
     * @param  integer
     * @return Revision
     */
    public function set_id($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Gets trabajo id
     *
     * @return integer
     */
    public function get_trabajo_id()
    {
        return $this->_trabajo_id;
    }

    /**
     * Gets trabajo id
     *
     * @param  integer
     * @return Revision
     */
    public function set_trabajo_id(\Paper $trabajo_id)
    {
        $this->_trabajo_id = $trabajo_id;

        return $this;
    }

    /**
     * Gets arbitro id
     *
     * @return integer
     */
    public function get_arbitro_id()
    {
        return $this->_arbitro_id;
    }

    /**
     * Gets arbitro id
     *
     * @param  integer
     * @return Revision
     */
    public function set_arbitro_id(\Arbitro $arbitro_id)
    {
        $this->_arbitro_id = $arbitro_id;

        return $this;
    }

    /**
     * Gets evaluation
     *
     * @return float
     */
    public function get_evaluation()
    {
        return $this->_evaluation;
    }

    /**
     * Gets evaluation
     *
     * @param  float
     * @return Revision
     */
    public function set_evaluation($evaluation)
    {
        $this->_evaluation = $evaluation;

        return $this;
    }

    /**
     * Saves the data to storage
     * 
     * @return boolean
     */
    public function save()
    {
        $data = array(
            'id' => $this->get_id(),
            'trabajo_id' => $this->get_trabajo_id()->get_id(),
            'arbitro_id' => $this->get_arbitro_id()->get_id(),
            'evaluation' => $this->get_evaluation(),
        );

        if ($this->_id > 0)
        {
            $this->db->where('id', $this->_id);

            if ($this->db->get('revision')->num_rows())
            {
                if ($this->db->update('revision', $data, array('id' => $this->_id)))
                {
                    return TRUE;
                }
            }
            else if ($this->db->insert('revision', $data))
            {
                return TRUE;
            }
        }
        else if ($this->db->insert('revision', $data))
        {
            $this->_id = $this->db->insert_id();
            
            return TRUE;
        }

        return FALSE;
    }

}