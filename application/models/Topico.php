<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Topico Model
 *
 * @package  CodeIgniter
 * @category Model
 */
class Topico extends CI_Model {

    /**
     * @var integer
     */
    protected $_id;

    /**
     * @var string
     */
    protected $_nombre;

    /**
     * Gets id
     *
     * @return integer
     */
    public function get_id()
    {
        return $this->_id;
    }

    /**
     * Gets id
     *
     * @param  integer
     * @return Topico
     */
    public function set_id($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Gets nombre
     *
     * @return string
     */
    public function get_nombre()
    {
        return $this->_nombre;
    }

    /**
     * Gets nombre
     *
     * @param  string
     * @return Topico
     */
    public function set_nombre($nombre)
    {
        $this->_nombre = $nombre;

        return $this;
    }

    /**
     * Saves the data to storage
     * 
     * @return boolean
     */
    public function save()
    {
        $data = array(
            'id' => $this->get_id(),
            'nombre' => $this->get_nombre(),
        );

        if ($this->_id > 0)
        {
            $this->db->where('id', $this->_id);

            if ($this->db->get('topico')->num_rows())
            {
                if ($this->db->update('topico', $data, array('id' => $this->_id)))
                {
                    return TRUE;
                }
            }
            else if ($this->db->insert('topico', $data))
            {
                return TRUE;
            }
        }
        else if ($this->db->insert('topico', $data))
        {
            $this->_id = $this->db->insert_id();
            
            return TRUE;
        }

        return FALSE;
    }

}