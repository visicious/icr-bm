<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Paper Arbitro Model
 *
 * @package  CodeIgniter
 * @category Model
 */
class Paper_arbitro extends CI_Model {

    /**
     * @var integer
     */
    protected $_id;

    /**
     * @var integer
     */
    protected $_arbitro_id;

    /**
     * @var integer
     */
    protected $_paper_id;

    /**
     * @var double
     */
    protected $_score;

    /**
     * Gets id
     *
     * @return integer
     */
    public function get_id()
    {
        return $this->_id;
    }

    /**
     * Gets id
     *
     * @param  integer
     * @return Paper_arbitro
     */
    public function set_id($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Gets arbitro id
     *
     * @return integer
     */
    public function get_arbitro_id()
    {
        return $this->_arbitro_id;
    }

    /**
     * Gets arbitro id
     *
     * @param  integer
     * @return Paper_arbitro
     */
    public function set_arbitro_id(\Arbitro $arbitro_id)
    {
        $this->_arbitro_id = $arbitro_id;

        return $this;
    }

    /**
     * Gets paper id
     *
     * @return integer
     */
    public function get_paper_id()
    {
        return $this->_paper_id;
    }

    /**
     * Gets paper id
     *
     * @param  integer
     * @return Paper_arbitro
     */
    public function set_paper_id(\Paper $paper_id)
    {
        $this->_paper_id = $paper_id;

        return $this;
    }

    /**
     * Gets score
     *
     * @return double
     */
    public function get_score()
    {
        return $this->_score;
    }

    /**
     * Gets score
     *
     * @param  double
     * @return Paper_arbitro
     */
    public function set_score($score)
    {
        $this->_score = $score;

        return $this;
    }

    /**
     * Saves the data to storage
     * 
     * @return boolean
     */
    public function save()
    {
        $data = array(
            'id' => $this->get_id(),
            'arbitro_id' => $this->get_arbitro_id()->get_id(),
            'paper_id' => $this->get_paper_id()->get_id(),
            'score' => $this->get_score(),
        );

        if ($this->_id > 0)
        {
            $this->db->where('id', $this->_id);

            if ($this->db->get('paper_arbitro')->num_rows())
            {
                if ($this->db->update('paper_arbitro', $data, array('id' => $this->_id)))
                {
                    return TRUE;
                }
            }
            else if ($this->db->insert('paper_arbitro', $data))
            {
                return TRUE;
            }
        }
        else if ($this->db->insert('paper_arbitro', $data))
        {
            $this->_id = $this->db->insert_id();
            
            return TRUE;
        }

        return FALSE;
    }

    public function save_multiple($items = array())
    {
        $this->db->insert_batch('paper_topico', $items);
        return $this->db->affected_rows();
    }
}