<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Arbitro Model
 *
 * @package  CodeIgniter
 * @category Model
 */
class Arbitro extends CI_Model {

    /**
     * @var integer
     */
    protected $_id;

    /**
     * @var string
     */
    protected $_nombre;

    /**
     * @var string
     */
    protected $_especialidad;

    /**
     * @var string
     */
    protected $_correo;

    /**
     * @var string
     */
    protected $_universidad;

    /**
     * @var string
     */
    protected $_perfil;
    protected $_dni;
    protected $_fecha_nacimiento;
    protected $_telefono;
    protected $_nacionalidad;
    protected $_pagina_web;

    /**
     * Gets id
     *
     * @return integer
     */
    public function get_id()
    {
        return $this->_id;
    }

    /**
     * Gets id
     *
     * @param  integer
     * @return Arbitro
     */
    public function set_id($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Gets nombre
     *
     * @return string
     */
    public function get_nombre()
    {
        return $this->_nombre;
    }

    /**
     * Gets nombre
     *
     * @param  string
     * @return Arbitro
     */
    public function set_nombre($nombre)
    {
        $this->_nombre = $nombre;

        return $this;
    }

    /**
     * Gets especialidad
     *
     * @return string
     */
    public function get_especialidad()
    {
        return $this->_especialidad;
    }

    /**
     * Gets especialidad
     *
     * @param  string
     * @return Arbitro
     */
    public function set_especialidad($especialidad)
    {
        $this->_especialidad = $especialidad;

        return $this;
    }

    /**
     * Gets correo
     *
     * @return string
     */
    public function get_correo()
    {
        return $this->_correo;
    }

    /**
     * Gets correo
     *
     * @param  string
     * @return Arbitro
     */
    public function set_correo($correo)
    {
        $this->_correo = $correo;

        return $this;
    }

    /**
     * Gets universidad
     *
     * @return string
     */
    public function get_universidad()
    {
        return $this->_universidad;
    }

    /**
     * Gets universidad
     *
     * @param  string
     * @return Arbitro
     */
    public function set_universidad($universidad)
    {
        $this->_universidad = $universidad;

        return $this;
    }

    /**
     * Gets perfil
     *
     * @return string
     */
    public function get_perfil()
    {
        return $this->_perfil;
    }

    /**
     * Gets perfil
     *
     * @param  string
     * @return Arbitro
     */
    public function set_perfil($perfil)
    {
        $this->_perfil = $perfil;

        return $this;
    }

    public function set_dni($dni)
    {
        $this->_dni = $dni;

        return $this;
    }

    public function set_fecha_nacimiento($fecha_nacimiento)
    {
        $this->_fecha_nacimiento = $fecha_nacimiento;

        return $this;
    }

    public function set_telefono($telefono)
    {
        $this->_telefono = $telefono;

        return $this;
    }

    public function set_nacionalidad($nacionalidad)
    {
        $this->_nacionalidad = $nacionalidad;

        return $this;
    }

    public function get_dni()
    {
        return $this->_dni;
    }

    public function get_fecha_nacimiento()
    {
        return $this->_fecha_nacimiento;
    }

    public function get_telefono()
    {
        return $this->_telefono;
    }

    public function get_nacionalidad()
    {
        return $this->_nacionalidad;
    }

    public function set_pagina_web($pagina_web)
    {
        $this->_pagina_web = $pagina_web;

        return $this;
    }

    public function get_pagina_web()
    {
        return $this->_pagina_web;
    }

    /**
     * Saves the data to storage
     * 
     * @return boolean
     */
    public function save()
    {
        $data = array(
            'id' => $this->get_id(),
            'nombre' => $this->get_nombre(),
            'especialidad' => $this->get_especialidad(),
            'correo' => $this->get_correo(),
            'universidad' => $this->get_universidad(),
            'perfil' => $this->get_perfil(),
            'dni' => $this->get_dni(),
            'fecha_nacimiento' => $this->get_fecha_nacimiento(),
            'telefono' => $this->get_telefono(),
            'nacionalidad' => $this->get_nacionalidad(),
            'pagina_web' => $this->get_pagina_web(),
        );

        if ($this->_id > 0)
        {
            $this->db->where('id', $this->_id);

            if ($this->db->get('arbitro')->num_rows())
            {
                if ($this->db->update('arbitro', $data, array('id' => $this->_id)))
                {
                    return TRUE;
                }
            }
            else if ($this->db->insert('arbitro', $data))
            {
                return TRUE;
            }
        }
        else if ($this->db->insert('arbitro', $data))
        {
            $this->_id = $this->db->insert_id();
            
            return TRUE;
        }

        return FALSE;
    }

}