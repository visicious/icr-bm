<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Arbitro Topico Model
 *
 * @package  CodeIgniter
 * @category Model
 */
class Arbitro_topico extends CI_Model {

    /**
     * @var integer
     */
    protected $_id;

    /**
     * @var integer
     */
    protected $_arbitro_id;

    /**
     * @var integer
     */
    protected $_topico_id;
    protected $_decision;

    /**
     * Gets id
     *
     * @return integer
     */
    public function get_id()
    {
        return $this->_id;
    }

    /**
     * Sets id
     *
     * @param  integer
     * @return Arbitro_topico
     */
    public function set_id($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Gets decision
     *
     * @return integer
     */
    public function get_decision()
    {
        return $this->_decision;
    }

    /**
     * Sets decision
     *
     * @param  integer
     * @return Arbitro_topico
     */
    public function set_decision($decision)
    {
        $this->_decision = $decision;

        return $this;
    }

    /**
     * Gets arbitro id
     *
     * @return integer
     */
    public function get_arbitro_id()
    {
        return $this->_arbitro_id;
    }

    /**
     * Gets arbitro id
     *
     * @param  integer
     * @return Arbitro_topico
     */
    public function set_arbitro_id(\Arbitro $arbitro_id)
    {
        $this->_arbitro_id = $arbitro_id;

        return $this;
    }

    /**
     * Gets topico id
     *
     * @return integer
     */
    public function get_topico_id()
    {
        return $this->_topico_id;
    }

    /**
     * Gets topico id
     *
     * @param  integer
     * @return Arbitro_topico
     */
    public function set_topico_id(\Topico $topico_id)
    {
        $this->_topico_id = $topico_id;

        return $this;
    }

    /**
     * Saves the data to storage
     * 
     * @return boolean
     */
    public function save()
    {
        $data = array(
            'id' => $this->get_id(),
            'arbitro_id' => $this->get_arbitro_id()->get_id(),
            'topico_id' => $this->get_topico_id()->get_id(),
            'decision' => $this->get_decision(),
        );

        if ($this->_id > 0)
        {
            $this->db->where('id', $this->_id);

            if ($this->db->get('arbitro_topico')->num_rows())
            {
                if ($this->db->update('arbitro_topico', $data, array('id' => $this->_id)))
                {
                    return TRUE;
                }
            }
            else if ($this->db->insert('arbitro_topico', $data))
            {
                return TRUE;
            }
        }
        else if ($this->db->insert('arbitro_topico', $data))
        {
            $this->_id = $this->db->insert_id();
            
            return TRUE;
        }

        return FALSE;
    }

    public function save_multiple($items = array())
    {
        $this->db->insert_batch('arbitro_topico', $items);
        return $this->db->affected_rows();
    }


}