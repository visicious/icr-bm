<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Arbitro Tema Model
 *
 * @package  CodeIgniter
 * @category Model
 */
class Arbitro_tema extends CI_Model {

    /**
     * @var integer
     */
    protected $_id;

    /**
     * @var integer
     */
    protected $_arbitro_id;

    /**
     * @var integer
     */
    protected $_tema_id;
    protected $_decision;

    /**
     * Gets id
     *
     * @return integer
     */
    public function get_id()
    {
        return $this->_id;
    }

    /**
     * Gets id
     *
     * @param  integer
     * @return Arbitro_tema
     */
    public function set_id($id)
    {
        $this->_id = $id;

        return $this;
    }


    public function get_decision()
    {
        return $this->_decision;
    }

    
    public function set_decision($decision)
    {
        $this->_decision = $decision;

        return $this;
    }


    /**
     * Gets arbitro id
     *
     * @return integer
     */
    public function get_arbitro_id()
    {
        return $this->_arbitro_id;
    }

    /**
     * Gets arbitro id
     *
     * @param  integer
     * @return Arbitro_tema
     */
    public function set_arbitro_id(\Arbitro $arbitro_id)
    {
        $this->_arbitro_id = $arbitro_id;

        return $this;
    }

    /**
     * Gets tema id
     *
     * @return integer
     */
    public function get_tema_id()
    {
        return $this->_tema_id;
    }

    /**
     * Gets tema id
     *
     * @param  integer
     * @return Arbitro_tema
     */
    public function set_tema_id(\Tema $tema_id)
    {
        $this->_tema_id = $tema_id;

        return $this;
    }

    /**
     * Saves the data to storage
     * 
     * @return boolean
     */
    public function save()
    {
        $data = array(
            'id' => $this->get_id(),
            'arbitro_id' => $this->get_arbitro_id()->get_id(),
            'tema_id' => $this->get_tema_id()->get_id(),
            'decision' => $this->get_decision(),
        );

        if ($this->_id > 0)
        {
            $this->db->where('id', $this->_id);

            if ($this->db->get('arbitro_tema')->num_rows())
            {
                if ($this->db->update('arbitro_tema', $data, array('id' => $this->_id)))
                {
                    return TRUE;
                }
            }
            else if ($this->db->insert('arbitro_tema', $data))
            {
                return TRUE;
            }
        }
        else if ($this->db->insert('arbitro_tema', $data))
        {
            $this->_id = $this->db->insert_id();
            
            return TRUE;
        }

        return FALSE;
    }

    public function save_multiple($items = array())
    {
        $this->db->insert_batch('arbitro_tema', $items);
        return $this->db->affected_rows();
    }


}