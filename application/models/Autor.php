<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Autor Model
 *
 * @package  CodeIgniter
 * @category Model
 */
class Autor extends CI_Model {

    /**
     * @var integer
     */
    protected $_id;

    /**
     * @var string
     */
    protected $_nombre;

    /**
     * @var string
     */
    protected $_correo;

    /**
     * @var string
     */
    protected $_universidad;

    /**
     * @var string
     */
    protected $_nacionalidad;

    /**
     * @var string
     */
    protected $_categoria;
    protected $_dni;
    protected $_fecha_nacimiento;
    protected $_telefono;
    protected $_pagina_web;
    protected $_has_doc_tesis;

    /**
     * Gets id
     *
     * @return integer
     */
    public function get_id()
    {
        return $this->_id;
    }

    /**
     * Gets id
     *
     * @param  integer
     * @return Autor
     */
    public function set_id($id)
    {
        $this->_id = $id;

        return $this;
    }

    public function get_dni()
    {
        return $this->_dni;
    }

    public function set_dni($dni)
    {
        $this->_dni = $dni;

        return $this;
    }

    public function get_has_doc_tesis()
    {
        return $this->_has_doc_tesis;
    }

    public function set_has_doc_tesis($has_doc_tesis)
    {
        $this->_has_doc_tesis = $has_doc_tesis;

        return $this;
    }

    public function get_fecha_nacimiento()
    {
        return $this->_fecha_nacimiento;
    }
    
    public function set_fecha_nacimiento($fecha_nacimiento)
    {
        $this->_fecha_nacimiento = $fecha_nacimiento;

        return $this;
    }

    public function get_telefono()
    {
        return $this->_telefono;
    }
    
    public function set_telefono($telefono)
    {
        $this->_telefono = $telefono;

        return $this;
    }

    public function get_pagina_web()
    {
        return $this->_pagina_web;
    }
    
    public function set_pagina_web($pagina_web)
    {
        $this->_pagina_web = $pagina_web;

        return $this;
    }

    /**
     * Gets nombre
     *
     * @return string
     */
    public function get_nombre()
    {
        return $this->_nombre;
    }

    /**
     * Gets nombre
     *
     * @param  string
     * @return Autor
     */
    public function set_nombre($nombre)
    {
        $this->_nombre = $nombre;

        return $this;
    }

    /**
     * Gets correo
     *
     * @return string
     */
    public function get_correo()
    {
        return $this->_correo;
    }

    /**
     * Gets correo
     *
     * @param  string
     * @return Autor
     */
    public function set_correo($correo)
    {
        $this->_correo = $correo;

        return $this;
    }

    /**
     * Gets universidad
     *
     * @return string
     */
    public function get_universidad()
    {
        return $this->_universidad;
    }

    /**
     * Gets universidad
     *
     * @param  string
     * @return Autor
     */
    public function set_universidad($universidad)
    {
        $this->_universidad = $universidad;

        return $this;
    }

    /**
     * Gets nacionalidad
     *
     * @return string
     */
    public function get_nacionalidad()
    {
        return $this->_nacionalidad;
    }

    /**
     * Gets nacionalidad
     *
     * @param  string
     * @return Autor
     */
    public function set_nacionalidad($nacionalidad)
    {
        $this->_nacionalidad = $nacionalidad;

        return $this;
    }

    /**
     * Gets categoria
     *
     * @return string
     */
    public function get_categoria()
    {
        return $this->_categoria;
    }

    /**
     * Gets categoria
     *
     * @param  string
     * @return Autor
     */
    public function set_categoria($categoria)
    {
        $this->_categoria = $categoria;

        return $this;
    }

 

    /**
     * Saves the data to storage
     * 
     * @return boolean
     */
    public function save()
    {
        $data = array(
            'id' => $this->get_id(),
            'nombre' => $this->get_nombre(),
            'correo' => $this->get_correo(),
            'universidad' => $this->get_universidad(),
            'nacionalidad' => $this->get_nacionalidad(),
            'categoria' => $this->get_categoria(),
            'dni' => $this->get_dni(),
            'fecha_nacimiento' => $this->get_fecha_nacimiento(),
            'telefono' => $this->get_telefono(),
            'pagina_web' => $this->get_pagina_web(),
            'has_doc_tesis' => $this->get_has_doc_tesis(),
        );

        if ($this->_id > 0)
        {
            $this->db->where('id', $this->_id);

            if ($this->db->get('autor')->num_rows())
            {
                if ($this->db->update('autor', $data, array('id' => $this->_id)))
                {
                    return TRUE;
                }
            }
            else if ($this->db->insert('autor', $data))
            {
                return TRUE;
            }
        }
        else if ($this->db->insert('autor', $data))
        {
            $this->_id = $this->db->insert_id();
            
            return TRUE;
        }

        return FALSE;
    }

}