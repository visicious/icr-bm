<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Ion Auth Lang - English
*
* Author: Ben Edmunds
*         ben.edmunds@gmail.com
*         @benedmunds
*
* Location: http://github.com/benedmunds/ion_auth/
*
* Created:  03.14.2010
*
* Description:  English language file for Ion Auth messages and errors
*
*/

// Account Creation
$lang['account_creation_successful']            = 'Cuenta creada con éxito';
$lang['account_creation_unsuccessful']          = '<strong style="color: white;background-color:red;padding:6px;">No se puede crear una cuenta</strong>';
$lang['account_creation_duplicate_email']       = '<strong style="color: white;background-color:red;padding:6px;">Correo electrónico ya usado o no válido</strong>';
$lang['account_creation_duplicate_identity']    = '<strong style="color: white;background-color:red;padding:6px;">Identidad ya utilizada o no válida</strong>';
$lang['account_creation_missing_default_group'] = '<strong style="color: white;background-color:red;padding:6px;">El grupo predeterminado no está configurado</strong>';
$lang['account_creation_invalid_default_group'] = '<strong style="color: white;background-color:red;padding:6px;">Conjunto de grupos predeterminados no válidos</strong>';


// Password
$lang['password_change_successful']          = 'Contraseña cambiada correctamente';
$lang['password_change_unsuccessful']        = '<strong style="color: white;background-color:red;padding:6px;">No se puede cambiar la contraseña</strong>';
$lang['forgot_password_successful']          = 'Correo electrónico de restablecimiento de contraseña enviado';
$lang['forgot_password_unsuccessful']        = '<strong style="color: white;background-color:red;padding:6px;">No se pudo enviar el enlace para restablecer la contraseña</strong>';

// Activation
$lang['activate_successful']                 = 'Cuenta activada';
$lang['activate_unsuccessful']               = '<strong style="color: white;background-color:red;padding:6px;">No se puede activar la cuenta</strong>';
$lang['deactivate_successful']               = '<strong style="color: white;background-color:red;padding:6px;">Cuenta desactivada</strong>';
$lang['deactivate_unsuccessful']             = '<strong style="color: white;background-color:red;padding:6px;">Incapaz de desactivar la cuenta</strong>';
$lang['activation_email_successful']         = 'Correo electrónico de activación enviado. Por favor revise su bandeja de entrada o correo no deseado';
$lang['activation_email_unsuccessful']       = '<strong style="color: white;background-color:red;padding:6px;">No se puede enviar correo electrónico de activación</strong>';
$lang['deactivate_current_user_unsuccessful']= '<strong style="color: white;background-color:red;padding:6px;">No puedes desactivarte a ti mismo</strong>';

// Login / Logout
$lang['login_successful']                    = 'Inicio de sesión exitoso';
$lang['login_unsuccessful']                  = '<strong style="color: white;background-color:red;padding:6px;">Ingreso incorrecto</strong>';
$lang['login_unsuccessful_not_active']       = '<strong style="color: white;background-color:red;padding:6px;">La cuenta está inactiva</strong>';
$lang['login_timeout']                       = '<strong style="color: white;background-color:red;padding:6px;">Temporalmente bloqueado. Inténtalo más tarde.</strong>';
$lang['logout_successful']                   = 'Desconectado con éxito';

// Account Changes
$lang['update_successful']                   = 'Información de cuenta actualizada con éxito';
$lang['update_unsuccessful']                 = '<strong style="color: white;background-color:red;padding:6px;">No se puede actualizar la información de la cuenta</strong>';
$lang['delete_successful']                   = 'Usuario eliminiado';
$lang['delete_unsuccessful']                 = '<strong style="color: white;background-color:red;padding:6px;">No se puede eliminar al usuario</strong>';

// Groups
$lang['group_creation_successful']           = 'Grupo creado con éxito';
$lang['group_already_exists']                = '<strong style="color: white;background-color:red;padding:6px;">Nombre del grupo ya existente</strong>';
$lang['group_update_successful']             = 'Detalles del grupo actualizados';
$lang['group_delete_successful']             = '<strong style="color: white;background-color:red;padding:6px;">Grupo eliminado</strong>';
$lang['group_delete_unsuccessful']           = '<strong style="color: white;background-color:red;padding:6px;">No se puede borrar el grupo</strong>';
$lang['group_delete_notallowed']             = '<strong style="color: white;background-color:red;padding:6px;">No se puede eliminar el grupo de administradores</strong>';
$lang['group_name_required']                 = '<strong style="color: white;background-color:red;padding:6px;">El nombre del grupo es un campo obligatorio</strong>';
$lang['group_name_admin_not_alter']          = '<strong style="color: white;background-color:red;padding:6px;">El nombre del grupo de administración no se puede cambiar</strong>';

// Activation Email
$lang['email_activation_subject']            = 'Activación de cuenta';
$lang['email_activate_heading']              = 'Activar cuenta para %s';
$lang['email_activate_subheading']           = 'Por favor haga clic en este enlace para %s.';
$lang['email_activate_link']                 = 'Activa tu cuenta';

// Forgot Password Email
$lang['email_forgotten_password_subject']    = 'Olvidó la verificación de contraseña';
$lang['email_forgot_password_heading']       = 'Restablecer contraseña para %s';
$lang['email_forgot_password_subheading']    = 'Por favor haga clic en este enlace para %s.';
$lang['email_forgot_password_link']          = 'Restablecer su contraseña';

// New Password Email
$lang['email_new_password_subject']          = 'Nueva contraseña';
$lang['email_new_password_heading']          = 'Nueva contraseña para %s';
$lang['email_new_password_subheading']       = 'Su contraseña ha sido restablecida a: %s';
