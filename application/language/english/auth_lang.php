<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Auth Lang - English
*
* Author: Ben Edmunds
* 		  ben.edmunds@gmail.com
*         @benedmunds
*
* Author: Daniel Davis
*         @ourmaninjapan
*
* Location: http://github.com/benedmunds/ion_auth/
*
* Created:  03.09.2013
*
* Description:  English language file for Ion Auth example views
*
*/

// Errors
$lang['error_csrf'] = 'Este formulario no pasó nuestros controles de seguridad.';

// Login
$lang['login_heading']         = 'Ingreso';
$lang['login_subheading']      = 'Inicie sesión con su correo electrónico y contraseña a continuación.';
$lang['login_identity_label']  = 'Correo electrónico:';
$lang['login_password_label']  = 'Contraseña:';
$lang['login_remember_label']  = 'Recuérdame:';
$lang['login_submit_btn']      = 'Ingreso';
$lang['login_forgot_password'] = '¿Olvidaste tu contraseña?';
$lang['login_create_user_link']  = 'Registrarse';

// Index
$lang['index_heading']           = 'Usuarios';
$lang['index_subheading']        = 'A continuación hay la lista de los usuarios.';
$lang['index_fname_th']          = 'Nombres';
$lang['index_lname_th']          = 'Apellidos';
$lang['index_email_th']          = 'Correo Electrónico';
$lang['index_groups_th']         = 'Grupos';
$lang['index_status_th']         = 'Estado';
$lang['index_action_th']         = 'Acción';
$lang['index_active_link']       = 'Activo';
$lang['index_inactive_link']     = 'Inactivo';
$lang['index_create_user_link']  = 'Crear un nuevo usuario';
$lang['index_create_group_link'] = 'Crea un nuevo grupo';

// Deactivate User
$lang['deactivate_heading']                  = 'Desactivar Usuario';
$lang['deactivate_subheading']               = '¿Seguro que quieres desactivar el usuario \'%s\' ?';
$lang['deactivate_confirm_y_label']          = 'Si:';
$lang['deactivate_confirm_n_label']          = 'No:';
$lang['deactivate_submit_btn']               = 'Enviar';
$lang['deactivate_validation_confirm_label'] = 'confirmación';
$lang['deactivate_validation_user_id_label'] = 'ID de usuario';

// Create User
$lang['create_user_heading']                           = 'Inscripción';
$lang['create_user_subheading']                        = 'Por favor ingrese la información requerida a continuación.';
$lang['create_user_fname_label']                       = 'Nombres:';
$lang['create_user_lname_label']                       = 'Apellidos:';
$lang['create_user_company_label']                     = 'Institución o Universidad:';
$lang['create_user_identity_label']                    = 'Identidad:';
$lang['create_user_email_label']                       = 'Correo Electrónico:';
$lang['create_user_phone_label']                       = 'Teléfono de Contacto:';
$lang['create_user_password_label']                    = 'Contraseña:';
$lang['create_user_password_confirm_label']            = 'Confirmar Contraseña:';
$lang['create_user_submit_btn']                        = 'Crear Usuario';
$lang['create_user_validation_fname_label']            = 'Nombres';
$lang['create_user_validation_lname_label']            = 'Apellidos';
$lang['create_user_validation_identity_label']         = 'Identidad';
$lang['create_user_validation_email_label']            = 'Correo Electrónico';
$lang['create_user_validation_phone_label']            = 'Teléfono de Contacto';
$lang['create_user_validation_company_label']          = 'Institución';
$lang['create_user_validation_password_label']         = 'Contraseña';
$lang['create_user_validation_password_confirm_label'] = 'Confirmar Contraseña';

// Edit User
$lang['edit_user_heading']                           = 'Editar Usuario';
$lang['edit_user_subheading']                        = 'Por favor ingrese la información del usuario a continuación.';
$lang['edit_user_fname_label']                       = 'Nombres:';
$lang['edit_user_lname_label']                       = 'Apellidos:';
$lang['edit_user_company_label']                     = 'Institución:';
$lang['edit_user_email_label']                       = 'Correo Electrónico:';
$lang['edit_user_phone_label']                       = 'Teléfono de Contacto:';
$lang['edit_user_password_label']                    = 'Contraseña: (para cambiar la contraseña)';
$lang['edit_user_password_confirm_label']            = 'Confirmar Contraseña: (para cambiar la contraseña)';
$lang['edit_user_groups_heading']                    = 'Miembro de Grupos';
$lang['edit_user_submit_btn']                        = 'Guardar Usuario';
$lang['edit_user_validation_fname_label']            = 'Nombres';
$lang['edit_user_validation_lname_label']            = 'Apellidos';
$lang['edit_user_validation_email_label']            = 'Correo Electrónico';
$lang['edit_user_validation_phone_label']            = 'Teléfono de Contacto';
$lang['edit_user_validation_company_label']          = 'Institución';
$lang['edit_user_validation_groups_label']           = 'Grupos';
$lang['edit_user_validation_password_label']         = 'Contraseña';
$lang['edit_user_validation_password_confirm_label'] = 'Confirmar Contraseña';

// Create Group
$lang['create_group_title']                  = 'Crear Grupo';
$lang['create_group_heading']                = 'Crear Grupo';
$lang['create_group_subheading']             = 'Por favor ingrese la información del grupo a continuación.';
$lang['create_group_name_label']             = 'Nombre del Grupo:';
$lang['create_group_desc_label']             = 'Descripción:';
$lang['create_group_submit_btn']             = 'Crear Grupo';
$lang['create_group_validation_name_label']  = 'Nombre del Grupo';
$lang['create_group_validation_desc_label']  = 'Descripción';

// Edit Group
$lang['edit_group_title']                  = 'Editar Grupo';
$lang['edit_group_saved']                  = 'Grupo Guardado';
$lang['edit_group_heading']                = 'Editar Grupo';
$lang['edit_group_subheading']             = 'Por favor ingrese la información del grupo a continuación.';
$lang['edit_group_name_label']             = 'Nombre del Grupo:';
$lang['edit_group_desc_label']             = 'Descripción:';
$lang['edit_group_submit_btn']             = 'Guardar Grupo';
$lang['edit_group_validation_name_label']  = 'Nombre del Grupo';
$lang['edit_group_validation_desc_label']  = 'Descripción';

// Change Password
$lang['change_password_heading']                               = 'Cambiar Contraseña';
$lang['change_password_old_password_label']                    = 'Contraseña Anterior:';
$lang['change_password_new_password_label']                    = 'Nueva Contraseña (de más de %s caracteres):';
$lang['change_password_new_password_confirm_label']            = 'Confirmar Nueva Contraseña:';
$lang['change_password_submit_btn']                            = 'Cambiar';
$lang['change_password_validation_old_password_label']         = 'Contraseña Anterior';
$lang['change_password_validation_new_password_label']         = 'Nueva Contraseña';
$lang['change_password_validation_new_password_confirm_label'] = 'Confirmar Nueva Contraseña';

// Forgot Password
$lang['forgot_password_heading']                 = 'Recuperar su contraseña';
$lang['forgot_password_subheading']              = 'Ingrese su %s para que podamos enviarle un correo para restablecer su contraseña.';
$lang['forgot_password_email_label']             = '%s:';
$lang['forgot_password_submit_btn']              = 'Enviar';
$lang['forgot_password_validation_email_label']  = 'Correo Electrónico';
$lang['forgot_password_identity_label'] = 'Identidad';
$lang['forgot_password_email_identity_label']    = 'Correo Electrónico';
$lang['forgot_password_email_not_found']         = 'Dirección de correo electrónico incorrecta.';
$lang['forgot_password_identity_not_found']         = 'Nombre de usuario incorrecto.';

// Reset Password
$lang['reset_password_heading']                               = 'Cambiar Contraseña';
$lang['reset_password_new_password_label']                    = 'Nueva Contraseña (de más de %s caracteres):';
$lang['reset_password_new_password_confirm_label']            = 'Confirmar Nueva Contraseña:';
$lang['reset_password_submit_btn']                            = 'Cambiar';
$lang['reset_password_validation_new_password_label']         = 'Nueva Contraseña';
$lang['reset_password_validation_new_password_confirm_label'] = 'Confirmar Nueva Contraseña';
