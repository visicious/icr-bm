<?php

$lang['required']			= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s es obligatorio.</strong>";
$lang['isset']				= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s debe contener un valor.</strong>";
$lang['valid_email']		= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s debe contener una dirección de correo válida.</strong>";
$lang['valid_emails']		= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s debe contener todas las direcciones de correo válidas.</strong>";
$lang['valid_url']			= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s debe contener una URL válida.</strong>";
$lang['valid_ip']			= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s debe contener una dirección IP válida.</strong>";
$lang['min_length']			= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s debe contener al menos %s caracteres de longitud.</strong>";
$lang['max_length']			= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s no debe exceder los %s caracteres de longitud.</strong>";
$lang['exact_length']		= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s debe tener exactamente %s carácteres.</strong>";
$lang['alpha']				= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s sólo puede contener carácteres alfabéticos.</strong>";
$lang['alpha_numeric']		= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s sólo puede contener carácteres alfanuméricos.</strong>";
$lang['alpha_dash']			= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s sólo puede contener carácteres alfanuméricos, guiones bajos '_' o guiones '-'.</strong>";
$lang['numeric']			= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s sólo puede contener números.</strong>";
$lang['is_numeric']			= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s sólo puede contener carácteres numéricos.</strong>";
$lang['integer']			= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s debe contener un número entero.</strong>";
$lang['regex_match']		= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s no tiene el formato correcto.</strong>";
$lang['matches']			= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s no concuerda con el campo %s .</strong>";
$lang['is_natural']			= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s debe contener sólo números positivos.</strong>";
$lang['is_natural_no_zero']	= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s debe contener un número mayor que 0.</strong>";
$lang['decimal']			= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s debe contener un número decimal.</strong>";
$lang['less_than']			= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s debe contener un número menor que %s.</strong>";
$lang['greater_than']		= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s debe contener un número mayor que %s.</strong>";
/* Added after 2.0.2 */
$lang['is_unique'] 			= '<strong style="color: white;background-color:red;padding:6px;">'."El campo %s debe contener un valor único.</strong>";

/* End of file form_validation_lang.php */
/* Location: ./system/language/spanish/form_validation_lang.php */