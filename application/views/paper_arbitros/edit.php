<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Paper_Arbitros
	</h1>
	<?php echo form_open('paper_arbitros/edit/' . $paper_arbitro->get_id(), 'class=""'); ?>
		<div class="text-right">
			<button type="submit" class="btn btn-primary">
				Submit
			</button>
			<a class="btn btn-default" href="<?php echo base_url('paper_arbitros'); ?>">
				Cancel
			</a>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Arbitro', 'arbitro_id', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_dropdown('arbitro_id', $arbitros, set_value('arbitro_id', $paper_arbitro->get_arbitro_id()->get_id()), 'class="form-control" required'); ?>
				<?php echo form_error('arbitro_id'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Paper', 'paper_id', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_dropdown('paper_id', $papers, set_value('paper_id', $paper_arbitro->get_paper_id()->get_id()), 'class="form-control" required'); ?>
				<?php echo form_error('paper_id'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Score', 'score', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('score', set_value('score', $paper_arbitro->get_score()), 'class="form-control" required'); ?>
				<?php echo form_error('score'); ?>
			</div>
		</div>
	<?php echo form_close(); ?>
<?php $this->load->view('layout/footer'); ?>