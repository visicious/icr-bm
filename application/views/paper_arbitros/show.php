<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Paper_Arbitros
	</h1>
	<div class="text-right">
		<a class="btn btn-default" href="<?php echo base_url('paper_arbitros'); ?>">
			Cancel
		</a>
	</div>
	<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?php echo form_label('Arbitros', 'arbitro_id', array('class' => 'control-label')); ?>
		<div class="">
			<input type="text" class="form-control" value="<?php echo $paper_arbitro->get_arbitro_id()->get_id(); ?>" disabled>
		</div>
	</div>
	<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?php echo form_label('Papers', 'paper_id', array('class' => 'control-label')); ?>
		<div class="">
			<input type="text" class="form-control" value="<?php echo $paper_arbitro->get_paper_id()->get_id(); ?>" disabled>
		</div>
	</div>
	<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?php echo form_label('Score', 'score', array('class' => 'control-label')); ?>
		<div class="">
			<input type="text" class="form-control" value="<?php echo $paper_arbitro->get_score(); ?>" disabled>
		</div>
	</div>
<?php $this->load->view('layout/footer'); ?>