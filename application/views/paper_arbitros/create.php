<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Asignar Arbitros
	</h1>
	<?php echo form_open('paper_arbitros/create', 'class=""'); ?>
		
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Arbitro', 'arbitro_id', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_dropdown('arbitro_id', $arbitros, set_value('arbitro_id'), 'class="form-control" required'); ?>
				<?php echo form_error('arbitro_id'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Paper', 'paper_id', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_dropdown('paper_id', $papers, set_value('paper_id'), 'class="form-control" required'); ?>
				<?php echo form_error('paper_id'); ?>
			</div>
		</div>
		<br />
		<div class="text-right">
			<button type="submit" class="btn btn-primary">
				Asignar
			</button>
			<a class="btn btn-default" href="<?php echo base_url('index.php/dashboard'); ?>">
				Cancelar
			</a>
		</div>
	<?php echo form_close(); ?>
<?php $this->load->view('layout/footer'); ?>