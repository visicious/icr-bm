<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Paper_Arbitros
	</h1>
	<div class="text-right">
		<a class="btn btn-default" href="<?php echo base_url('paper_arbitros/create'); ?>">
			Create A New Paper_Arbitro
		</a>
	</div>
	<?php if ($paper_arbitros): ?>
		<table class="table table table-striped table-hover">
			<thead>
				<tr>
					<td>Arbitro</td>
					<td>Paper</td>
					<td>Score</td>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($paper_arbitros as $paper_arbitro): ?>
					<tr>
						<td><?php echo $paper_arbitro->get_arbitro_id()->get_id(); ?></td>
						<td><?php echo $paper_arbitro->get_paper_id()->get_id(); ?></td>
						<td><?php echo $paper_arbitro->get_score(); ?></td>
						<td>
							<a href="<?php echo base_url('paper_arbitros/edit/' . $paper_arbitro->get_id()); ?>">
								<i class="fa fa-edit fa-2x"></i>
							</a>
							<a href="<?php echo base_url('paper_arbitros/delete/' . $paper_arbitro->get_id()); ?>">
								<i class="fa fa-trash fa-2x"></i>
							</a>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php echo $links; ?>
	<?php elseif ($this->input->get('keyword')): ?>
		Your search - <b><?php echo $this->input->get('keyword') ?></b> - did not match any paper arbitros.
	<?php else: ?>
		There are no paper arbitros that are currently available.
	<?php endif; ?>
<?php $this->load->view('layout/footer'); ?>