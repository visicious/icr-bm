<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Arbitros
	</h1>
	<?php echo form_open('arbitros/edit/' . $arbitro->get_id(), 'class=""'); ?>
		<div class="text-right">
			<button type="submit" class="btn btn-primary">
				Submit
			</button>
			<a class="btn btn-default" href="<?php echo base_url('arbitros'); ?>">
				Cancel
			</a>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Nombre', 'nombre', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('nombre', set_value('nombre', $arbitro->get_nombre()), 'class="form-control" required'); ?>
				<?php echo form_error('nombre'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Especialidad', 'especialidad', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('especialidad', set_value('especialidad', $arbitro->get_especialidad()), 'class="form-control" required'); ?>
				<?php echo form_error('especialidad'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Correo', 'correo', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('correo', set_value('correo', $arbitro->get_correo()), 'class="form-control" required'); ?>
				<?php echo form_error('correo'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Universidad', 'universidad', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('universidad', set_value('universidad', $arbitro->get_universidad()), 'class="form-control" required'); ?>
				<?php echo form_error('universidad'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Perfil', 'perfil', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('perfil', set_value('perfil', $arbitro->get_perfil()), 'class="form-control" required'); ?>
				<?php echo form_error('perfil'); ?>
			</div>
		</div>
	<?php echo form_close(); ?>
<?php $this->load->view('layout/footer'); ?>