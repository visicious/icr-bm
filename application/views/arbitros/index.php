<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Arbitros
	</h1>
	<div class="text-right">
		<a class="btn btn-default" href="<?php echo base_url('arbitros/create'); ?>">
			Create A New Arbitro
		</a>
	</div>
	<?php if ($arbitros): ?>
		<table class="table table table-striped table-hover">
			<thead>
				<tr>
					<td>Nombre</td>
					<td>Especialidad</td>
					<td>Correo</td>
					<td>Universidad</td>
					<td>Perfil</td>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($arbitros as $arbitro): ?>
					<tr>
						<td><?php echo $arbitro->get_nombre(); ?></td>
						<td><?php echo $arbitro->get_especialidad(); ?></td>
						<td><?php echo $arbitro->get_correo(); ?></td>
						<td><?php echo $arbitro->get_universidad(); ?></td>
						<td><?php echo $arbitro->get_perfil(); ?></td>
						<td>
							<a href="<?php echo base_url('arbitros/edit/' . $arbitro->get_id()); ?>">
								<i class="fa fa-edit fa-2x"></i>
							</a>
							<a href="<?php echo base_url('arbitros/delete/' . $arbitro->get_id()); ?>">
								<i class="fa fa-trash fa-2x"></i>
							</a>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php echo $links; ?>
	<?php elseif ($this->input->get('keyword')): ?>
		Your search - <b><?php echo $this->input->get('keyword') ?></b> - did not match any arbitros.
	<?php else: ?>
		There are no arbitros that are currently available.
	<?php endif; ?>
<?php $this->load->view('layout/footer'); ?>