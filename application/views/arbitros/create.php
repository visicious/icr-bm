<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Registro de Evaluador
	</h1>
	<?php echo form_open('arbitros/create', 'class=""'); ?>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Nombres y Apellidos', 'nombre', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('nombre', set_value('nombre'), 'class="form-control" required'); ?>
				<?php echo form_error('nombre'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Especialidad', 'especialidad', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('especialidad', set_value('especialidad'), 'class="form-control" required'); ?>
				<?php echo form_error('especialidad'); ?>
			</div>
		</div>
		<?php if(!$is_evaluador): ?>
			<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<?php echo form_label('Correo', 'correo', array('class' => 'control-label')); ?>
				<div class="">
					<?php echo form_input('correo', set_value('correo'), 'class="form-control" required'); ?>
					<?php echo form_error('correo'); ?>
				</div>
			</div>
		<?php endif; ?>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Universidad o Institución', 'universidad', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('universidad', set_value('universidad'), 'class="form-control" required'); ?>
				<?php echo form_error('universidad'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Pais', 'nacionalidad', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('nacionalidad', set_value('nacionalidad'), 'class="form-control" required'); ?>
				<?php echo form_error('nacionalidad'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('DNI/Pasaporte', 'dni', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('dni', set_value('dni'), 'class="form-control" required'); ?>
				<?php echo form_error('dni'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Fecha de Nacimiento', 'fecha_nacimiento', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input(array('name'  =>'fecha_nacimiento', 'type'  =>'date'), set_value('fecha_nacimiento'), 'class="form-control" required'); ?>
				<?php echo form_error('fecha_nacimiento'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Teléfono', 'telefono', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input(array('name'  =>'telefono', 'type'  =>'number'), set_value('telefono'), 'class="form-control" required'); ?>
				<?php echo form_error('telefono'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Página Web (Opcional)', 'pagina_web', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('pagina_web', set_value('pagina_web'), 'class="form-control" '); ?>
				<?php echo form_error('pagina_web'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Perfil', 'perfil', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_textarea('perfil', set_value('perfil'), 'class="form-control" required'); ?>
				<?php echo form_error('perfil'); ?>
			</div>
		</div>
		<div class="text-right">
			<button type="submit" class="btn btn-primary">
				Enviar
			</button>
			<?php if($is_evaluador): ?>
				<a class="btn btn-default btn-cancel-evaluador-icrbm" href="#">
					Cancelar
				</a>
			<?php else: ?>
				<a class="btn btn-default" href="<?php echo base_url('arbitros/create'); ?>">
					Cancelar
				</a>
			<?php endif; ?>
		</div>
	<?php echo form_close(); ?>
<?php $this->load->view('layout/footer'); ?>