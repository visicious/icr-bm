<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Jean Phillip Castelo Huaquipaco">
    <meta name="msapplication-TileImage" content="http://www.icr-bm.org/2018/wp-content/uploads/2018/06/cropped-icr-bm-icon-270x270.png">
    <link rel="apple-touch-icon-precomposed" href="http://www.icr-bm.org/2018/wp-content/uploads/2018/06/cropped-icr-bm-icon-180x180.png">
    <link rel="icon" href="http://www.icr-bm.org/2018/wp-content/uploads/2018/06/cropped-icr-bm-icon-192x192.png" sizes="192x192">
    <link rel="icon" href="http://www.icr-bm.org/2018/wp-content/uploads/2018/06/cropped-icr-bm-icon-32x32.png" sizes="32x32">
    <!--<title>Document <?php //echo ($this->uri->segment(1)) ? ' - ' . ucwords(str_replace('_', ' ', $this->uri->segment(1))) : NULL; ?></title>-->
    <script type="text/javascript" src="<?php echo base_url('/js/jquery-3.3.1.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('/js/bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('/js/jquery-confirm.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('/js/core.js'); ?>"></script>


    <title>INTERNATIONAL CONFERENCE OF RESEARCH IN BUSINESS &amp; MANAGEMENT</title>
    <link rel="dns-prefetch" href="//fonts.googleapis.com">
    <link rel="dns-prefetch" href="//s.w.org">
    <link rel="alternate" type="application/rss+xml" title="INTERNATIONAL CONFERENCE OF RESEARCH IN BUSINESS &amp; MANAGEMENT » Feed" href="http://www.icr-bm.org/2018/?feed=rss2">
    <link rel="alternate" type="application/rss+xml" title="INTERNATIONAL CONFERENCE OF RESEARCH IN BUSINESS &amp; MANAGEMENT » RSS de los comentarios" href="http://www.icr-bm.org/2018/?feed=comments-rss2">
    <link rel="alternate" type="application/rss+xml" title="INTERNATIONAL CONFERENCE OF RESEARCH IN BUSINESS &amp; MANAGEMENT » Inicio RSS de los comentarios" href="http://www.icr-bm.org/2018/?feed=rss2&amp;page_id=3641">
    <script async="" src="https://www.google-analytics.com/analytics.js"></script>
    <link rel="stylesheet" id="dashicons-css" href="http://www.icr-bm.org/2018/wp-includes/css/dashicons.min.css?ver=4.9.8" type="text/css" media="all">
    <link rel="stylesheet" id="admin-bar-css" href="http://www.icr-bm.org/2018/wp-includes/css/admin-bar.min.css?ver=4.9.8" type="text/css" media="all">
    <link rel="stylesheet" id="contact-form-7-css" href="http://www.icr-bm.org/2018/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.2" type="text/css" media="all">
    <link rel="stylesheet" id="style-css" href="http://www.icr-bm.org/2018/wp-content/themes/thekeynote/style.css?ver=4.9.8" type="text/css" media="all">
    <link rel="stylesheet" id="Raleway-google-font-css" href="http://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2Cregular%2C500%2C600%2C700%2C800%2C900&amp;subset=latin&amp;ver=4.9.8" type="text/css" media="all">
    <link rel="stylesheet" id="Montserrat-google-font-css" href="http://fonts.googleapis.com/css?family=Montserrat%3Aregular%2C700&amp;subset=latin&amp;ver=4.9.8" type="text/css" media="all">
    <link rel="stylesheet" id="Droid-Serif-google-font-css" href="http://fonts.googleapis.com/css?family=Droid+Serif%3Aregular%2Citalic%2C700%2C700italic&amp;subset=latin&amp;ver=4.9.8" type="text/css" media="all">
    <link rel="stylesheet" id="superfish-css" href="http://www.icr-bm.org/2018/wp-content/themes/thekeynote/plugins/superfish/css/superfish.css?ver=4.9.8" type="text/css" media="all">
    <link rel="stylesheet" id="dlmenu-css" href="http://www.icr-bm.org/2018/wp-content/themes/thekeynote/plugins/dl-menu/component.css?ver=4.9.8" type="text/css" media="all">
    <link rel="stylesheet" id="font-awesome-css" href="http://www.icr-bm.org/2018/wp-content/themes/thekeynote/plugins/font-awesome-new/css/font-awesome.min.css?ver=4.9.8" type="text/css" media="all">
    <link rel="stylesheet" id="jquery-fancybox-css" href="http://www.icr-bm.org/2018/wp-content/themes/thekeynote/plugins/fancybox/jquery.fancybox.css?ver=4.9.8" type="text/css" media="all">
    <link rel="stylesheet" id="thekeynote_flexslider-css" href="http://www.icr-bm.org/2018/wp-content/themes/thekeynote/plugins/flexslider/flexslider.css?ver=4.9.8" type="text/css" media="all">
    <link rel="stylesheet" id="style-responsive-css" href="http://www.icr-bm.org/2018/wp-content/themes/thekeynote/stylesheet/style-responsive.css?ver=4.9.8" type="text/css" media="all">
    <link rel="stylesheet" id="style-custom-css" href="http://www.icr-bm.org/2018/wp-content/themes/thekeynote/stylesheet/style-custom.css?1535639283&amp;ver=4.9.8" type="text/css" media="all">
    <link rel="stylesheet" id="newsletter-css" href="http://www.icr-bm.org/2018/wp-content/plugins/newsletter/style.css?ver=5.5.2" type="text/css" media="all">
    <link rel="stylesheet" id="ms-main-css" href="http://www.icr-bm.org/2018/wp-content/plugins/masterslider/public/assets/css/masterslider.main.css?ver=3.2.2" type="text/css" media="all">
    <link rel="stylesheet" id="ms-custom-css" href="http://www.icr-bm.org/2018/wp-content/uploads/masterslider/custom.css?ver=4" type="text/css" media="all">
    <script type="text/javascript" src="http://www.icr-bm.org/2018/wp-includes/js/jquery/jquery.js?ver=1.12.4"></script>
    <script type="text/javascript" src="http://www.icr-bm.org/2018/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1"></script>
    <link rel="https://api.w.org/" href="http://www.icr-bm.org/2018/index.php?rest_route=/">
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.icr-bm.org/2018/xmlrpc.php?rsd">
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.icr-bm.org/2018/wp-includes/wlwmanifest.xml"> 
    <meta name="generator" content="WordPress 4.9.8">
    <link rel="canonical" href="http://www.icr-bm.org/2018/">
    <link rel="shortlink" href="http://www.icr-bm.org/2018/">
    <link rel="alternate" type="application/json+oembed" href="http://www.icr-bm.org/2018/index.php?rest_route=%2Foembed%2F1.0%2Fembed&amp;url=http%3A%2F%2Fwww.icr-bm.org%2F2018%2F">
    <link rel="alternate" type="text/xml+oembed" href="http://www.icr-bm.org/2018/index.php?rest_route=%2Foembed%2F1.0%2Fembed&amp;url=http%3A%2F%2Fwww.icr-bm.org%2F2018%2F&amp;format=xml">
    <script>
        var ms_grabbing_curosr='http://www.icr-bm.org/2018/wp-content/plugins/masterslider/public/assets/css/common/grabbing.cur',ms_grab_curosr='http://www.icr-bm.org/2018/wp-content/plugins/masterslider/public/assets/css/common/grab.cur';
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-123177076-1', 'auto');
        ga('send', 'pageview');
    </script><!-- load the script for older ie version -->
    <!--[if lt IE 9]>
    <script src="http://www.icr-bm.org/2018/wp-content/themes/thekeynote/javascript/html5.js" type="text/javascript"></script>
    <script src="http://www.icr-bm.org/2018/wp-content/themes/thekeynote/plugins/easy-pie-chart/excanvas.js" type="text/javascript"></script>
    <![endif]-->
    <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
    <style type="text/css" media="print">#wpadminbar { display:none; }</style>
    <style type="text/css" media="screen">
        html { margin-top: 32px !important; }
        * html body { margin-top: 32px !important; }
        @media screen and ( max-width: 782px ) {
            html { margin-top: 46px !important; }
            * html body { margin-top: 46px !important; }
        }
    </style>
    <link rel="icon" href="http://www.icr-bm.org/2018/wp-content/uploads/2018/06/cropped-icr-bm-icon-32x32.png" sizes="32x32">
    <link rel="icon" href="http://www.icr-bm.org/2018/wp-content/uploads/2018/06/cropped-icr-bm-icon-192x192.png" sizes="192x192">
    <link rel="apple-touch-icon-precomposed" href="http://www.icr-bm.org/2018/wp-content/uploads/2018/06/cropped-icr-bm-icon-180x180.png">
    <meta name="msapplication-TileImage" content="http://www.icr-bm.org/2018/wp-content/uploads/2018/06/cropped-icr-bm-icon-270x270.png">

    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('/css/style.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('/css/jquery-confirm.min.css'); ?>">
    
</head>
<body class="home page-template-default page page-id-3641 logged-in admin-bar _masterslider _msp_version_3.2.2 customize-support">
  <style type="text/css" id="wp-custom-css">
        .gdlr-navigation-wrapper {
            margin-left: -3px;
            margin-right: -3px;
        }

        #unsa-auspiciador-icrbm {
            margin-top: 63px;
        }

        #custom_html-2{
            margin-bottom: 20px;
        }

        .gdlr-logo {
            margin-left: -68px;
        }

        .gdlr-banner-item-wrapper{
            height: 194px;
        }

        @media(max-width: 1140px){
        .gdlr-logo {
            float: left;
            margin-left: 0px;
            max-width: 227px;
        }
        
        .gdlr-logo.left-logo a img {
            width: 168px !important;
        }
        
        .gdlr-logo.left-logo {
            float: none;
            margin: 0px auto;
            margin-bottom: 25px;
        }
        
        .gdlr-navigation-wrapper {
            float: left;
            padding-right: 0px;
            margin-left: 0px;
        margin-right: 0px;
        }
    }

    @media(max-width: 959px){
        .gdlr-logo.left-logo {
            margin: 0px auto !important;
            padding: 0px 0px 30px 0px;
            float: none;
            max-width: 366px !important;
            text-align: center;
        }
        
        .gdlr-logo.left-logo a img {
            width: 20vw !important;
            margin-left: 0px !important;
            padding-left: 4px;
        }
        
        .gdlr-logo {
            float: none;
            margin-left: -68px;
            max-width: 318px;
        }
        
        .gdlr-navigation-wrapper {
            float: none;
        }
    }

    @media(max-width: 750px){
        .gdlr-logo {
            max-width: 221px;
        }
    }

        @media(min-width: 1141px){
            .gdlr-logo.left-logo {
            margin-left: -7px;
            }
        }
    </style>
    <header class="gdlr-header-wrapper">
        <!-- top navigation -->
        
        <!-- logo -->
        <div class="gdlr-header-inner">
            <div class="gdlr-header-container container">
                <!-- logo -->
                <div class="gdlr-logo">
                    <a href="http://www.icr-bm.org/2018">
                        <img src="http://www.icr-bm.org/2018/wp-content/uploads/2018/07/logo-icrbm.png" alt="" width="505" height="196">                    </a>
                    <div class="gdlr-responsive-navigation dl-menuwrapper" id="gdlr-responsive-navigation"><button class="dl-trigger">Open Menu</button><ul id="menu-main-menu" class="dl-menu gdlr-main-mobile-menu"><li id="menu-item-4236" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-4227 current_page_item current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-4236"><a href="http://www.icr-bm.org/2018/">Inicio</a>
<ul class="dl-submenu">
    <li id="menu-item-4241" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-4241"><a href="http://www.icr-bm.org/2018/#presentacion-icrbm">Presentación</a></li>
    <li id="menu-item-4009" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-4009"><a href="http://www.icr-bm.org/2018/#objetivos-icrbm">Objetivos</a></li>
    <li id="menu-item-4242" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-4242"><a href="http://www.icr-bm.org/2018/#beneficios-icrbm">Beneficios</a></li>
    <li id="menu-item-3991" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-3991"><a href="http://www.icr-bm.org/2018/#resultados-esperados-icrbm">Resultados Esperados</a></li>
</ul>
</li>
<li id="menu-item-4111" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4111"><a href="http://www.icr-bm.org/2018/call-for-paper/">Call for Papers</a>
<ul class="dl-submenu">
    <li id="menu-item-4362" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4362"><a href="http://www.icr-bm.org/2018/?page_id=3344">Registro de Autor</a></li>
    <li id="menu-item-4307" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4307"><a href="http://www.icr-bm.org/2018/cronograma/">Fechas Importantes</a></li>
    <li id="menu-item-4092" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4092"><a href="http://www.icr-bm.org/2018/revisores/">Acceso de Revisores</a></li>
    <li id="menu-item-4632" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4632"><a href="http://www.icr-bm.org/tablero/index.php/auth/login">Ingreso de Autores</a></li>
</ul>
</li>
<li id="menu-item-4224" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-4224"><a href="http://www.icr-bm.org/2018/?page_id=4269">Inscripciones</a>
<ul class="dl-submenu">
    <li id="menu-item-4274" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4274"><a href="http://www.icr-bm.org/2018/registro-como-asistente/">Registro de Asistente</a></li>
    <li id="menu-item-4361" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4361"><a href="http://www.icr-bm.org/2018/?page_id=3344">Registro de Autor</a></li>
    <li id="menu-item-4302" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4302"><a href="http://www.icr-bm.org/2018/contacto/">Consultas</a></li>
</ul>
</li>
<li id="menu-item-4110" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4110"><a href="http://www.icr-bm.org/2018/programa/">Programa del Evento</a>
<ul class="dl-submenu">
    <li id="menu-item-4072" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4072"><a href="http://www.icr-bm.org/2018/ponentes/">Ponentes</a></li>
    <li id="menu-item-4248" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4248"><a href="http://www.icr-bm.org/2018/?page_id=4244#tracks-icrbm">Tracks</a></li>
    <li id="menu-item-4249" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4249"><a href="http://www.icr-bm.org/2018/?page_id=4244#topicos-icrbm">Temáticas</a></li>
    <li id="menu-item-4243" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4243"><a href="http://www.icr-bm.org/2018/programa/">Programa de Sesiones</a></li>
    <li id="menu-item-4097" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4097"><a href="http://www.icr-bm.org/2018/lugar/">Lugar</a></li>
    <li id="menu-item-4078" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4078"><a href="http://www.icr-bm.org/2018/cronograma/">Fechas Importantes</a></li>
</ul>
</li>
<li id="menu-item-4109" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4109"><a href="http://www.icr-bm.org/2018/organizacion/">Organización</a>
<ul class="dl-submenu">
    <li id="menu-item-4333" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4333"><a href="http://www.icr-bm.org/2018/chair/">Chair</a></li>
    <li id="menu-item-4255" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4255"><a href="http://www.icr-bm.org/2018/comite-de-programa/">Comité de Programa</a></li>
    <li id="menu-item-4256" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4256"><a href="http://www.icr-bm.org/2018/comite-organizador/">Comité Organizador</a></li>
    <li id="menu-item-4633" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4633"><a href="http://www.icr-bm.org/2018/revisores/">Acceso de Revisores</a></li>
</ul>
</li>
<li id="menu-item-4308" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4308"><a href="http://www.icr-bm.org/2018/contacto/">Informes</a>
<ul class="dl-submenu">
    <li id="menu-item-4311" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4311"><a href="http://www.icr-bm.org/2018/?page_id=4328">FAQ</a></li>
    <li id="menu-item-4310" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4310"><a href="http://www.icr-bm.org/2018/?page_id=4331">Glosario</a></li>
    <li id="menu-item-4309" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4309"><a href="http://www.icr-bm.org/2018/contacto/">Contacto</a></li>
</ul>
</li>
</ul></div>             </div>

                <!-- navigation -->
                <div class="gdlr-navigation-wrapper"><nav class="gdlr-navigation" id="gdlr-main-navigation" role="navigation"><ul id="menu-main-menu-1" class="sf-menu gdlr-main-menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-4227 current_page_item current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-4236menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-4227 current_page_item current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-4236 gdlr-normal-menu"><a href="http://www.icr-bm.org/2018/">Inicio</a>
<ul class="sub-menu">
    <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-4241"><a href="http://www.icr-bm.org/2018/#presentacion-icrbm">Presentación</a></li>
    <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-4009"><a href="http://www.icr-bm.org/2018/#objetivos-icrbm">Objetivos</a></li>
    <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-4242"><a href="http://www.icr-bm.org/2018/#beneficios-icrbm">Beneficios</a></li>
    <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-3991"><a href="http://www.icr-bm.org/2018/#resultados-esperados-icrbm">Resultados Esperados</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4111menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4111 gdlr-normal-menu"><a href="http://www.icr-bm.org/2018/call-for-paper/">Call for Papers</a>
<ul class="sub-menu">
    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4362"><a href="http://www.icr-bm.org/2018/?page_id=3344">Registro de Autor</a></li>
    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4307"><a href="http://www.icr-bm.org/2018/cronograma/">Fechas Importantes</a></li>
    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4092"><a href="http://www.icr-bm.org/2018/revisores/">Acceso de Revisores</a></li>
    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4632"><a href="http://www.icr-bm.org/tablero/index.php/auth/login">Ingreso de Autores</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-4224menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-4224 gdlr-normal-menu"><a href="http://www.icr-bm.org/2018/?page_id=4269">Inscripciones</a>
<ul class="sub-menu">
    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4274"><a href="http://www.icr-bm.org/2018/registro-como-asistente/">Registro de Asistente</a></li>
    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4361"><a href="http://www.icr-bm.org/2018/?page_id=3344">Registro de Autor</a></li>
    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4302"><a href="http://www.icr-bm.org/2018/contacto/">Consultas</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4110menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4110 gdlr-normal-menu"><a href="http://www.icr-bm.org/2018/programa/">Programa del Evento</a>
<ul class="sub-menu">
    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4072"><a href="http://www.icr-bm.org/2018/ponentes/">Ponentes</a></li>
    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4248"><a href="http://www.icr-bm.org/2018/?page_id=4244#tracks-icrbm">Tracks</a></li>
    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4249"><a href="http://www.icr-bm.org/2018/?page_id=4244#topicos-icrbm">Temáticas</a></li>
    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4243"><a href="http://www.icr-bm.org/2018/programa/">Programa de Sesiones</a></li>
    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4097"><a href="http://www.icr-bm.org/2018/lugar/">Lugar</a></li>
    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4078"><a href="http://www.icr-bm.org/2018/cronograma/">Fechas Importantes</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4109menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4109 gdlr-normal-menu"><a href="http://www.icr-bm.org/2018/organizacion/">Organización</a>
<ul class="sub-menu">
    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4333"><a href="http://www.icr-bm.org/2018/chair/">Chair</a></li>
    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4255"><a href="http://www.icr-bm.org/2018/comite-de-programa/">Comité de Programa</a></li>
    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4256"><a href="http://www.icr-bm.org/2018/comite-organizador/">Comité Organizador</a></li>
    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4633"><a href="http://www.icr-bm.org/2018/revisores/">Acceso de Revisores</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4308menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-4308 gdlr-normal-menu"><a href="http://www.icr-bm.org/2018/contacto/">Informes</a>
<ul class="sub-menu">
    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4311"><a href="http://www.icr-bm.org/2018/?page_id=4328">FAQ</a></li>
    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4310"><a href="http://www.icr-bm.org/2018/?page_id=4331">Glosario</a></li>
    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4309"><a href="http://www.icr-bm.org/2018/contacto/">Contacto</a></li>
</ul>
</li>
</ul></nav> 
<!--<span class="gdlr-menu-search-button-sep">|</span>
<i class="fa fa-search icon-search gdlr-menu-search-button" id="gdlr-menu-search-button" ></i>-->
<div class="gdlr-menu-search" id="gdlr-menu-search">
    <form method="get" id="searchform" action="http://www.icr-bm.org/2018/">
                <div class="search-text">
            <input type="text" value="Tipo Palabras clave" name="s" autocomplete="off" data-default="Tipo Palabras clave">
        </div>
        <input type="submit" value="">
        <div class="clear"></div>
    </form> 
</div>      
<div class="gdlr-navigation-gimmick" id="gdlr-navigation-gimmick"></div><div class="clear"></div></div><div class="gdlr-logo left-logo" style="margin-top: 24px; margin-bottom: 24px; max-width: 2px; "><a href="http://www.unsa.edu.pe/"><img src="http://www.icr-bm.org/2018/wp-content/uploads/2018/09/unsaLogo-icrbm.jpg" title="Universidad Nacional de San Agustin de Arequipa" alt="Universidad Nacional de San Agustin de Arequipa" width="505" height="196" style="padding-bottom:9px;display: inline-block; width: 120px; max-width: 318px; margin-left: -39px;"></a><a href="http://vri.unsa.edu.pe/"><img src="http://www.icr-bm.org/2018/wp-content/uploads/2018/08/unsa-investiga-icrbm.jpg" title="UNSA Investiga - Vicerectorado de Investigación" alt="UNSA Investiga - Vicerectorado de Investigación" width="505" height="196" style="display: inline-block; width: 120px; max-width: 318px; margin-left: -39px;"></a></div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="clear"></div>
        <script type="text/javascript">
                function sendScore( paper_id, arbitro_id) {
                    if ($("input[name='score["+paper_id+"]']").val() != null){
                        var score_val = $("input[name='score["+paper_id+"]']").val();
                        $.ajax({
                            url: "<?php echo base_url('index.php/paper_arbitros/set_score/"+paper_id+"/"+arbitro_id+"/"+score_val+"')?>",
                            method: "GET",
                            success : function(response){
                                if(response.type == "success") {
                                    alert(response.text);
                                }
                            }
                        });
                    }
                    
                }
        </script>
    </header>
    <div class="container">
