	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Registro de Autores	
	</h1>
	<?php if ($users): ?>
		<table class="table table table-striped table-hover">
			<thead>
				<tr>
					<td>Nombre</td>
					<td>Correo</td>
					<td>DNI/Pasaporte/Documento de Identidad</td>
					<td>Inscrito como</td>
					<td>Pago</td>
					<td>Voucher</td>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($users as $autor): ?>
					<tr>
						<td><?php echo $autor->first_name." ".$autor->last_name; ?></td>
						<td><?php echo $autor->email; ?></td>
						<td><?php echo $autor->dni; ?></td>
						<td><?php echo $autor->user_type; ?></td>
						<td><?php echo $autor->bank_detail; ?></td>
						<td>
							<img src="<?php echo base_url('index.php/uploads/'.$autor->voucher_slug) ;?>">
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php echo $links; ?>
	<?php elseif ($this->input->get('keyword')): ?>
		Your search - <b><?php echo $this->input->get('keyword') ?></b> - did not match any autors.
	<?php else: ?>
		There are no autors that are currently available.
	<?php endif; ?>
<?php $this->load->view('layout/footer'); ?>