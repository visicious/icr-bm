<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Autors
	</h1>
	<div class="text-right">
		<a class="btn btn-default" href="<?php echo base_url('autors'); ?>">
			Cancel
		</a>
	</div>
	<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?php echo form_label('Nombre', 'nombre', array('class' => 'control-label')); ?>
		<div class="">
			<input type="text" class="form-control" value="<?php echo $autor->get_nombre(); ?>" disabled>
		</div>
	</div>
	<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?php echo form_label('Correo', 'correo', array('class' => 'control-label')); ?>
		<div class="">
			<input type="text" class="form-control" value="<?php echo $autor->get_correo(); ?>" disabled>
		</div>
	</div>
	<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?php echo form_label('Universidad', 'universidad', array('class' => 'control-label')); ?>
		<div class="">
			<input type="text" class="form-control" value="<?php echo $autor->get_universidad(); ?>" disabled>
		</div>
	</div>
	<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?php echo form_label('Nacionalidad', 'nacionalidad', array('class' => 'control-label')); ?>
		<div class="">
			<input type="text" class="form-control" value="<?php echo $autor->get_nacionalidad(); ?>" disabled>
		</div>
	</div>
	<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?php echo form_label('Categoria', 'categoria', array('class' => 'control-label')); ?>
		<div class="">
			<input type="text" class="form-control" value="<?php echo $autor->get_categoria(); ?>" disabled>
		</div>
	</div>
<?php $this->load->view('layout/footer'); ?>