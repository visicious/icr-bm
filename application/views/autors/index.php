<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Autors
	</h1>
	<div class="text-right">
		<a class="btn btn-default" href="<?php echo base_url('index.php/autors/get_autors_pdf'); ?>">
			Obtener Inscritos
		</a>
	</div>
	<div class="text-right">
		<a class="btn btn-default" href="<?php echo base_url('autors/create'); ?>">
			Create A New Autor
		</a>
	</div>
	<?php if ($autors): ?>
		<table class="table table table-striped table-hover">
			<thead>
				<tr>
					<td>Nombre</td>
					<td>Correo</td>
					<td>Universidad</td>
					<td>Nacionalidad</td>
					<td>Categoria</td>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($autors as $autor): ?>
					<tr>
						<td><?php echo $autor->get_nombre(); ?></td>
						<td><?php echo $autor->get_correo(); ?></td>
						<td><?php echo $autor->get_universidad(); ?></td>
						<td><?php echo $autor->get_nacionalidad(); ?></td>
						<td><?php echo $autor->get_categoria(); ?></td>
						<td>
							<a href="<?php echo base_url('autors/edit/' . $autor->get_id()); ?>">
								<i class="fa fa-edit fa-2x"></i>
							</a>
							<a href="<?php echo base_url('autors/delete/' . $autor->get_id()); ?>">
								<i class="fa fa-trash fa-2x"></i>
							</a>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php echo $links; ?>
	<?php elseif ($this->input->get('keyword')): ?>
		Your search - <b><?php echo $this->input->get('keyword') ?></b> - did not match any autors.
	<?php else: ?>
		There are no autors that are currently available.
	<?php endif; ?>
<?php $this->load->view('layout/footer'); ?>