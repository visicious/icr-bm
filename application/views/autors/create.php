<?php $this->load->view('layout/header'); ?>
	<br />
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Registro de Autores
	</h1>
	<br />
	<?php if($is_additional_autor): ?>
		<?php echo form_open('autors/add_author/'.$paper_id, 'class=""'); ?>
	<?php else: ?>	
		<?php echo form_open('autors/create', 'class=""'); ?>
	<?php endif;?>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Nombres y Apellidos (como va a aparecer en su certificado)', 'nombre', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('nombre', set_value('nombre'), 'class="form-control" required'); ?>
				<?php echo form_error('nombre'); ?>
			</div>
		</div>
		<?php if(!$is_member): ?>
			<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<?php echo form_label('Correo', 'correo', array('class' => 'control-label')); ?>
				<div class="">
					<?php echo form_input('correo', set_value('correo'), 'class="form-control" required'); ?>
					<?php echo form_error('correo'); ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if(!$has_universidad): ?>
			<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<?php echo form_label('Universidad o Institución', 'universidad', array('class' => 'control-label')); ?>
				<div class="">
					<?php echo form_input('universidad', set_value('universidad'), 'class="form-control" required'); ?>
					<?php echo form_error('universidad'); ?>
				</div>
			</div>
		<?php endif; ?>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Nacionalidad', 'nacionalidad', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('nacionalidad', set_value('nacionalidad'), 'class="form-control" required'); ?>
				<?php echo form_error('nacionalidad'); ?>
			</div>
		</div>
		<?php if(!$has_dni): ?>
			<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<?php echo form_label('DNI/Pasaporte', 'dni', array('class' => 'control-label')); ?>
				<div class="">
					<?php echo form_input('dni', set_value('dni'), 'class="form-control" required'); ?>
					<?php echo form_error('dni'); ?>
				</div>
			</div>
		<?php endif; ?>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Fecha de Nacimiento', 'fecha_nacimiento', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input(array('name'  =>'fecha_nacimiento', 'type'  =>'date'), set_value('fecha_nacimiento'), 'class="form-control" required'); ?>
				<?php echo form_error('fecha_nacimiento'); ?>
			</div>
		</div>
		<?php if(!$has_phone): ?>
			<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<?php echo form_label('Teléfono', 'telefono', array('class' => 'control-label')); ?>
				<div class="">
					<?php echo form_input(array('name'  =>'telefono', 'type'  =>'number'), set_value('telefono'), 'class="form-control" required'); ?>
					<?php echo form_error('telefono'); ?>
				</div>
			</div>
		<?php endif; ?>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Página Web (Opcional)', 'pagina_web', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('pagina_web', set_value('pagina_web'), 'class="form-control" '); ?>
				<?php echo form_error('pagina_web'); ?>
			</div>
		</div>
		<br />
		<div class="text-right">
			<button type="submit" class="btn btn-primary">
				Enviar
			</button>
			<?php if(!$is_member): ?>
				<a class="btn btn-default" href="<?php echo base_url('autors'); ?>">
					Cancelar
				</a>
			<?php else: ?>
				<a class="btn btn-default" href="<?php echo base_url('dashboard/index'); ?>">
					Cancelar
				</a>
			<?php endif;?>
		</div>
		<br />
	<?php echo form_close(); ?>
<?php $this->load->view('layout/footer'); ?>