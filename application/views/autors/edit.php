<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Editar Autor
	</h1>
	<br />
	<?php echo form_open('autors/edit/' . $autor->get_id(), 'class=""'); ?>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Nombre', 'nombre', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('nombre', set_value('nombre', $autor->get_nombre()), 'class="form-control" required'); ?>
				<?php echo form_error('nombre'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Correo', 'correo', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('correo', set_value('correo', $autor->get_correo()), 'class="form-control" required'); ?>
				<?php echo form_error('correo'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Universidad', 'universidad', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('universidad', set_value('universidad', $autor->get_universidad()), 'class="form-control" required'); ?>
				<?php echo form_error('universidad'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Nacionalidad', 'nacionalidad', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('nacionalidad', set_value('nacionalidad', $autor->get_nacionalidad()), 'class="form-control" required'); ?>
				<?php echo form_error('nacionalidad'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('DNI/Pasaporte', 'dni', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('dni', set_value('dni'), 'class="form-control" required'); ?>
				<?php echo form_error('dni'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Fecha de Nacimiento', 'fecha_nacimiento', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input(array('name'  =>'fecha_nacimiento', 'type'  =>'date'), set_value('fecha_nacimiento'), 'class="form-control" required'); ?>
				<?php echo form_error('fecha_nacimiento'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Teléfono', 'telefono', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input(array('name'  =>'telefono', 'type'  =>'number'), set_value('telefono'), 'class="form-control" required'); ?>
				<?php echo form_error('telefono'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Página Web (Opcional)', 'pagina_web', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('pagina_web', set_value('pagina_web'), 'class="form-control" '); ?>
				<?php echo form_error('pagina_web'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Categoria', 'categoria', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('categoria', set_value('categoria', $autor->get_categoria()), 'class="form-control" required'); ?>
				<?php echo form_error('categoria'); ?>
			</div>
		</div>
		<br/>
		<div class="text-right">
			<button type="submit" class="btn btn-primary">
				Enviar
			</button>
			<a class="btn btn-default" href="<?php echo base_url('autors'); ?>">
				Cancelar
			</a>
		</div>
		<br/>
	<?php echo form_close(); ?>
<?php $this->load->view('layout/footer'); ?>