<?php $this->load->view('layout/header'); ?>
    <br />
    <h1>
        <i class="fa fa-lg fa-list"></i> 
        Modulo de
        <?php if($user_type == 'evaluador'): ?>
            Revisor
        <?php elseif($user_type == 'member' && !$is_author): ?>
            Participante
        <?php else: ?>
            Autor
        <?php endif; ?>
    </h1>
    <br />
    <?php if ($show_topicos && ($user_type == 'evaluador' || $user_type == 'chair')) : ?>
        <div class="text-left">
            <h3>
                Escoja a continuación los tópicos de los que desea ser evaluador:
            </h3>
        </div>
        <br />
        <?php echo form_open('dashboard/pick_tracks', 'class=""'); ?>
            <div class="row">
                <div class="table-label-icrbm col-lg-6 col-md-6 col-sm-6 col-6">
                    Tópicos
                </div>
                <div class="table-label-true-icrbm col-lg-2 col-md-2 col-sm-2 col-2">
                    Me Interesa
                </div>
                <div class="table-label-neutral-icrbm col-lg-2 col-md-2 col-sm-2 col-2">
                    Indiferente
                </div>
                <div class="table-label-false-icrbm col-lg-2 col-md-2 col-sm-2 col-2">
                    No me Interesa
                </div>
            </div>
            <?php foreach ($topicos as $topico): ?>
                <div class="row table-icrbm">
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-6 table-cell-icrbm">
                        <strong><?php echo form_label($topico->get_nombre(), 'decision['.$topico->get_id().']', array('class' => 'control-label')); ?></strong>
                    </div>  
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 table-cell-icrbm table-true-icrbm">
                        <?php echo form_radio('decision['.$topico->get_id().']', 'topico_de_interes',set_value('decision['.$topico->get_id().']'), 'class="form-control icrbm-checkbox" required'); ?>
                        <?php echo form_error('decision['.$topico->get_id().']'); ?>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 table-cell-icrbm table-neutral-icrbm">
                        <?php echo form_radio('decision['.$topico->get_id().']', 'indiferente',set_value('decision['.$topico->get_id().']'), 'class="form-control icrbm-checkbox" required'); ?>
                        <?php echo form_error('decision['.$topico->get_id().']'); ?>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-2 table-cell-icrbm table-false-icrbm">
                        <?php echo form_radio('decision['.$topico->get_id().']', 'no_interes',set_value('decision['.$topico->get_id().']'), 'class="form-control icrbm-checkbox" required'); ?>
                        <?php echo form_error('decision['.$topico->get_id().']'); ?>
                    </div>
                </div>
            <?php endforeach;?>
            <br />
            <div class="text-right">
                <button type="submit" class="btn btn-primary">
                    Enviar
                </button>
                <a class="btn btn-default btn-cancel-evaluador-icrbm" href="#">
                    Cancelar
                </a>
            </div>
        <?php echo form_close(); ?>
        <br />
    <?php elseif ($user_type == 'member' && !$has_pay): ?>
        <div class="row">
            <div class="icrbm_add_paper col-lg-12 col-md-12 col-sm-12 col-12">
                <h3><a href="<?php echo base_url('index.php/paper/create');?>">
                    ¡Gracias! En menos de 48 horas validaremos su pago y su cuenta será activada.
                </a></h3>
            </div>
        </div>
    <?php elseif ($user_type == 'member' && $is_author): ?>
        <div class="text-left">
            <h3>
                Papers Subidos:
            </h3>
        </div>
        <br />
        <div class="row">
                <div class=" col-lg-12 col-md-12 col-sm-12 col-12">
                    <?php if (isset($papers)): ?>
                    <?php foreach ($papers as $paper): ?>
                    <div class="row">
                        <div class="icrbm_papers col-lg-9 col-md-9 col-sm-9 col-9">
                            <h4><strong><a href="<?php echo base_url('/uploads/'.$paper->get_trabajo());?>" target="_blank"><?php echo $paper->get_titulo(); ?></a></strong></h4>
                            <br />
                            <p> Autores: </p>
                            <div class="row">
                            <?php foreach ($other_autors[$paper->get_id()] as $key => $autor): ?>
                                <div class="icrbm-autor col-lg-4 col-md-4 col-sm-6 col-6">
                                    <div class="icrbm-autor-title"><?php echo $autor->get_nombre();?></div>
                                    <div class="icrbm-autor-button">
                                        <a href="<?php echo base_url('index.php/autors/edit/'.$autor->get_id());?>">
                                            <i class="fa fa-lg fa-pencil"></i>
                                            Editar Autor
                                        </a>
                                    </div>
                                    <!--<div class="icrbm-autor-button">
                                        <a href="<?php echo base_url('index.php/autors/delete_authors/'.$paper->get_id().'/'.$autor->get_id());?>">
                                            <i class="fa fa-lg fa-close"></i>
                                            Borrar Autor
                                        </a>
                                    </div>-->
                                </div>
                            <?php endforeach;?>
                            </div>
                        </div>
                        <div class="icrbm_papers col-lg-3 col-md-3 col-sm-3 col-3">
                            <a href="<?php echo base_url('index.php/autors/add_author/'.$paper->get_id());?>"><i class="fa fa-lg fa-plus"></i> Agregar autor</a>
                        </div>
                    </div>
                    <br />
                    <?php endforeach;?>
                    <?php endif;?>

                    <div class="row">
                        <div class="icrbm_add_paper col-lg-12 col-md-12 col-sm-12 col-12">
                            <h3><a href="<?php echo base_url('index.php/papers/create');?>">
                                Subir Paper
                            </a></h3>
                        </div>
                    </div>
                </div>
        </div>
        </div>
        
        <br />
    <?php elseif ($user_type == 'member' && !$is_author): ?>
        <div class="row">
            <div class="icrbm_add_paper col-lg-12 col-md-12 col-sm-12 col-12">
                <h3><a href="#">
                    ¡Gracias! Le hemos enviado un correo a su bandeja de entrada. Revise su carpeta de spam. En menos de 48 horas validaremos su pago y su cuenta será activada.
                </a></h3>
            </div>
        </div>

    <?php elseif ($user_type == 'chair'): ?>
        <div class="eval-papers">
            <div class="text-left">
                <h3>
                    Papers a evaluar
                </h3>
            </div>
            <br />
            <?php if (isset($assigned_papers)): ?>
            <?php foreach ($assigned_papers as $paper): ?>
            <div class="row row-eq-height">
                <div class="icrbm_papers col-lg-9 col-md-9 col-sm-9 col-9">
                    <h4><strong><a href="<?php echo base_url('/uploads/'.$paper->get_trabajo());?>" target="_blank"><?php echo $paper->get_titulo(); ?></a></strong></h4>
                    <br />
                </div>
                <div class=" icrbm_papers col-lg-3 col-md-3 col-sm-3 col-3">
                    <i class="fa fa-lg fa-check"></i> Calificación
                    <br />
                    <div >
                        <input style="width: 100%;padding-left: 5px;" type="number" value="<?php echo $paper->score; ?>" id="score[<?php echo $paper->get_id(); ?>]" name="score[<?php echo $paper->get_id(); ?>]" />
                        <input style="width: 100%;" type="button" value="Calificar" onclick="sendScore(<?php echo $paper->get_id(); ?>, <?php echo $paper->arbitro_id; ?>)" />
                    </div>

                </div>

            </div>
            <br />
            <?php endforeach;?>
            <?php endif;?>
        </div>
        <br />
        <div class="reviewers-control">
            <div class="text-left">
                <h3>
                    Tablero de Control
                </h3>
            </div>
            <br />
            
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <th scope="col">Trabajo</th>
                        <?php foreach ($reviewers as $reviewer): ?>
                            <th scope="col"><?php echo $reviewer->get_nombre(); ?></th>
                        <?php endforeach;?>
                        <th scope="col">Resultado</th>
                    </thead>
                    <?php if (isset($topicos_per_paper)): ?>
                        <tbody>
                            <?php foreach ($topicos_per_paper as $paper_index => $topico_per_paper): ?>
                            <tr>
                                <th scope="row">
                                    <?php echo $topico_per_paper['titulo']; ?><br />
                                    <div class="topicos-label-container-icrbm">
                                        <?php echo '<label class="badge badge-light" style="margin-left:5px;">'.implode('</label><label class="badge badge-light" style="margin-left:5px;">',explode(',',$topico_per_paper['topicos'])).'</label>'; ?><br />
                                    </div>
                                </th>
                                <?php if (isset($papers_per_reviewer) && count($papers_per_reviewer)> 0) : ?>
                                    <?php foreach ($papers_per_reviewer as $paper_per_reviewer): ?>
                                        <?php if ($paper_per_reviewer->get_arbitro_id()->get_id() == $paper_index): ?>
                                            <td><a href="<?php echo base_url('index.php/uploads/'.$paper_per_reviewer->get_paper_id()->get_trabajo()) ;?>">Asignado</a><br/>
                                            </td>
                                        <?php else: ?>
                                            <td><a href="<?php echo base_url('index.php/uploads/'.$paper_per_reviewer->get_paper_id()->get_trabajo()) ;?>">Asignar</a><br/>
                                            </td>
                                        <?php endif;?>  
                                    <?php endforeach;?>
                                <?php else: ?>
                                    <?php foreach ($reviewers as $reviewer): ?>
                                        <td><a href="#">Asignar</a><br/>
                                            </td>
                                    <?php endforeach;?>
                                <?php endif;?>
                                <td> <a href="#"><i class="fa fa-lg fa-envelope"></i> Enviar Correo</a> </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    <?php else: ?>
                        No tienen ningun paper asignado
                    <?php endif;?>
                </table>
            </div>
            <br />
        </div>
    <?php elseif (!empty($show_papers) && $user_type == 'evaluador'): ?>
        <div class="text-left">
            <h3>
                Papers a evaluar
            </h3>
        </div>
        <br />
        
        <?php if (isset($assigned_papers)): ?>
        <?php foreach ($assigned_papers as $paper): ?>
        <div class="row row-eq-height">
            <div class="icrbm_papers col-lg-9 col-md-9 col-sm-9 col-9">
                <h4><strong><a href="<?php echo base_url('/uploads/'.$paper->get_trabajo());?>" target="_blank"><?php echo $paper->get_titulo(); ?></a></strong></h4>
                <br />
            </div>
            <div class=" icrbm_papers col-lg-3 col-md-3 col-sm-3 col-3">
                <i class="fa fa-lg fa-check"></i> Calificación
                <br />
                <div >
                    <input style="width: 100%;padding-left: 5px;" type="number" value="<?php echo $paper->score; ?>" id="score[<?php echo $paper->get_id(); ?>]" name="score[<?php echo $paper->get_id(); ?>]" />
                    <input style="width: 100%;" type="button" value="Calificar" onclick="sendScore(<?php echo $paper->get_id(); ?>, <?php echo $paper->arbitro_id; ?>)" />
                </div>

            </div>

        </div>
        <br />
        <?php endforeach;?>
        <?php endif;?>
        
    <?php elseif ($user_type == 'evaluador'): ?>
        <div class="text-left">
            <h3>
                Papers a evaluar
            </h3>
        </div>
        <br />
        <div class="eval-papers">No tiene ningún paper para evaluar aún.</div>
    

    <?php endif; ?>
<?php $this->load->view('layout/footer'); ?>