<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Paper_Topicos
	</h1>
	<div class="text-right">
		<a class="btn btn-default" href="<?php echo base_url('paper_topicos/create'); ?>">
			Create A New Paper_Topico
		</a>
	</div>
	<?php if ($paper_topicos): ?>
		<table class="table table table-striped table-hover">
			<thead>
				<tr>
					<td>Topico</td>
					<td>Paper</td>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($paper_topicos as $paper_topico): ?>
					<tr>
						<td><?php echo $paper_topico->get_topico_id()->get_id(); ?></td>
						<td><?php echo $paper_topico->get_paper_id()->get_id(); ?></td>
						<td>
							<a href="<?php echo base_url('paper_topicos/edit/' . $paper_topico->get_id()); ?>">
								<i class="fa fa-edit fa-2x"></i>
							</a>
							<a href="<?php echo base_url('paper_topicos/delete/' . $paper_topico->get_id()); ?>">
								<i class="fa fa-trash fa-2x"></i>
							</a>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php echo $links; ?>
	<?php elseif ($this->input->get('keyword')): ?>
		Your search - <b><?php echo $this->input->get('keyword') ?></b> - did not match any paper topicos.
	<?php else: ?>
		There are no paper topicos that are currently available.
	<?php endif; ?>
<?php $this->load->view('layout/footer'); ?>