<?php $this->load->view('layout/header'); ?>
	<br />
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Papers
	</h1>
	<br />
	<?php echo form_open_multipart('papers/create', 'class=""'); ?>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Titulo', 'titulo', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('titulo', set_value('titulo'), 'class="form-control" required'); ?>
				<?php echo form_error('titulo'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Abstract', 'abstract', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_textarea('abstract', set_value('abstract'), 'class="form-control" required'); ?>
				<?php echo form_error('abstract'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Trabajo <strong>(formatos aceptados: doc, docx)(Tamaño máximo: 10MB)</strong>', 'trabajo', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_upload('trabajo', set_value('trabajo'), 'class="form-control" required'); ?>
				<?php echo form_error('trabajo'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('<strong>¿Va a participar en el coloquio doctoral? (Con su propia tesis de doctorado)</strong>', 'trabajo', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_radio('has_doc_tesis','yes', 'class="form-control icrbm-checkbox" '); ?>
				<?php echo form_label('Si', '', array('class' => 'control-label icrbm-checkbox-label')); ?>
				<?php echo form_error('has_doc_tesis'); ?>
			</div>
			<div class="">
				<?php echo form_radio('has_doc_tesis', 'no', 'class="form-control icrbm-checkbox" '); ?>
				<?php echo form_label('No', '', array('class' => 'control-label icrbm-checkbox-label')); ?>
				<?php echo form_error('has_doc_tesis'); ?>
			</div>
		</div>

		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 paper-topicos-icrbm">
			<div class=""><?php echo form_label('Topicos / Keywords <strong>(escoger 5 items en concordancia con las palabras claves de su trabajo)</strong>', 'topicos', array('class' => 'control-label')); ?></div>
			<div class="row">
				<?php foreach ($topicos as $key=>$topico): ?>

				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<?php echo form_checkbox('topicos['.$key.']',$topico, set_value('topicos['.$key.']'), 'class="form-control icrbm-checkbox" '); ?>
					<?php echo form_label($topico, '', array('class' => 'control-label icrbm-checkbox-label')); ?>
					<?php echo form_error('topicos'); ?>
				</div>
				<?php endforeach;?>
			</div>
		</div>
		<br />
		<div class="text-right">
			<button type="submit" class="btn btn-primary">
				Enviar
			</button>
			<a class="btn btn-default" href="<?php echo base_url('papers'); ?>">
				Cancelar
			</a>
		</div>
		<br />
	<?php echo form_close(); ?>
	<br />
<?php $this->load->view('layout/footer'); ?>