<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Papers
	</h1>
	<?php echo form_open('papers/edit/' . $paper->get_id(), 'class=""'); ?>
		<div class="text-right">
			<button type="submit" class="btn btn-primary">
				Submit
			</button>
			<a class="btn btn-default" href="<?php echo base_url('papers'); ?>">
				Cancel
			</a>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Titulo', 'titulo', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('titulo', set_value('titulo', $paper->get_titulo()), 'class="form-control" required'); ?>
				<?php echo form_error('titulo'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Abstract', 'abstract', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('abstract', set_value('abstract', $paper->get_abstract()), 'class="form-control" required'); ?>
				<?php echo form_error('abstract'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Trabajo', 'trabajo', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('trabajo', set_value('trabajo', $paper->get_trabajo()), 'class="form-control" required'); ?>
				<?php echo form_error('trabajo'); ?>
			</div>
		</div>
	<?php echo form_close(); ?>
<?php $this->load->view('layout/footer'); ?>