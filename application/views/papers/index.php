<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Papers
	</h1>
	<div class="text-right">
		<a class="btn btn-default" href="<?php echo base_url('papers/create'); ?>">
			Create A New Paper
		</a>
	</div>
	<?php if ($papers): ?>
		<table class="table table table-striped table-hover">
			<thead>
				<tr>
					<td>Titulo</td>
					<td>Abstract</td>
					<td>Trabajo</td>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($papers as $paper): ?>
					<tr>
						<td><?php echo $paper->get_titulo(); ?></td>
						<td><?php echo $paper->get_abstract(); ?></td>
						<td><?php echo $paper->get_trabajo(); ?></td>
						<td>
							<a href="<?php echo base_url('papers/edit/' . $paper->get_id()); ?>">
								<i class="fa fa-edit fa-2x"></i>
							</a>
							<a href="<?php echo base_url('papers/delete/' . $paper->get_id()); ?>">
								<i class="fa fa-trash fa-2x"></i>
							</a>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php echo $links; ?>
	<?php elseif ($this->input->get('keyword')): ?>
		Your search - <b><?php echo $this->input->get('keyword') ?></b> - did not match any papers.
	<?php else: ?>
		There are no papers that are currently available.
	<?php endif; ?>
<?php $this->load->view('layout/footer'); ?>