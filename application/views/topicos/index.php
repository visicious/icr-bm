<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Topicos
	</h1>
	<div class="text-right">
		<a class="btn btn-default" href="<?php echo base_url('topicos/create'); ?>">
			Create A New Topico
		</a>
	</div>
	<?php if ($topicos): ?>
		<table class="table table table-striped table-hover">
			<thead>
				<tr>
					<td>Nombre</td>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($topicos as $topico): ?>
					<tr>
						<td><?php echo $topico->get_nombre(); ?></td>
						<td>
							<a href="<?php echo base_url('topicos/edit/' . $topico->get_id()); ?>">
								<i class="fa fa-edit fa-2x"></i>
							</a>
							<a href="<?php echo base_url('topicos/delete/' . $topico->get_id()); ?>">
								<i class="fa fa-trash fa-2x"></i>
							</a>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php echo $links; ?>
	<?php elseif ($this->input->get('keyword')): ?>
		Your search - <b><?php echo $this->input->get('keyword') ?></b> - did not match any topicos.
	<?php else: ?>
		There are no topicos that are currently available.
	<?php endif; ?>
<?php $this->load->view('layout/footer'); ?>