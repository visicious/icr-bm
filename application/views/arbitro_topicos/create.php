<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Arbitro_Topicos
	</h1>
	<?php echo form_open('arbitro_topicos/create', 'class=""'); ?>
		<div class="text-right">
			<button type="submit" class="btn btn-primary">
				Submit
			</button>
			<a class="btn btn-default" href="<?php echo base_url('arbitro_topicos'); ?>">
				Cancel
			</a>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Arbitro', 'arbitro_id', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_dropdown('arbitro_id', $arbitros, set_value('arbitro_id'), 'class="form-control" required'); ?>
				<?php echo form_error('arbitro_id'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Topico', 'topico_id', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_dropdown('topico_id', $topicos, set_value('topico_id'), 'class="form-control" required'); ?>
				<?php echo form_error('topico_id'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Decision', 'decision', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('decision', $temas, set_value('decision'), 'class="form-control" required'); ?>
				<?php echo form_error('decision'); ?>
			</div>
		</div>
	<?php echo form_close(); ?>
<?php $this->load->view('layout/footer'); ?>