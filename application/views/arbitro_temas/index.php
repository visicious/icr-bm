<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Arbitro_Temas
	</h1>
	<div class="text-right">
		<a class="btn btn-default" href="<?php echo base_url('arbitro_temas/create'); ?>">
			Create A New Arbitro_Tema
		</a>
	</div>
	<?php if ($arbitro_temas): ?>
		<table class="table table table-striped table-hover">
			<thead>
				<tr>
					<td>Arbitro</td>
					<td>Tema</td>
					<td>Decision</td>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($arbitro_temas as $arbitro_tema): ?>
					<tr>
						<td><?php echo $arbitro_tema->get_arbitro_id()->get_id(); ?></td>
						<td><?php echo $arbitro_tema->get_tema_id()->get_id(); ?></td>
						<td><?php echo $arbitro_tema->get_decision(); ?></td>
						<td>
							<a href="<?php echo base_url('arbitro_temas/edit/' . $arbitro_tema->get_id()); ?>">
								<i class="fa fa-edit fa-2x"></i>
							</a>
							<a href="<?php echo base_url('arbitro_temas/delete/' . $arbitro_tema->get_id()); ?>">
								<i class="fa fa-trash fa-2x"></i>
							</a>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php echo $links; ?>
	<?php elseif ($this->input->get('keyword')): ?>
		Your search - <b><?php echo $this->input->get('keyword') ?></b> - did not match any arbitro temas.
	<?php else: ?>
		There are no arbitro temas that are currently available.
	<?php endif; ?>
<?php $this->load->view('layout/footer'); ?>