<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Autor_Papers
	</h1>
	<?php echo form_open('autor_papers/edit/' . $autor_paper->get_id(), 'class=""'); ?>
		<div class="text-right">
			<button type="submit" class="btn btn-primary">
				Submit
			</button>
			<a class="btn btn-default" href="<?php echo base_url('autor_papers'); ?>">
				Cancel
			</a>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Autor', 'autor_id', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_dropdown('autor_id', $autors, set_value('autor_id', $autor_paper->get_autor_id()->get_id()), 'class="form-control" required'); ?>
				<?php echo form_error('autor_id'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Paper', 'paper_id', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_dropdown('paper_id', $papers, set_value('paper_id', $autor_paper->get_paper_id()->get_id()), 'class="form-control" required'); ?>
				<?php echo form_error('paper_id'); ?>
			</div>
		</div>
	<?php echo form_close(); ?>
<?php $this->load->view('layout/footer'); ?>