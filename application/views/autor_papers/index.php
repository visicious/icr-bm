<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Autor_Papers
	</h1>
	<div class="text-right">
		<a class="btn btn-default" href="<?php echo base_url('autor_papers/create'); ?>">
			Create A New Autor_Paper
		</a>
	</div>
	<?php if ($autor_papers): ?>
		<table class="table table table-striped table-hover">
			<thead>
				<tr>
					<td>Autor</td>
					<td>Paper</td>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($autor_papers as $autor_paper): ?>
					<tr>
						<td><?php echo $autor_paper->get_autor_id()->get_id(); ?></td>
						<td><?php echo $autor_paper->get_paper_id()->get_id(); ?></td>
						<td>
							<a href="<?php echo base_url('autor_papers/edit/' . $autor_paper->get_id()); ?>">
								<i class="fa fa-edit fa-2x"></i>
							</a>
							<a href="<?php echo base_url('autor_papers/delete/' . $autor_paper->get_id()); ?>">
								<i class="fa fa-trash fa-2x"></i>
							</a>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php echo $links; ?>
	<?php elseif ($this->input->get('keyword')): ?>
		Your search - <b><?php echo $this->input->get('keyword') ?></b> - did not match any autor papers.
	<?php else: ?>
		There are no autor papers that are currently available.
	<?php endif; ?>
<?php $this->load->view('layout/footer'); ?>