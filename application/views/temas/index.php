<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Temas
	</h1>
	<div class="text-right">
		<a class="btn btn-default" href="<?php echo base_url('temas/create'); ?>">
			Create A New Tema
		</a>
	</div>
	<?php if ($temas): ?>
		<table class="table table table-striped table-hover">
			<thead>
				<tr>
					<td>Nombre</td>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($temas as $tema): ?>
					<tr>
						<td><?php echo $tema->get_nombre(); ?></td>
						<td>
							<a href="<?php echo base_url('temas/edit/' . $tema->get_id()); ?>">
								<i class="fa fa-edit fa-2x"></i>
							</a>
							<a href="<?php echo base_url('temas/delete/' . $tema->get_id()); ?>">
								<i class="fa fa-trash fa-2x"></i>
							</a>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php echo $links; ?>
	<?php elseif ($this->input->get('keyword')): ?>
		Your search - <b><?php echo $this->input->get('keyword') ?></b> - did not match any temas.
	<?php else: ?>
		There are no temas that are currently available.
	<?php endif; ?>
<?php $this->load->view('layout/footer'); ?>