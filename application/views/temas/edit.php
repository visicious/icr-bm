<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Temas
	</h1>
	<?php echo form_open('temas/edit/' . $tema->get_id(), 'class=""'); ?>
		<div class="text-right">
			<button type="submit" class="btn btn-primary">
				Submit
			</button>
			<a class="btn btn-default" href="<?php echo base_url('temas'); ?>">
				Cancel
			</a>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Nombre', 'nombre', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('nombre', set_value('nombre', $tema->get_nombre()), 'class="form-control" required'); ?>
				<?php echo form_error('nombre'); ?>
			</div>
		</div>
	<?php echo form_close(); ?>
<?php $this->load->view('layout/footer'); ?>