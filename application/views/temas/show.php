<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Temas
	</h1>
	<div class="text-right">
		<a class="btn btn-default" href="<?php echo base_url('temas'); ?>">
			Cancel
		</a>
	</div>
	<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?php echo form_label('Nombre', 'nombre', array('class' => 'control-label')); ?>
		<div class="">
			<input type="text" class="form-control" value="<?php echo $tema->get_nombre(); ?>" disabled>
		</div>
	</div>
<?php $this->load->view('layout/footer'); ?>