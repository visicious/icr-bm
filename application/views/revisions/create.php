<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Revisions
	</h1>
	<?php echo form_open('revisions/create', 'class=""'); ?>
		<div class="text-right">
			<button type="submit" class="btn btn-primary">
				Submit
			</button>
			<a class="btn btn-default" href="<?php echo base_url('revisions'); ?>">
				Cancel
			</a>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Paper', 'trabajo_id', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_dropdown('trabajo_id', $papers, set_value('trabajo_id'), 'class="form-control" required'); ?>
				<?php echo form_error('trabajo_id'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Arbitro', 'arbitro_id', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_dropdown('arbitro_id', $arbitros, set_value('arbitro_id'), 'class="form-control" required'); ?>
				<?php echo form_error('arbitro_id'); ?>
			</div>
		</div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php echo form_label('Evaluation', 'evaluation', array('class' => 'control-label')); ?>
			<div class="">
				<?php echo form_input('evaluation', set_value('evaluation'), 'class="form-control" '); ?>
				<?php echo form_error('evaluation'); ?>
			</div>
		</div>
	<?php echo form_close(); ?>
<?php $this->load->view('layout/footer'); ?>