<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Revisions
	</h1>
	<div class="text-right">
		<a class="btn btn-default" href="<?php echo base_url('revisions'); ?>">
			Cancel
		</a>
	</div>
	<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?php echo form_label('Papers', 'trabajo_id', array('class' => 'control-label')); ?>
		<div class="">
			<input type="text" class="form-control" value="<?php echo $revision->get_trabajo_id()->get_id(); ?>" disabled>
		</div>
	</div>
	<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?php echo form_label('Arbitros', 'arbitro_id', array('class' => 'control-label')); ?>
		<div class="">
			<input type="text" class="form-control" value="<?php echo $revision->get_arbitro_id()->get_id(); ?>" disabled>
		</div>
	</div>
	<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?php echo form_label('Evaluation', 'evaluation', array('class' => 'control-label')); ?>
		<div class="">
			<input type="text" class="form-control" value="<?php echo $revision->get_evaluation(); ?>" disabled>
		</div>
	</div>
<?php $this->load->view('layout/footer'); ?>