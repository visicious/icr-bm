<?php $this->load->view('layout/header'); ?>
	<h1>
		<i class="fa fa-lg fa-list"></i> 
		Revisions
	</h1>
	<div class="text-right">
		<a class="btn btn-default" href="<?php echo base_url('revisions/create'); ?>">
			Create A New Revision
		</a>
	</div>
	<?php if ($revisions): ?>
		<table class="table table table-striped table-hover">
			<thead>
				<tr>
					<td>Trabajo</td>
					<td>Arbitro</td>
					<td>Evaluation</td>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($revisions as $revision): ?>
					<tr>
						<td><?php echo $revision->get_trabajo_id()->get_id(); ?></td>
						<td><?php echo $revision->get_arbitro_id()->get_id(); ?></td>
						<td><?php echo $revision->get_evaluation(); ?></td>
						<td>
							<a href="<?php echo base_url('revisions/edit/' . $revision->get_id()); ?>">
								<i class="fa fa-edit fa-2x"></i>
							</a>
							<a href="<?php echo base_url('revisions/delete/' . $revision->get_id()); ?>">
								<i class="fa fa-trash fa-2x"></i>
							</a>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php echo $links; ?>
	<?php elseif ($this->input->get('keyword')): ?>
		Your search - <b><?php echo $this->input->get('keyword') ?></b> - did not match any revisions.
	<?php else: ?>
		There are no revisions that are currently available.
	<?php endif; ?>
<?php $this->load->view('layout/footer'); ?>