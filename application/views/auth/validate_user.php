<?php $this->load->view('layout/header'); ?>
<br />
<h1>Validar Inscripción</h1>
<br />
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <?php if (isset($user)): ?>
      <h4>El usuario identificado como:</h4>
      <ul>
        <li><strong><?php echo $user->first_name." ".$user->last_name;?></strong></li>
        <li>DNI/Pasaporte/Documento de Identidad: <strong><?php echo $user->dni;?></strong></li>
        <li>Correo: <strong><?php echo $user->email;?></strong></li>
        <li>Registrado como: <strong><?php echo $user->user_type;?></strong></li>
      </ul>
      <br />
      <h3>¿Ha podido ser validado su pago? </h3>


      <div id="infoMessage"><?php echo $message;?></div>

      <?php echo form_open(uri_string());?>
          <p>
              <?php echo form_radio($has_pay_yes);?>
              <?php echo 'Si';?> 
              <br />
              <?php echo form_radio($has_pay_no);?>
              <?php echo 'No';?>
          </p>   

          <br />
          <p><?php echo form_submit('submit', 'Enviar','class="btn btn-primary"');?></p>

      <?php echo form_close();?>
      <?php else: ?>
        <h3>El usuario no existe</h3> 
      <?php endif;?>
    </div>
</div>
<br />
<?php $this->load->view('layout/footer'); ?>