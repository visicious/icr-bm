<?php $this->load->view('layout/header'); ?>
<br />
<h1>Confirmar su Inscripción</h1>
<br />

<?php if (!$already_uploaded): ?>
    <p>Suba una <strong>foto del voucher</strong> correspondiente a su pago y una <strong>foto suya</strong> para colocarla en su credencial</p>
    <br />

    <div id="infoMessage"><?php echo $message;?></div>

    <?php echo form_open_multipart("auth/upload_pay");?>

          <p>
                <strong>Foto del Voucher</strong> (formatos válidos: png, jpg, jpeg) <br />
                <?php echo form_upload($voucher);?>
          </p>

          <p>
                <strong>Foto para su Credencial</strong> (formatos válidos: png, jpg, jpeg)  <br />
                <?php echo form_upload($user_photo);?>
          </p>    

          <br />
          <p><?php echo form_submit('submit', 'Enviar','class="btn btn-primary"');?></p>

    <?php echo form_close();?>
<?php else: ?>
    <div class="row">
        <div class="icrbm_add_paper col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3><a href="<?php echo base_url('paper/create');?>">
                ¡Gracias! En menos de 48 horas validaremos su pago.
                <?php if ($is_author): ?>Una vez validado podrá subir su paper.<?php endif;?>
            </a></h3>
        </div>
    </div>
<?php endif;?>
<br />
<?php $this->load->view('layout/footer'); ?>