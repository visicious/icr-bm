<?php $this->load->view('layout/header'); ?>
<br />
<?php if($has_user_type): ?>
    <?php if($render_user_type == 'autor'): ?>
      <h1><?php echo lang('create_user_heading');
                echo " de Autor"; ?></h1>
    <?php elseif($render_user_type == 'participante'): ?>  
      <h1><?php echo lang('create_user_heading');
                echo " de Participante"; ?></h1>
    <?php endif;?>
<?php else: ?>
  <h1><?php echo lang('create_user_heading');?></h1>
<?php endif;?>
<br />
<p><?php echo lang('create_user_subheading');?></p>
<br />

<div id="infoMessage"><?php echo $message;?></div>

<?php if($has_user_type): ?>
  <?php echo form_open_multipart("auth/create_user/".$render_user_type);?>
<?php else: ?>
  <?php echo form_open_multipart("auth/create_user");?>
<?php endif;?>

<div class="row">
  <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6">
      <p>
            <?php echo lang('create_user_fname_label', 'first_name');?> <br />
            <?php echo form_input($first_name);?>
      </p>

      <p>
            <?php echo lang('create_user_lname_label', 'last_name');?> <br />
            <?php echo form_input($last_name);?>
      </p>
      
      <?php
      if($identity_column!=='email') {
          echo '<p>';
          echo lang('create_user_identity_label', 'identity');
          echo '<br />';
          echo form_error('identity');
          echo form_input($identity);
          echo '</p>';
      }
      ?>
      <p>
            <?php echo 'DNI/Pasaporte/Documento de Identidad:'?> <br />
            <?php echo form_input($dni);?>
      </p>

      <p>
            <?php echo 'Sexo:'?> 
      </p>
      <p>
            <?php echo form_radio($gender_male);?>
            <?php echo 'Masculino';?> 
            <br />
            <?php echo form_radio($gender_female);?>
            <?php echo 'Femenino';?>
      </p>

      <p>
            <?php echo lang('create_user_email_label', 'email');?> <br />
            <?php echo form_input($email);?>
      </p>

      <p>
            <?php echo lang('create_user_phone_label', 'phone');?> <br />
            <?php echo form_input($phone);?>
      </p>

      <p>
            <?php echo lang('create_user_company_label', 'company');?> <br />
            <?php echo form_input($company);?>
      </p>

      <p>
            <?php echo 'Carrera:';?> <br />
            <?php echo form_input($career);?>
      </p>

      <?php if(!$has_user_type): ?>
        <p>
            <?php echo 'Registro como:'?> 
        </p>
        <p>
              <?php echo form_radio($user_type_autor);?>
              <?php echo 'Autor';?> 
              <br />
              <?php echo form_radio($user_type_participante);?>
              <?php echo 'Participante';?>

        </p>
      <?php endif;?>

      <p>
            <?php echo 'Intereses dentro del Congreso:';?> <br />
            <?php echo form_input($interests);?>
      </p>

      <p>
            <?php echo 'Breve Perfil Profesional(máximo 250 caracteres):';?> <br />
            <?php echo form_textarea($professional_resume);?>
      </p>

      <br />
  </div>
  <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6">
      <h3>Datos del Pago</h3>
      <br />
       <p>
                <strong>Foto del Voucher</strong> (formatos válidos: png, jpg, jpeg; resolución de 300 pixeles) <br />
                <?php echo form_upload($voucher);?>
          </p>

          <p>
                <strong>Foto para su Credencial</strong> (formatos válidos: png, jpg, jpeg; resolución de 300 pixeles)  <br />
                <?php echo form_upload($user_photo);?>
          </p>    


      <p>
            <?php echo 'Banco:';?> <br />
            <?php echo form_dropdown($bank_name);?>
      </p>

      <p>
            <?php echo 'Número de Operación:';?> <a href="#" class="show-operation-number-example">Si tiene dudas, vea ejemplo</a><br />
              <img class="operation-number-example"src="<?php echo base_url('images/ejemplo-voucher.jpeg');?>" style="display: none;width: 60%; height: 60%; ">
            <?php echo form_input($bank_number);?>
      </p>      

      <p>
            <?php echo 'Fecha de la Operación:'?> <br />
            <?php echo form_input($bank_date);?>
      </p>

      <p>
            <?php echo 'Monto de la Operación:';?> <br />
            <?php echo form_dropdown($bank_amount);?>
      </p>
      <br />

      <h3>Crear Contraseña</h3>
      <br />
      <p>
            <?php echo lang('create_user_password_label', 'password');?> <br />
            <?php echo form_input($password);?>
      </p>

      <p>
            <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
            <?php echo form_input($password_confirm);?>
      </p>

      <br />
      <p><?php echo form_submit('submit', lang('create_user_submit_btn'),'class="btn btn-primary"');?></p>
  </div>
</div>
<?php echo form_close();?>
<br />
<?php $this->load->view('layout/footer'); ?>