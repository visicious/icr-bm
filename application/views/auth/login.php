<?php $this->load->view('layout/header'); ?>
<br />
<h1><?php echo lang('login_heading');?></h1>
<br />
<p><?php echo lang('login_subheading');?></p>
<br />
<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open("auth/login");?>

  <p>
    <?php echo lang('login_identity_label', 'identity');?>
    <?php echo form_input($identity);?>
  </p>

  <p>
    <?php echo lang('login_password_label', 'password');?>
    <?php echo form_input($password);?>
  </p>

  <p>
    <?php echo lang('login_remember_label', 'remember');?>
    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
  </p>


  <p><?php echo form_submit('submit', lang('login_submit_btn'), 'class="btn btn-primary"');?></p>

<?php echo form_close();?>

<p><a href="create_user" class="btn btn-primary"><?php echo lang('login_create_user_link');?></a></p>
<p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p
<?php $this->load->view('layout/footer'); ?>