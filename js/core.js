function evalAlert(){
    $.alert({
        title: 'Urgente',
        content: '¡Es necesario para poder completar su registro!',
        type: 'orange',
    });
}

$(document).ready(function(){
    $(".btn-cancel-evaluador-icrbm").on("click", function(e){
        evalAlert();
        e.preventDefault();
    });

    $(".table-neutral-icrbm").children().prop( "checked", true );
});
