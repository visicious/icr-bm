<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2018, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']		= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} es necesario.</strong>';
$lang['form_validation_isset']			= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} no puede estar vacío.</strong>';
$lang['form_validation_valid_email']		= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} debe ser un correo electrónico válido.</strong>';
$lang['form_validation_valid_emails']		= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} debe contener todas las direcciones de correo electrónico válidas.</strong>';
$lang['form_validation_valid_url']		= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} debe contener una URL válida.</strong>';
$lang['form_validation_valid_ip']		= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} debe contener una IP válida.</strong>';
$lang['form_validation_min_length']		= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} al menos debe ser de {param} caracteres de longitud.</strong>';
$lang['form_validation_max_length']		= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} no puede exceder de {param} caracteres de longitud.</strong>';
$lang['form_validation_exact_length']		= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} debe tener exactamente {param} caracteres de longitud.</strong>';
$lang['form_validation_alpha']			= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} solo puede contener caracteres alfabéticos.</strong>';
$lang['form_validation_alpha_numeric']		= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} solo puede contener caracteres alfanuméricos.</strong>';
$lang['form_validation_alpha_numeric_spaces']	= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} solo puede contener caracteres alfanuméricos y espacios.</strong>';
$lang['form_validation_alpha_dash']		= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} may only contain alpha-numeric characters, underscores, and dashes.</strong>';
$lang['form_validation_numeric']		= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} solo puede contener caracteres alfanuméricos, guiones bajos y guiones.</strong>';
$lang['form_validation_is_numeric']		= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} debe contener solo caracteres numéricos.</strong>';
$lang['form_validation_integer']		= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} debe contener un número entero.</strong>';
$lang['form_validation_regex_match']		= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} no está en el formato correcto.</strong>';
$lang['form_validation_matches']		= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} no coincide con el campo {param}.</strong>';
$lang['form_validation_differs']		= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} debe ser diferente al campo {param}.</strong>';
$lang['form_validation_is_unique'] 		= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} ya existe.</strong>';
$lang['form_validation_is_natural']		= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} solo debe contener dígitos.</strong>';
$lang['form_validation_is_natural_no_zero']	= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} solo debe contener dígitos y debe ser mayor que cero.</strong>';
$lang['form_validation_decimal']		= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} debe contener un número decimal.</strong>';
$lang['form_validation_less_than']		= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} debe contener un número menor que {param}.</strong>';
$lang['form_validation_less_than_equal_to']	= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} debe contener un número menor o igual a {param}.</strong>';
$lang['form_validation_greater_than']		= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} debe contener un número mayor que {param}.</strong>';
$lang['form_validation_greater_than_equal_to']	= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} debe contener un número mayor o igual a {param}.</strong>';
$lang['form_validation_error_message_not_set']	= 'No se puede acceder a un mensaje de error correspondiente al campo {field}.';
$lang['form_validation_in_list']		= '<strong style="color: white;background-color:red;padding:6px;">El campo {field} debe ser uno de: {param}.</strong>';
